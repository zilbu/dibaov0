<?php if(!defined('bcms'))die('Cannot access directly!'); 
$group_active = 0;
if( $_REQUEST['module'] == 'do_user'||
    $_REQUEST['module'] == 'user_group'||
    $_REQUEST['module'] == 'user_profile'
)$group_active = 1;
if( $_REQUEST['module'] == 'explorer'||
    $_REQUEST['module'] == 'langsys' ||
    $_REQUEST['tblname'] == 'site'
)$group_active = 2;
?>
<script type="text/javascript">
$(document).ready(function(){
    //style menu left
    var icons = {
			header: "ui-icon-circle-arrow-e",
			headerSelected: "ui-icon-circle-arrow-s"
		};
    $('#menuleft').accordion({
        icons: icons,
        active : <?=$group_active?>,
        clearStyle : true
    });
});
</script>
<?php
$menu_array  = array(
    $strManagement.' '.$strInformation => array(
									array(
                                                  "title" => 'Sản phẩm',
                                                  "href" => "?module=categories1&tblname=articles",
                                                  "onclick" => "window.location='?module=categories1&tblname=articles'",
                                                  "icon" => "icons/articles.png"
                                                  ),
									
									// array(
                                                // "title" => 'Chuyên mục sản phẩm',
                                                // "href" => "javascript:void(0);",
                                                // "onclick" => "window.location='?module=quickedit&tblname=articles_cat'",
                                                // "icon" => "icons/products.png"
                                               // ),
									array(
                                                "title" => 'Nhóm sản phẩm',
                                                "href" => "javascript:void(0);",
                                                "onclick" => "window.location='?module=quickedit&tblname=menu'",
                                                "icon" => "icons/products.png"
                                               ),	
										//$strNews	   
                                    array(
                                                "title" => 'Bài viết',
                                                "href" => "javascript:void(0);",
                                                "onclick" => "window.location='?module=categories1&tblname=news'",
                                                "icon" => "icons/edit2.png"
                                                ),
									array(
                                                "title" => 'Đại lý',
                                                "href" => "javascript:void(0);",
                                                // "onclick" => "window.location='?module=categories1&tblname=locationitem'",
                                                "onclick" => "window.location='?module=quickedit&tblname=locationitem'",
                                                "icon" => "icons/edit2.png"
                                                ),
									// array(
                                                // "title" => 'Trả lời',
                                                // "href" => "javascript:void(0);",
                                                // "onclick" => "window.location='?module=categories1&tblname=comment'",
                                                // "onclick" => "window.location='?module=quickedit&tblname=comment'",
                                                // "icon" => "icons/edit2.png"
                                                // ),
									// array(
                                                // "title" => 'Hỏi đáp xe điện',
                                                // "href" => "javascript:void(0);",
                                                // "onclick" => "window.location='?module=categories1&tblname=hoidap'",
                                                // "onclick" => "window.location='?module=quickedit&tblname=hoidap'",
                                                // "icon" => "icons/edit2.png"
                                                // ),
									// array(
                                                // "title" => 'Quản lý khuyến mãi',
                                                // "href" => "javascript:void(0);",
                                                // "onclick" => "window.location='?module=categories1&tblname=khuyenmai'",
                                                // "onclick" => "window.location='?module=quickedit&tblname=khuyenmai'",
                                                // "icon" => "icons/edit2.png"
                                                // ),
									// array(
                                                // "title" => 'Quản lý bảo hành',
                                                // "href" => "javascript:void(0);",
                                                // "onclick" => "window.location='?module=categories1&tblname=banxe'",
                                                // "onclick" => "window.location='?module=quickedit&tblname=banxe'",
                                                // "icon" => "icons/security.png"
                                                // ),
									// array(
                                                  // "title" => 'Thư viện ảnh',
                                                  // "href" => "?module=categories1&tblname=gallerys",
                                                  // "onclick" => "window.location='?module=categories1&tblname=gallerys'",
                                                  // "icon" => "icons/picture.png"
                                                  // ),
										
                                    array(
                                                  "title" => $strWeblink,
                                                  "href" => "?module=quickedit&tblname=weblinks",
                                                  "onclick" => "window.location='?module=quickedit&tblname=weblinks'",
                                                  "icon" => "icons/weblinks.png"
                                                  ),
                                    array(
                                                  "title" => $strSlide,
                                                  "href" => "?module=quickedit&tblname=slides",
                                                  "onclick" => "window.location='?module=quickedit&tblname=slides'",
                                                  "icon" => "icons/media1.png"
                                                  ),

									// array(
                                                // "title" => 'Đơn hàng',
                                                // "href" => "javascript:void(0);",
                                                // "onclick" => "window.location='?module=quickedit&tblname=invoices'",
                                                // "icon" => "icons/order.gif"
                                                // ),
											
                                    array(
                                                "title" => $strContact,
                                                "href" => "javascript:void(0);",
                                                "onclick" => "window.location='?module=quickedit&tblname=contact'",
                                                "icon" => "icons/contact.png"
                                                ),
									// array(
                                                // "title" => 'Email',
                                                // "href" => "javascript:void(0);",
                                                // "onclick" => "window.location='?module=quickedit&tblname=emailcustomer'",
                                                // "icon" => "icons/contact.png"
                                                // ),
									// array(
                                               // "title" => "Videos",
                                               // "href" => "javascript:void(0);",
                                               // "onclick" => "window.location='?module=quickedit&tblname=videos'",
                                               // "icon" => "icons/media1.png"
 
                                    // ),
									// array(
                                               // "title" => "Chương trình khuyến mãi",
                                               // "href" => "javascript:void(0);",
                                               // "onclick" => "window.location='?module=quickedit&tblname=tbl_sale'",
                                               // "icon" => "icons/media1.png"
                                               // )
                                    ),
    $strManagement.' '.$strUser => array(
                                    array(
                                                "title" => $strCreatNew,
                                                "href" => "javascript:void(0);",
                                                "onclick" => "window.location='?module=do_user'",
                                                "icon" => "icons/user.png"
                                                ),
                                    array(
                                                "title" => $strManagement.' '.$strGroup,
                                                "href" => "javascript:void(0);",
                                                "onclick" => "window.location='?module=user_group'",
                                                "icon" => "icons/usergroup.png"
                                                )
                                    ),
    $strManagement.' '.$strSystem => array(
                                    array(
                                                "title" => $strFile,
                                                "href" => "javascript:void(0);",
                                                "onclick" => "window.location='?module=explorer&file_type=files'",
                                                "icon" => "icons/folder48.gif"
                                                ),
                                    array(
                                                "title" => $strImage,
                                                "href" => "javascript:void(0);",
                                                "onclick" => "window.location='?module=explorer&file_type=images'",
                                                "icon" => "icons/photo.png"
                                                ),
                                    array(
                                                "title" => $strLanguage,
                                                "href" => "javascript:void(0);",
                                                "onclick" => "window.location='?module=langsys'",
                                                "icon" => "icons/edit1.png"
                                                ),
                                     array(
                                                "title" => $strConfig,
                                                "href" => "javascript:void(0);",
                                                "onclick" => "window.location='?module=site&tblname=site'",
                                                "icon" => "icons/setting2.png"
                                           )
                                    )
    

);

?>
<div class="menuleft" id="menuleft">
    <?php
    foreach($menu_array as $kmenu => $gmenu)
    {?>
        <h3><a href="javascript:void(0);"><?php echo $kmenu; ?></a></h3>
        <div>
            <ul>
                <?php foreach($gmenu as $imenu) { ?>
                <li>
                    <a href="<?=$imenu['href']?>" onClick="<?=$imenu['onclick']?>"><?=$imenu['title']?></a>
                </li>
                <?php } ?>
            </ul>
        </div>
    <?php }
    ?>
</div>