browser.initSearch = function(){
    $("input#txtsearch").val("");
    $("input#btreset").click(function(){
        $("input#txtsearch").val("");
        browser.refresh();
    });
    $("input#btsearch").click(function(){
        browser.searchFile($("input#txtsearch").val());
    });
    if (!_.kuki.isSet('searchOnType'))
        _.kuki.set('searchOnType', 'off');
  /*  $(window).keypress(function(event) {
            var keycode = (event.keyCode ? event.keyCode : event.which);
            keypressed = String.fromCharCode(keycode);
            if(!$("input#txtsearch").is(":focus") && keypressed.match(/[a-zA-Z0-9]/)) {
                $("input#txtsearch").val($("input#txtsearch").val()+keypressed).focus(); 
                $("input#txtsearch").trigger("keypress");
            }
        });*/
    if(_.kuki.get('searchOnType') == 'on' )
    {
        $("input#cbtype").attr("checked","checked");
        $("input#txtsearch").keypress(function(){
            browser.searchFile($("input#txtsearch").val());
        });
    }
    else{
        $("input#cbtype").removeAttr("checked");
        $("input#txtsearch").unbind('keypress');
    }
    $("input#cbtype").change(function(){
        if($("input#cbtype").is(":checked"))
        {
            _.kuki.set('searchOnType','on');
            $("input#txtsearch").keypress(function(){browser.searchFile($("input#txtsearch").val());});
        }
        else{
            _.kuki.set('searchOnType','off');
            $("input#txtsearch").unbind('keypress');
        }
    });
}
browser.searchFile = function(keyword) {
    selected = null;
    if (!browser.files || !browser.files.sort)
        browser.files = [];
    $.each(browser.files,function(i,file){
        var string1 = file.name;
        var string2 = keyword;
        console.log(string1+string2+string1.search(new RegExp(string2, "i")));
        if(string1.search(new RegExp(string2, "i")) >= 0)browser.files[i].ishide = false;
        else browser.files[i].ishide = true;
    });
    browser.showFiles();
    browser.initFiles();
};