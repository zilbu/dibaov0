﻿/***************************/
//@Author: Adrian "yEnS" Mato Gondelle & Ivan Guardado Castro
//@website: www.yensdesign.com
//@email: yensamg@gmail.com
//@license: Feel free to use it, but keep this credits please!					
/***************************/

$(document).ready(function(){
	$(".menu1 > li").click(function(e){
		switch(e.target.id){
			case "news":
				//change status & style menu
				$("#news").addClass("active");
				$("#products").removeClass("active");
				$("#links").removeClass("active");
				//display selected division, hide others
				$("div.news").fadeIn();
				$("div.products").css("display", "none");
				$("div.links").css("display", "none");
			break;
			case "products":
				//change status & style menu
				$("#news").removeClass("active");
				$("#products").addClass("active");
				$("#links").removeClass("active");
				//display selected division, hide others
				$("div.products").fadeIn();
				$("div.news").css("display", "none");
				$("div.links").css("display", "none");
			break;
			case "links":
				//change status & style menu
				$("#news").removeClass("active");
				$("#products").removeClass("active");
				$("#links").addClass("active");
				//display selected division, hide others
				$("div.links").fadeIn();
				$("div.news").css("display", "none");
				$("div.products").css("display", "none");
			break;
		}
		//alert(e.target.id);
		return false;
	});
});