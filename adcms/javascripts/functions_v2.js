// JavaScript Document

var vatgia_product_favorites_comment	= '<div class="title">Lợi ích khi theo dõi sản phẩm?</div><div class="content">Khi quý khách "Theo dõi" sản phẩm thì mọi thông tin như giá cả, hỏi đáp, đánh giá liên quan đến sản phẩm được cập nhật, thì hệ thống sẽ tự động gửi thông tin vào tài khoản của quý khách trên Vatgia.com. Quý khách có thể theo dõi thông báo mới từ hệ thống trên icon hình quả cầu bên cạnh ô tìm kiếm của website.</div>';
function addToCartButton(record_id, estore_id, estore_name, vatgia_verified, map_friend, direct_add_to_cart_link, baokim_payment, redirect){
	strReturn	= '<div class="add_to_cart_button">';
	if(vatgia_verified == 1) strReturn	+= '<div class="vatgia_verified">Hãy đặt hàng tại đây<br />để được đảm bảo an toàn <img class="tooltip_content" tooltipContent="vatgia_verified_text" src="' + fs_imagepath + 'icon_question_small.gif" style="cursor:pointer" /></div>';
	if(direct_add_to_cart_link != ""){
		strReturn	+= '<div><a href="' + direct_add_to_cart_link + '"><img src="' + fs_imagepath + 'btn_order.gif" /></a></div>';
		strReturn	+= '<div class="end"><a class="tooltip_content" tooltipContent="vatgia_product_favorites_comment" href="/home/addfavorites.php?addto=product&record_id=' + record_id + '&redirect=' + redirect + '" rel="nofollow"><img src="' + fs_imagepath + 'btn_order_favorites.gif" /></a></div>';
	}
	else{
		href	= '/home/addtocart.php?iPro=' + record_id + '&estore_id=' + estore_id + '&estore_name=' + estore_name + '&return=' + redirect;
		strReturn	+= '<div><a href="javascript:;" onclick="addToCartLink(\'' + href + '\'); return false;"><img src="' + fs_imagepath + 'btn_order.gif" /></a></div>';
		strReturn	+= '<div class="or">Hoặc</div>';
		strReturn	+= '<div>';
			if(map_friend !== null){
				strReturn	+= '<select id="friend_id"><option value="0">- Chọn người nhận -</option>';
				$.each(map_friend, function(key, value){
					strReturn	+= '<option value="' + key + '">' + value + '</option>';
				});
				strReturn	+= '</select>';
			}
			strReturn	+= '<a href="javascript:;" onClick="window.location.href=\'/home/buynow.php?iPro=' + record_id + '&iUse=' + estore_id + '&iUf=\' + parseInt($(&quot;#friend_id&quot;).val())"><img src="' + fs_imagepath + 'btn_order_1_click.gif" /></a>';
		strReturn	+= '</div>';
		strReturn	+= '<div' + (baokim_payment == 0 ? ' class="end"' : "") + '><a class="tooltip_content" tooltipContent="vatgia_product_favorites_comment" href="/home/addfavorites.php?addto=product&record_id=' + record_id + '&redirect=' + redirect + '" rel="nofollow"><img src="' + fs_imagepath + 'btn_order_favorites.gif" /></a></div>';
		if(baokim_payment == 1) strReturn	+= '<div class="baokim_pay_now end">An toàn hơn qua Baokim.vn<br /><a href="/hom