<?php if(!defined('bcms'))die('Cannot access directly!'); ?>
<?php
//config 
require_once('quickedit_config.php');
//gen ajax

//save data
$_mess = array();
if(isset($_POST['faction']) && $_POST['faction'] == "quickadd") 
{
    $now = date("Y-m-d H:i:s");
    $lang = get_langID();
    $active = 1;
    $level = 3;
    $category = $_POST['category'];
    $input = array("time" => $now,
                    "active" => $active,
                    "level" => $level,
                    "lang" => $lang,
                    "category" => $category);
    $title_array = $_POST['title'];
    $image_array = $_POST['image'];
    $_count = 0;
    foreach($image_array as $key => $image_i)
    {
        $input['url'] = $image_i;
        $input['title'] = addslashes($title_array[$key]);
        if(do_sql($tblname,$input,"insert"))
        $_count++;
    }
    $_mess[] = "Đã lưu ".$_count." ảnh vào nhóm ảnh ".get_by_id($tblname."_cat",$category,"name");
    
}
    
//main module show

?>
<div style="width: 100%; text-align: center; position: relative; height: 30px; vertical-align: middle;z-index: 1;">
    <div id="topcontrol" style="width: 100%; text-align: center; position: absolute;background-color: #E1E1E1;">
    <?php
    if (isset($_SESSION['userID']))
    	{
    	$allow_edit=true;
    	?>
        
    	<a href="index.php?module=categories1&tblname=<?=$tblname?>" class="back_button" onclick="">
    	<?php echo $strBack; ?>
    	</a>
    	<a href="javascript:void(0);" class="save_button" onclick="document.quickadd.submit();">
    	<?php echo 'Lưu toàn bộ'; ?>
    	</a>
        <?php if($have_cat){ ?>
        <a href="javascript:void(0);" class="add_button" onclick="selectFiles()" >
    	<?php echo 'Thêm ảnh'; ?>
    	</a>
        <?php } ?>
        
    	<?php
    	}
    else
    	{
    	?>
    	<p class="buttontext">&nbsp;&nbsp;<?php echo $strNotice['208']; ?></p>
    	<?php
    	}
    	?>
    <span id="toggle_button" class="showcontrol" style="float: left; height: 28px;"></span>
    </div>
</div>
<script>
function selectFiles(type ) {
    //show dialog
	var width = $(window).width() - 50;
	var height = $(window).height() - 50;
    if(!type)type = '<?=$_GET['tblname']?>';
    else type += '&dir=<?=$_GET['tblname']?>/';
	type='image';
    $("#kcfinder_div").html('<iframe name="kcfinder_iframe" src="bsexplorer/browse.php?type='+type+'" ' +
        'frameborder="0" width="100%" height="100%" marginwidth="0" marginheight="0" scrolling="no" />')
                        .dialog({
        position : 'center',
		dialogClass : 'myDialog',
        width : width,
        height : height,
        modal : true
    });
	$('.myDialog.ui-dialog').css({position : "fixed", top : "25px" , left : "25px" });
    //var div = document.getElementById('kcfinder_div');
   // if (div.style.display == "block") {
   //     div.style.display = 'none';
    //    div.innerHTML = '';
   //     return;
   // }
    window.KCFinder = {
        callBackMultiple: function(files) {
            window.KCFinder = null;
            //alert(files);
            addFileToField(files);
            $("#kcfinder_div").dialog('close');
          //  textarea.value = "";
           // for (var i = 0; i < files.length; i++)
           //     textarea.value += files[i] + "\n";
        }
    };
    //div.innerHTML = '';
   // div.style.display = 'block';
}
function addFileToField(files)
{
    var o = '';
    for (var i = 0; i < files.length; i++)
    {
        o += '<div class="arow">';
        o += '        <label>Tiêu đề</label><input type="text" name="title[]" />';
        o += '        <label>Ảnh</label><input onclick="selectFile(this);" type="text" value="'+files[i].replace(/^\/+/,"")+'" name="image[]" class="previewable select_image" />';
        o += '<a href="" target="_blank" class="imgpreview"><img /></span>';
        o += '        <a href="javascript:void(0);" class="del_button" onclick="removearow(this);">Xóa</a>';
        o += '    </div>';
    }
    $("#quickaddcontainer").append(o);
    $("a.del_button").button({
        icons: {
                primary: "ui-icon-trash"
            }
    });
    $(".previewable").each(function(){
        //var fieldname = $(this).attr("name");
        if($(this).val() != "")
        //$("#"+fieldname+"_prev").attr("src","../"+$(this).val());
        {
            $(this).nextAll('.imgpreview:first').lightBox()
                    .attr("href","/"+$(this).val())
                    .children('img')
                    .attr("src","ajax/image.php?width=50&height=50&cropratio=2:1&image=/"+$(this).val());
        }
    });
    $(".previewable").blur(function(){
        //var fieldname = $(this).attr("name");
        if($(this).val() != "")
        //$("#"+fieldname+"_prev").attr("src","../"+$(this).val());
        {
            $(this).nextAll('.imgpreview:first')
                    .attr("href","/"+$(this).val())
                    .children('img')
                    .attr("src","ajax/image.php?width=50&height=50&cropratio=2:1&image=/"+$(this).val());
        }
    });
    $(".previewable").change(function(){
        //var fieldname = $(this).attr("name");
        if($(this).val() != "")
        //$("#"+fieldname+"_prev").attr("src","../"+$(this).val());
        {
            $(this).nextAll('.imgpreview:first')
                    .attr("href","/"+$(this).val())
                    .children('img')
                    .attr("src","ajax/image.php?width=50&height=50&cropratio=2:1&image=/"+$(this).val());
        }
        
    });
}
function removearow(obj)
{
    $(obj).parents("div.arow:first").remove();
}
</script>
<div class="ui-widget-content">
    <h3 class="ui-widget-header">
        Thêm nhanh ảnh
    </h3>
    <div class="ui-widget-body">
        <form name="quickadd" action="" method="post" class="adminform">
        <input type="hidden" name="faction" value="quickadd" />
        <div style="padding: 10px;" id="quickaddcontainer">
            <div>
                <label>Chọn nhóm</label>
                <select name="category">
                <?php
                $getcats = get_all($tblname."_cat"," lang='".get_langID()."' "," level asc ");
                if(count($getcats))
                {
                    foreach($getcats as $catitem)
                    {
                        if(strlen($catitem['level'])==5)$inden = "----";
                        else if(strlen($catitem['level'])==8)$inden = "--------";
                        else $inden = "";
                        echo "<option value=\"".$catitem['id']."\">".$inden.stripslashes($catitem['name'])."</option>";
                    }
                }
                ?>
                </select> <a href="javascript:void(0);" class="add_button" onclick="selectFiles();"> Thêm ảnh </a>
            </div>
            <div class="message">
                <?=implode("<br>",$_mess)?>
            </div>
            <!--div class="arow">
                <label>Tiêu đề</label><input type="text" name="title[]" />
                <label>Ảnh</label><input type="text" name="image[]" class="previewable select_image" />
                <a href="javascript:void(0);" class="del_button" onclick="removearow(this);">Xóa</a>
            </div-->
        
        
        </div>
        </form>
    </div>
</div>
 


<?php












?>