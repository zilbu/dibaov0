<?php if(!defined('bcms'))die('Cannot access directly!'); ?>
<?php
//config module
$tblname = $_REQUEST['tblname'];
$idfield = 'id';
$configarrray = array();
//get some array for select elelment

$activeArray = array();
$getActive = get_all("active","","");
if(count($getActive))
{
    foreach($getActive as $aitem)
    {
        $activeArray[] = $aitem['id'].":$inden".stripslashes($aitem['status']);
    }
}
$activeArray = ":--".$strSelect."--;".implode(";",$activeArray);


$priceArray = array();
$getPrice = get_all("articles","","");
if(count($getPrice))
{
    foreach($getPrice as $aitem)
    {
        $priceArray[] = $aitem['id'].":$inden".stripslashes($aitem['title']);
    }
}
$priceArray = ":--".$strSelect."--;".implode(";",$priceArray);



$levelArray = array();
$getLevel = get_all("level","","");
if(count($getLevel))
{
    foreach($getLevel as $aitem)
    {
        $levelArray[] = $aitem['level'].":$inden".stripslashes($aitem['name']);
    }
}
$levelArray = ":--".$strSelect."--;".implode(";",$levelArray);

$avdproductsArray = array();
$getAvdproducts = get_all("avdproducts","","");
if(count($getAvdproducts))
{
    foreach($getAvdproducts as $aitem)
    {
        $avdproductsArray[] = $aitem['id'].":$inden".stripslashes($aitem['type']);
    }
}
$avdproductsArray = ":--".$strSelect."--;".implode(";",$avdproductsArray);

$advertisingArray = array();
$getAdvertising = get_all("advertising","","");
if(count($getAdvertising))
{
    foreach($getAdvertising as $aitem)
    {
        $advertisingArray[] = $aitem['id'].":$inden".stripslashes($aitem['type_ad']);
    }
}
$advertisingArray = ":--".$strSelect."--;".implode(";",$advertisingArray);


$langArray = array();
$getLang = get_all("languages",""," id asc ");
if(count($getLang))
{
    foreach($getLang as $aitem)
    {
        $langArray[] = $aitem['lang'].":$inden".stripslashes($aitem['name']);
    }
}
$langArray = ":--".$strSelect."--;".implode(";",$langArray);

$edittype = 'detailedit';
if($_GET['type'] == 'quickedit') $edittype = "quickedit";
$have_cat = table_exists($tblname."_cat");

$catarraystring = array();
    $getcats = get_all($tblname."_cat",""," level asc ");
    if(count($getcats))
    {
        foreach($getcats as $catitem)
        {
            if(strlen($catitem['level'])==5)$inden = "-- ";
            else if(strlen($catitem['level'])==8)$inden = "---- ";
            else $inden = "";
            $catarraystring[] = $catitem['id'].":$inden".stripslashes($catitem['name']);
        }
    }
    $scatarraystring = ":--".$strSelect."--;".implode(";",$catarraystring);
    $catarraystring = implode(";",$catarraystring);

 if($tblname == 'ranges')
{
    /** 
    Nhóm-category-select_data-none-none-no-price_range_cat.name-price_range_cat.id
    Từ (VNĐ)-pfrom-text-60-none-no-none-none
    Đến (VNĐ)-pto-text-60-none-no-none-none
    Log time-log-time-none-none-yes-none-none
    Ngôn ngữ-lang-select-none-none-no-languages.code-languages.id
     **/
    $idfield = 'id';//{name:'title', index:'title', editable : true,edittype:'text',editoptions: {style:'height:19px'}}, 
    $viewarray = array('id','category','pfrom','pto','active','lang');
    $titlearray = array('ID','Nhóm','Từ','Đến','trạng thái','Ngôn ngữ');
    $configarrray = array(  'id'=>array("index"=>"id","width"=>50),
							'category'  =>array("editable"=>"true",
                                            "width" => "150",
                                            "index"    =>"category",
                                            "stype"     => "select",
                                            "editoptions"=>array('value'=>$scatarraystring),
                                            "searchoptions" => array('value'=>$scatarraystring),
                                            "edittype" =>"select"),
                            'pfrom'=>     array("index"    =>"pfrom","edittype"=>"text",
                                            "editable" => "true",
                                            "index"    =>"price",
                                            "width" => 100,
                                            "align" => 'right',
                                         //   "formatter" => "currencyFmatter",
                                            "editoptions" => array("style"=>"height:19px","maxlen"=>"15")),
                            'pto'=>     array("index"    =>"pto","edittype"=>"text",
                                            "editable" => "true",
                                            "index"    =>"price",
                                            "width" => 100,
                                            "align" => 'right',
                                           // "formatter" => "currencyFmatter",
                                            "editoptions" => array("style"=>"height:19px","maxlen"=>"15")),
							'active'      =>array("index"=>"active",
                                                "stype"     => "select",
												"editable"=>"true",
												"width" => 70,
												"edittype" => "select",
												"searchoptions" => array('value'=>$activeArray),
												"editoptions" => array('value'=>$activeArray)),
							'lang'=>     array("index"    =>"lang","width" => 70)
					   );
					   
		$quicklink = array(array("title"=>"Chế độ sửa nhanh","class"=>"edit_button","link"=>"index.php?module=quickedit&tblname=ranges&editon=select"),
		array("title"=>"Chế độ xem thường","class"=>"edit_button","link" => "index.php?module=quickedit&tblname=ranges"));					   

}

else if($tblname == "booking")
{
    /*
	Mr/Mrs -sex-text-60-none-no-none-none
	First name -firstname-text-60-none-no-none-none
	Surname -surname-text-60-none-no-none-none
	Quốc tịch -nationality-text-60-none-no-none-none
	Điện thoại -phone-text-60-none-no-none-none
	Email -email-text-60-none-no-none-none
	Địa chỉ -address-text-60-none-no-none-none
	Ngày đi-start-text-60-none-no-none-none
	Ngày về-end-text-60-none-no-none-none
	Loại phòng-typeroom-text-60-none-no-none-none
	Loại tour-kindstourism-text-60-none-no-none-none
	Loại khách sạn -typehotel-text-60-none-no-none-none
	Người lớn-adults-text-60-none-no-none-none
	Trẻ em-children-text-60-none-no-none-none
	Trẻ sơ sinh-infant-text-60-none-no-none-none
	Comments -comments-textarea-none-none-yes-none-none
	Ngày đăng-log-time-none-none-yes-none-none
	Trạng thái-active-select-none-none-no-active.status-active.id
	Ngôn ngữ-lang-select-none-none-no-languages.code-languages.id     
	*/
    $idfield = 'id';//{name:'title', index:'title', editable : true,edittype:'text',editoptions: {style:'height:19px'}}, 
    $viewarray = array('id','sex','firstname','surname','nationality','phone','email','address','log','lang','active');
    $titlearray = array('ID','Ông/Bà','First name','Surname','Quốc tịch','Điện thoại','Email','Địa chỉ','Thời gian','Ngôn ngữ','Trạng thái');
    $configarrray = array(  'id'=>array("index"=>"id","width"=>50),
                            'sex'=>     array("index"    =>"sex","width"=>"200","search"=>"true"),
                            'firstname'=>     array("index"    =>"firstname","width"=>"200","search"=>"true"),
                            'surname'=>     array("index"    =>"surname","width"=>"200","search"=>"true"),
                            'nationality'=>     array("index"    =>"nationality","width"=>"200","search"=>"true"),
                            'phone'=>     array("index"    =>"phone","width"=>"200","search"=>"true"),
                            'email'=>     array("index"    =>"email","width"=>"200","search"=>"true"),
                            'address'=>     array("index"    =>"address","width"=>"200","search"=>"true"),
							 'log'=>     array("index"    =>"log"),
                            'lang'=>     array("index"    =>"lang"),
                            'active'      =>array("index"=>"active",
                                                "stype"     => "select",                            
                                                "searchoptions" => array('value'=>$activeArray))
                        );
    $edittype = 'detailedit';
}
else if($tblname == 'properties')
{
    /*
	Tên hãng-name-text-40-none-no-none-none
	Image-image-imageonserver-60-none-yes-none-none
	Mô tả chi tiết-description-text-60-none-no-none-none
	Địa chỉ-address-text-60-none-no-none-none
	Log time-log-time-none-none-yes-none-none
	Ngôn ngữ-lang-select-none-none-no-languages.code-languages.id
     */ 
    $idfield = 'id';//{name:'title', index:'title', editable : true,edittype:'text',editoptions: {style:'height:19px'}}, 
    $viewarray = array('id','name','category','image','description','level','active','log');
    $titlearray = array('ID','Tên','Loại','Hình ảnh','Mô tả','Mức ưu tiên','Kích hoạt','Thời gian');
    $configarrray = array(  'id'=>array("index"=>"id","width"=>"30"),
                            'name'=>     array("index"    =>"name","width"=>"100","editable"=>"true"),
							'category'  =>array("editable"=>"true",
                                            "width" => "150",
                                            "index"    =>"category",
                                            "stype"     => "select",
                                            "editoptions"=>array('value'=>$scatarraystring),
                                            "searchoptions" => array('value'=>$scatarraystring),
                                            "edittype" =>"select"),
                            'image'=>     array("index"    =>"image","search"=>"false","formatter"=>"imgView"),
                            'description'=>     array("index"    =>"description","editable"=>"true","edittype"=>"textarea"),
                            'level'=>     array("index"    =>"level","width"=>"50","editable"=>"true"),
                            'active'=>     array("index"    =>"active",
                                                "stype"=>"select",
                                                "searchoptions"=>array("value"=>$activeArray),
                                                "edittype"=>"select",
                                                "editable"=>"true",
                                                "editoptions"=>array("value"=>$activeArray),"width"=>"50"),
                            'log'=>     array("index"    =>"log",
                                            "search" => "false")
                        );
    $quicklink = array(array("title"=>"Chế độ sửa nhanh","class"=>"edit_button","link"=>"index.php?module=quickedit&tblname=manufacturers&editon=select"),
	array("title"=>"Chế độ xem thường","class"=>"edit_button","link" => "index.php?module=quickedit&tblname=manufacturers"));
}
else if($tblname == "khuyenmai")
{
    /*
		Title-name-text-60-none-no-none-none
		Url chi tiết-url-time-none-none-yes-none-none
		Log time-log-time-none-none-yes-none-none
		Level-level-select-none-none-no-level.name-level.level
		Language-lang-select-none-none-no-languages.code-languages.id
		Active-active-select-none-none-no-active.status-active.id
     */
    $idfield = 'id';//{name:'title', index:'title', editable : true,edittype:'text',editoptions: {style:'height:19px'}}, 
    $viewarray = array('id','name','url','level','log','lang');
    $titlearray = array('ID','Tiêu đề','Vị trí hiển thị','Mức ưu tiên','Thời gian','Ngôn ngữ');
    $configarrray = array(  'id'=>array("index"=>"id","width"=>50),
                            'name'=>     array("index"    =>"name","width"=>"200","search"=>"false"),
							'url'=>     array("index"    =>"url","width"=>"300"),
							'level'=>     array("index"    =>"level",
                                                "stype"     => "select",
                                                "searchoptions" => array('value'=>$levelArray)),
							'log'=>     array("index"    =>"log","width"=>"300"),
                            'lang'=>     array("index"    =>"lang")
                        );
    $edittype = 'detailedit';
}
else if($tblname == "menu")
{
    /*
    Tiêu đề-name-text-60-none-no-none-none
	Level-level-select-none-none-no-level.name-level.level
	Log time-log-time-none-none-yes-none-none
	Language-lang-select-none-none-no-languages.code-languages.id
	Active-active-select-none-none-no-active.status-active.id
     */
    $idfield = 'id';//{name:'title', index:'title', editable : true,edittype:'text',editoptions: {style:'height:19px'}}, 
    $viewarray = array('id','name','position','limit1','level','log','lang');
    $titlearray = array('ID','Tiêu đề','Vị trí hiển thị','Số lượng ','Mức ưu tiên','Thời gian','Ngôn ngữ');
    $configarrray = array(  'id'=>array("index"=>"id","width"=>50),
                            'name'=>     array("index"    =>"name","width"=>"200","search"=>"false"),
							'limit1'=>     array("index"    =>"limit1","width"=>"300"),
							'position'=>     array("index"    =>"position",
                                                "stype"     => "select",
                                                "searchoptions" => array('value'=>$positionArray)),
							'level'=>     array("index"    =>"level",
                                                "stype"     => "select",
                                                "searchoptions" => array('value'=>$levelArray)),
							'log'=>     array("index"    =>"log","width"=>"300"),
                            'lang'=>     array("index"    =>"lang")
                        );
    $edittype = 'detailedit';
}

else if($tblname == "locationitem")
{
    /*
    Tiêu đề-title-text-60-none-no-none-none
	Địa điểm-location-select-none-none-no-location.name-location.id
	Địa chỉ-address-textarea-none-none-yes-none-none
	Log time-log-time-none-none-yes-none-none
	Level-level-select-none-none-no-level.name-level.level
	Language-lang-select-none-none-no-languages.code-languages.id
	Active-active-select-none-none-no-active.status-active.id
     */
    $idfield = 'id';//{name:'title', index:'title', editable : true,edittype:'text',editoptions: {style:'height:19px'}}, 
    $viewarray = array('id','title','location','log','level','lang','active');
    $titlearray = array('ID','Tiêu đề','Vị trí','Thời gian đăng','Mức ưu tiên','Ngôn ngữ','trạng thái');
    $configarrray = array(  'id'=>array("index"=>"id","width"=>50),
                            'title'=>     array("index"    =>"title","width"=>"200","search"=>"false"),
							'location'=>     array("index"    =>"location","width"=>"300"),
							'log'=>     array("index"    =>"log","width"=>"300"),
							'level'=>     array("index"    =>"level",
                                                "stype"     => "select",
                                                "searchoptions" => array('value'=>$levelArray)),
                            'lang'=>     array("index"    =>"lang"),
                            'active'=>     array("index"    =>"active")
                        );
    $edittype = 'detailedit';
}
else if($tblname == "comment")
{
    /*
    Tiêu đề-title-text-60-none-no-none-none
	Địa điểm-location-select-none-none-no-location.name-location.id
	Địa chỉ-address-textarea-none-none-yes-none-none
	Log time-log-time-none-none-yes-none-none
	Level-level-select-none-none-no-level.name-level.level
	Language-lang-select-none-none-no-languages.code-languages.id
	Active-active-select-none-none-no-active.status-active.id
     */
    $idfield = 'id';//{name:'title', index:'title', editable : true,edittype:'text',editoptions: {style:'height:19px'}}, 
    $viewarray = array('id','title','messenger','email','log','level','lang','active');
    $titlearray = array('ID','Tiêu đề','Nội dung','Email','Thời gian đăng','Mức ưu tiên','Ngôn ngữ','trạng thái');
    $configarrray = array(  'id'=>array("index"=>"id","width"=>50),
                            'title'=>     array("index"    =>"title","width"=>"200","search"=>"true"),
							'name'=>     array("index"    =>"name","width"=>"300"),
							'email'=>     array("index"    =>"email","width"=>"300"),
							'log'=>     array("index"    =>"log","width"=>"300"),
							'level'=>     array("index"    =>"level",
                                                "stype"     => "select",
                                                "searchoptions" => array('value'=>$levelArray)),
                            'lang'=>     array("index"    =>"lang"),
                            'active'=>     array("index"    =>"active")
                        );
    $edittype = 'detailedit';
}
else if($tblname == "emailcustomer")
{
    /*
    Tên Email-email-text-60-none-no-none-none
	Log time-log-time-none-none-yes-none-none
	Level-level-select-none-none-no-level.name-level.level
	Language-lang-select-none-none-no-languages.code-languages.id
	Active-active-select-none-none-no-active.status-active.id
     */
    $idfield = 'id';//{name:'title', index:'title', editable : true,edittype:'text',editoptions: {style:'height:19px'}}, 
    $viewarray = array('id','email','title','log','level','lang','active');
    $titlearray = array('ID','Email','Tiêu đề','Thời gian đăng','Mức ưu tiên','Ngôn ngữ','trạng thái');
    $configarrray = array(  'id'=>array("index"=>"id","width"=>50),
							'email'=>     array("index"    =>"email","width"=>"300"),
							'title'=>     array("index"    =>"title","width"=>"300"),
							'log'=>     array("index"    =>"log","width"=>"300"),
							'level'=>     array("index"    =>"level",
                                                "stype"     => "select",
                                                "searchoptions" => array('value'=>$levelArray)),
                            'lang'=>     array("index"    =>"lang"),
                            'active'=>     array("index"    =>"active")
                        );
    $edittype = 'detailedit';
}
else if($tblname == "hoidap")
{
    /*
    Tiêu đề-title-text-60-none-no-none-none
	Địa điểm-location-select-none-none-no-location.name-location.id
	Địa chỉ-address-textarea-none-none-yes-none-none
	Log time-log-time-none-none-yes-none-none
	Level-level-select-none-none-no-level.name-level.level
	Language-lang-select-none-none-no-languages.code-languages.id
	Active-active-select-none-none-no-active.status-active.id
     */
    $idfield = 'id';//{name:'title', index:'title', editable : true,edittype:'text',editoptions: {style:'height:19px'}}, 
    $viewarray = array('id','title','messenger','email','log','level','lang','active');
    $titlearray = array('ID','Tiêu đề','Nội dung','Email','Thời gian đăng','Mức ưu tiên','Ngôn ngữ','trạng thái');
    $configarrray = array(  'id'=>array("index"=>"id","width"=>50),
                            'title'=>     array("index"    =>"title","width"=>"200","search"=>"true"),
							'name'=>     array("index"    =>"name","width"=>"300"),
							'email'=>     array("index"    =>"email","width"=>"300"),
							'log'=>     array("index"    =>"log","width"=>"300"),
							'level'=>     array("index"    =>"level",
                                                "stype"     => "select",
                                                "searchoptions" => array('value'=>$levelArray)),
                            'lang'=>     array("index"    =>"lang"),
                            'active'=>     array("index"    =>"active")
                        );
    $edittype = 'detailedit';
}
else if($tblname == "gallerys")
{
    /** 
   
	Tiêu đề-title-text-60-none-no-none-none
	Hình ảnh-url-imageonserver-60-none-yes-none-none
	Ngày đăng-time-time-none-none-yes-none-none
	Cấp độ-level-select-none-none-no-level.name-level.level
	Ngôn ngữ-lang-select-none-none-no-languages.code-languages.id
	Trạng thái-active-select-none-none-no-active.status-active.id
     **/
    $idfield = 'id';//{name:'title', index:'title', editable : true,edittype:'text',editoptions: {style:'height:19px'}}, 
    $viewarray = array('id','title','url','time','level','lang','active');
    $titlearray = array('ID','Tiêu đề','Hình ảnh','Thời gian','Cấp độ','Ngôn ngữ','Trạng thái');
    $configarrray = array(  'id'=>array("index"=>"id","width"=>70),
                            'title'=>     array("index"    =>"title","width"=>"200","editable"=>"true","search"=>"false"),
                            'url'=>     array("index"    =>"url",
                                                "search" => "false",
												"editable"=>"false",
                                                "formatter"=>"imgView",
                                                "width"=>"70"),	
                            'time'=>     array("index"    =>"time","editable"=>"true","width"=>"300"),
							 'level'=>     array("index"    =>"level",
												"width"=>"100",
												"stype"     => "select",
												"editable"=>"true",
												"edittype" => "select",
												"searchoptions" => array('value'=>$levelArray),
												"editoptions" => array('value'=>$levelArray)
												),
							 'lang'=>     array("index"    =>"lang"),
                             'active'      =>array(
												"editable"=>"true",
												"index"=>"active",
												"width"=>"60",
                                                "stype"     => "select",                            
                                                "searchoptions" => array('value'=>$activeArray),
												"edittype" =>"select",	
												"editoptions"=>array("style"=>"height:19px",
                                                                "value"=>$activeArray
                                                                ))
                        );
    $additionlink = array(array("link"=>"/adcms/index.php?module=quickadd&tblname=".$tblname,"title"=>"Thêm nhanh","class"=>"add_button"));
   // $edittype = 'detailedit';
}
else if($tblname == 'weblinks')
{
    $idfield = 'id';//{name:'title', index:'title', editable : true,edittype:'text',editoptions: {style:'height:19px'}}, 
    $viewarray = array('id','org_name','org_web','image','level','advertising','active');
    $titlearray = array('ID','Tiêu đề','Liên kết','Hình ảnh','Mức ưu tiên','Vị trí','Trạng thái');
    $configarrray = array(  'id'=>array("index"=>"id","width"=>50),
                            'org_name'=>     array("index"    =>"org_name", "width"=>"100"),
                            'org_web'=>     array("index"    =>"org_web","formatter"=>"linkUrl"),
                            'image'=>     array("index"    =>"image",
                                                "formatter"=>"imgView",
                                                "width"=>"50",
                                                "search"=>"false"),
                            'level'=>     array("index"    =>"level"),
                            'advertising'      =>array("index"=>"advertising",
												"width"=>"120",
                                                "stype"     => "select",
												"editable"=>"true",
                                                "searchoptions" => array('value'=>$advertisingArray)
												),
                            'active'      =>array("index"=>"active",
                                                "stype"     => "select",                            
                                                "searchoptions" => array('value'=>$activeArray)
                                                )
                        );
    $edittype = 'detailedit';
}
else if($tblname == 'news' or $tblname == 'articles' or $tblname == 'hotel' or $tblname == 'menus')
{
    $idfield = 'id';//{name:'title', index:'title', editable : true,edittype:'text',editoptions: {style:'height:19px'}}, 
    $viewarray = array('id','title','price','category','image','level','log','lang','active');
    $titlearray = array('ID','Tiêu đề','Giá bán','Nhóm','Ảnh','Mức ưu tiên','Thời gian nhập','Ngôn ngữ','Trạng thái');
    $configarrray = array(  'id'=>array("index"=>"id","width"=>50),
                            'title'=>     array("index"    =>"title","width"=>"300"),                            'price'=>     array("index"    =>"price","width"=>"300"),
                            'category'=>     array("index"    =>"category",
													//"search"=>"false",
													 "searchoptions" => array('value'=>$scatarraystring)
													),
                            'image'=>     array("index"    =>"image",
                                                "search" => "false",
                                                "formatter"=>"imgView",
                                                "width"=>"50"),
                            'level'=>     array("index"    =>"level"),
                            'log'=>     array("index"    =>"log"),
                            'lang'=>     array("index"    =>"lang"),
                            'active'      =>array("index"=>"active",
                                                "stype"     => "select",                            
                                                "searchoptions" => array('value'=>$activeArray))
                        );
    $edittype = 'detailedit';
}
else if($tblname == 'banxe')
{
    $idfield = 'id';//{name:'title', index:'title', editable : true,edittype:'text',editoptions: {style:'height:19px'}}, 
    $viewarray = array('id','description','title','code','keywords','level','log','lang','active');
    $titlearray = array('ID','Tên chủ mua xe','Tên xe','Số khung','Ngày bán','Mức ưu tiên','Thời gian nhập','Ngôn ngữ','Trạng thái');
    $configarrray = array(  'id'=>array("index"=>"id","width"=>50),
                            'description'=>     array("index"    =>"description","width"=>"300"),
                            'title'=>     array("index"    =>"title","width"=>"300"),
                            'code'=>     array("index"    =>"code","width"=>"300"),
                            'keywords'=>     array("index"    =>"keywords","width"=>"300"),
                            'level'=>     array("index"    =>"level"),
                            'log'=>     array("index"    =>"log"),
                            'lang'=>     array("index"    =>"lang"),
                            'active'      =>array("index"=>"active",
                                                "stype"     => "select",                            
                                                "searchoptions" => array('value'=>$activeArray))
                        );
    $edittype = 'detailedit';
}

else if($tblname == 'categorys')
{
    $idfield = 'id';//{name:'title', index:'title', editable : true,edittype:'text',editoptions: {style:'height:19px'}}, 
    $viewarray = array('id','title','category','area','service','discount','minimum_price','image_banner','log','lang','active');
    $titlearray = array('ID','Tiêu đề','Nhóm SP','Khu vực','Phí dịch vụ','Giá giảm','Mức giá tối thiểu','Banner','Thời gian nhập','Ngôn ngữ','Trạng thái');
    $configarrray = array(  'id'=>array("index"=>"id","width"=>50),
                            'title'=>     array("index"    =>"title","width"=>"300"),
                            'category'=>     array("index"    =>"category",
													"search"=>"false",
													"searchoptions" => array('value'=>$activeArray)
													),
							'area'=>     array("index"    =>"area","width"=>"80" ),
							'service'=>     array("index"    =>"service","width"=>"90" ),
							'discount'=>     array("index"    =>"discount","width"=>"90" ),
							'minimum_price'=>     array("index"    =>"minimum_price","width"=>"130" ),
                            'image_banner'=>     array("index"    =>"image_banner",
                                                "search" => "false",
                                                "formatter"=>"imgView",
                                                "width"=>"60"),
                            'log'=>     array("index"    =>"log"),
                            'lang'=>     array("index"    =>"lang"),
                            'active'      =>array("index"=>"active",
                                                "stype"     => "select",                            
                                                "searchoptions" => array('value'=>$activeArray))
                        );
    $edittype = 'detailedit';
}
else if($tblname == 'articles_cat')
{
    $idfield = 'id';//{name:'title', index:'title', editable : true,edittype:'text',editoptions: {style:'height:19px'}}, 
    $viewarray = array('id','name','image','banner','banner_menu','log','lang','active');
    $titlearray = array('ID','Tên','Banner main','Banner chuyên mục','Background menu','Thời gian nhập','Ngôn ngữ','Trạng thái');
    $configarrray = array(  'id'=>array("index"=>"id","width"=>50),
                            'name'=>     array("index"    =>"title","width"=>"200"),
                            'image'=>     array("index"    =>"image_banner",
                                                "search" => "false",
                                                "formatter"=>"imgView",
                                                "width"=>"60"),
							'banner'=>     array("index"    =>"banner",
                                                "search" => "false",
                                                "formatter"=>"imgView",
                                                "width"=>"60"),
							'banner_menu'=>     array("index"    =>"banner_menu",
                                                "search" => "false",
                                                "formatter"=>"imgView",
                                                "width"=>"60"),
                            'log'=>     array("index"    =>"log"),
                            'lang'=>     array("index"    =>"lang"),
                            'active'      =>array("index"=>"active",
                                                "stype"     => "select",                            
                                                "searchoptions" => array('value'=>$activeArray))
                        );
    $edittype = 'detailedit';
}
else if($tblname == "site")
{
    $idfield = 'id';//{name:'title', index:'title', editable : true,edittype:'text',editoptions: {style:'height:19px'}}, 
    $viewarray = array('id','sitename','siteurl','description','keywords','lang');
    $titlearray = array('ID','Tên site','Link site','Description(Meta)','Keyword(Meta)','WEB Icon','Ngôn ngữ');
    $configarrray = array(  'id'=>array("index"=>"id","width"=>50),
                            'sitename'=>     array("index"    =>"sitename","width"=>"200","search"=>"false"),
                            'siteurl'=>     array("index"    =>"siteurl","search"=>"false","width"=>"200"),
                            'description'=>     array("index"    =>"description","width"=>"300"),
                            'keywords'=>     array("index"    =>"keywords",
                                                "width"=>"300"),
                            'icon'=>     array("index"    =>"icon",
                                                "search" => "false",
                                                "formatter"=>"imgView",
                                                "width"=>"60"),
                            'lang'=>     array("index"    =>"lang")
                        );
    $edittype = 'detailedit';
}
else if($tblname == "support_online")
{
    $idfield = 'id';//{name:'title', index:'title', editable : true,edittype:'text',editoptions: {style:'height:19px'}}, 
    $viewarray = array('id','support_name','nick_name','type_name','support_phone','support_email','active','lang');
    $titlearray = array('ID','Tên','Nick','Type','Số điện thoại','Email','Trạng thái','Ngôn ngữ');
    $configarrray = array(  'id'=>array("index"=>"id","width"=>50),
                            'support_name'=>     array("index"    =>"support_name","width"=>"200","search"=>"false"),
                            'nick_name'=>     array("index"    =>"nick_name","width"=>"200"),
							'type_name'=>     array("index"    =>"type_name"),
                            'support_phone'=>     array("index"    =>"support_phone","width"=>"300"),
                            'support_email'=>     array("index"    =>"support_email","width"=>"300"),
                            'active'=>     array("index"    =>"active"),
                            'lang'=>     array("index"    =>"lang")
                        );
    $edittype = 'detailedit';
}
else if($tblname == "invoices")
{
    /*
     Tên khách hàng-fullname-text-60-none-no-none-none
	Địa chỉ-address-text-60-none-no-none-none
	Email-email-text-60-none-no-none-none
	Điện thoại-phone-text-60-none-no-none-none
	Thanh toán-payment-text-60-none-no-none-none
	Ngày gửi-datetime-label-none-none-yes-none-none
	Sản phẩm-products-textarea-none-none-yes-none-none
	Thông tin-content-textarea-none-none-yes-none-none
	Ngôn ngữ-lang-select-none-none-no-languages.code-languages.id
     */
    $idfield = 'id';//{name:'title', index:'title', editable : true,edittype:'text',editoptions: {style:'height:19px'}}, 
    $viewarray = array('id','fullname','address','email','phone','payment','log','products','content');
    $titlearray = array('ID','Tên','Địa chỉ','Email','Số điện thoại','Phương thức thanh toán','Thời gian','Sản phẩm','Nội dung');
    $configarrray = array(  'id'=>array("index"=>"id","width"=>50,"search"=>"false"),
                            'fullname'=>     array("index"    =>"fullname","width"=>"200"),
                            'address'=>     array("index"    =>"address","width"=>"200"),
                            'email'=>     array("index"    =>"email","width"=>"250"),
                            'phone'=>     array("index"    =>"phone","width"=>"200"),
                            'payment'=>     array("index"    =>"payment","width"=>"200"),
                            'log'=>     array("index"    =>"log","width"=>"130"),
                            'products'=>     array("index"    =>"products","width"=>"450"),
                            'content'=>     array("index"    =>"content","width"=>'300')
                        );
			$phpExcel = array(array("title"=>"Tạo file Excel","class"=>"edit_button","link"=>"PHPExcel/download-xls.php?tblname=".$tblname.""));
}
else if($tblname == "contact")
{
    $idfield = 'id';//{name:'title', index:'title', editable : true,edittype:'text',editoptions: {style:'height:19px'}}, 
    $viewarray = array('id','fullname','address','phone','email','log','content','lang');
    $titlearray = array('ID','Tên','Địa chỉ','Số điện thoại','Email','Thời gian','Nội dung','Ngôn ngữ');
    $configarrray = array(  'id'=>array("index"=>"id","width"=>50),
                            'fullname'=>     array("index"    =>"fullname","width"=>"200","search"=>"false"),
                            'address'=>     array("index"    =>"address","width"=>"200"),
                            'phone'=>     array("index"    =>"phone","width"=>"300"),
                            'email'=>     array("index"    =>"email","width"=>"300"),
                            'log'=>     array("index"    =>"log","width"=>"300"),
                            'content'=>     array("index"    =>"content","width"=>'500'),
                            'lang'=>     array("index"    =>"lang")
                        );
    $edittype = 'detailedit';
}
else if($tblname == "videos")
{
    /** 
    Tiêu đề-title-text-60-none-no-none-none
	Url video-url_video-text-60-none-no-none-none
	Ngày đăng-time-time-none-none-yes-none-none
	Cấp độ-level-select-none-none-no-level.name-level.level
	Language-lang-select-none-none-no-languages.code-languages.id
	Trạng thái-active-select-none-none-no-active.status-active.id
     **/
    $idfield = 'id';//{name:'title', index:'title', editable : true,edittype:'text',editoptions: {style:'height:19px'}}, 
    $viewarray = array('id','title','url_video','time','level','lang','active');
    $titlearray = array('ID','Tiêu đề','ID video','Thời gian','Cấp độ','Ngôn ngữ','Trạng thái');
    $configarrray = array(  'id'=>array("index"=>"id","width"=>70),
                            'title'=>     array("index"    =>"title","width"=>"200","search"=>"false"),
                            'url_video'=>     array("index"    =>"url_video"),
                            'time'=>     array("index"    =>"time","width"=>"300"),
                            'level'=>     array("index"    =>"level",
                                                "width"=>"300"),
                            'lang'=>     array("index"    =>"lang"),
							'active'      =>array("index"=>"active")
                        );
    $edittype = 'detailedit';
}
else if($tblname == 'slides')
{
    /** 
    Tên dự án-title-text-60-none-no-none-none
Giới thiệu dự án-content-textarea-none-none-yes-none-none
Hình ảnh-image-imageonserver-60-none-yes-none-none
Ngày đăng-log-time-none-none-yes-none-none
Cấp độ-level-select-none-none-no-level.name-level.level
Ngôn ngữ-lang-select-none-none-no-languages.code-languages.id
Trạng thái-active-select-none-none-no-active.status-active.id
     **/
    $idfield = 'id';//{name:'title', index:'title', editable : true,edittype:'text',editoptions: {style:'height:19px'}}, 
    $viewarray = array('id','org_name','org_web','image','level','log','lang','active');
    $titlearray = array('ID','Tiêu đề','Liên kết','Image','Mức ưu tiên','Thời gian nhập','Ngôn ngữ','Trạng thái');
    $configarrray = array(  'id'=>array("index"=>"id","width"=>50),
                            'org_name'=>     array("index"    =>"org_name","width"=>"250"),
                            'org_web'=>     array("index"    =>"org_web","width"=>"250","formatter"=>"linkUrl"),
                            'image'=>     array("index"    =>"image",
                                                "search" => "false",
                                                "formatter"=>"imgView",
                                                "width"=>"70"),	
                            'level'=>     array("index"    =>"level",
                                                "stype"     => "select",
                                                "searchoptions" => array('value'=>$levelArray)),
                            'log'=>     array("index"    =>"log"),
                            'lang'=>     array("index"    =>"lang"),
                            'active'      =>array("index"=>"active",
                                                "stype"     => "select",                            
                                                "searchoptions" => array('value'=>$activeArray))
                        );						
}
else if($tblname == 'avdproducts')
{
	/*
	Tiêu đề-type-textarea-none-none-yes-none-none
	Vị trí -position-checkbox-none-none-no-position.position-position.id
	Mức ưu tiên -order-select_data-none-none-no-faqs_cat.name-faqs_cat.id
	Số sản phẩm hiển thị-number-textarea-none-none-yes-none-none
	Trạng thái-active-select-none-none-no-active.status-active.id
	Ngôn ngữ-lang-select-none-none-no-languages.code-languages.id
	*/
    $idfield = 'id';//{name:'title', index:'title', editable : true,edittype:'text',editoptions: {style:'height:19px'}}, 
    $viewarray = array('id','title','position','priority','number','lang','active');
    $titlearray = array('ID','Tiêu đề','Vị trí','Mức ưu tiên','Số sản phẩm hiển thị','Ngôn ngữ','Trạng thái');
    $configarrray = array(  'id'=>array("index"=>"id","width"=>50),
                            'title'=>     array("index"    =>"title","width"=>"300"),
							'position'=>     array("index"    =>"position",
                                                "stype"     => "select",
                                                "searchoptions" => array('value'=>$positionArray)),
							'priority'=>     array("index"    =>"priority","width"=>"300"),
							'number'=>     array("index"    =>"number","width"=>"300"),
                            'lang'=>     array("index"    =>"lang"),
                            'active'      =>array("index"=>"active",
                                                "stype"     => "select",                            
                                                "searchoptions" => array('value'=>$activeArray))
                        );
    $edittype = 'detailedit';
}

else if($tblname == "rate")
{
    $idfield = 'id';//{name:'title', index:'title', editable : true,edittype:'text',editoptions: {style:'height:19px'}}, 
    $viewarray = array('id','fullname','address','phone','email','rate','log','active','lang');
    $titlearray = array('ID','Tên','Địa chỉ','Số điện thoại','Email','Điểm đánh giá','Thời gian','Trạng thái','Ngôn ngữ');
    $configarrray = array(  'id'=>array("index"=>"id","width"=>50),
                            'fullname'=>     array("index"    =>"fullname","width"=>"200","search"=>"false"),
                            'address'=>     array("index"    =>"address","width"=>"200"),
                            'phone'=>     array("index"    =>"phone","width"=>"300"),
                            'email'=>     array("index"    =>"email","width"=>"300"),
							'rate'=>     array("index"    =>"rate","width"=>'500'),
                            'log'=>     array("index"    =>"log","width"=>"300"),
							'active'      =>array("index"=>"active",
								"stype"     => "select",                            
								"searchoptions" => array('value'=>$activeArray)),
                            'lang'=>     array("index"    =>"lang")
							
                        );
    $edittype = 'detailedit';
}
else if($tblname == "tbl_sale")
{
		// echo '<pre>';
		// print_r($priceArray);
	// echo '</pre>';

    $idfield = 'id';//{name:'title', index:'title', editable : true,edittype:'text',editoptions: {style:'height:19px'}}, 
    $viewarray = array('id','name','title','price','log','active','lang');
    $titlearray = array('ID','Tên','Tiêu đều','Giá','Thời gian','Trạng thái','Ngôn ngữ');
    $configarrray = array(  'id'=>array("index"=>"id","width"=>50),
                           'name'      =>array("index"=>"name",
								"stype"     => "select",                            
								"searchoptions" => array('value'=>$priceArray)),
                            'title'=>     array("index"    =>"title","width"=>"200"),
                            'price'=>     array("index"    =>"price","width"=>"300"),
                            'log'=>     array("index"    =>"log","width"=>"300"),
							'active'      =>array("index"=>"active",
								"stype"     => "select",                            
								"searchoptions" => array('value'=>$activeArray)),
                            'lang'=>     array("index"    =>"lang")
							
                        );
    $edittype = 'detailedit';
}
else if($tblname == 'price_range')
{
    /** 
    Nhóm-category-select_data-none-none-no-price_range_cat.name-price_range_cat.id
    Từ (VNĐ)-pfrom-text-60-none-no-none-none
    Đến (VNĐ)-pto-text-60-none-no-none-none
    Log time-log-time-none-none-yes-none-none
    Ngôn ngữ-lang-select-none-none-no-languages.code-languages.id
     **/
    $idfield = 'id';//{name:'title', index:'title', editable : true,edittype:'text',editoptions: {style:'height:19px'}}, 
    $viewarray = array('id','pfrom','pto');
    $titlearray = array('ID','Từ (vnđ)','Đến (vnđ)');
    $configarrray = array(  'id'=>array("index"=>"id","width"=>50),
                            'pfrom'=>     array("index"    =>"pfrom","edittype"=>"text",
                                            "editable" => "true",
                                            "index"    =>"price",
                                            "width" => 70,
                                            "align" => 'right',
                                            "formatter" => "currencyFmatter",
                                            "editoptions" => array("style"=>"height:19px","maxlen"=>"15")),
                            'pto'=>     array("index"    =>"pto","edittype"=>"text",
                                            "editable" => "true",
                                            "index"    =>"price",
                                            "width" => 70,
                                            "align" => 'right',
                                            "formatter" => "currencyFmatter",
                                            "editoptions" => array("style"=>"height:19px","maxlen"=>"15"))
                        );

}
?>