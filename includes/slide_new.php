
<script type="text/javascript" language="javascript" src="/javascripts/jquery_008.js"></script>
	<script type="text/javascript" language="javascript">
		$(function() {
			//	Scrolled by user interaction
			$('#slider_img').carouFredSel({
				auto: true,
				prev: '#prev2',
				next: '#next2',
				pagination: "#pager2",
				scroll: 1,
				mousewheel: true,
				swipe: {
					onMouse: true,
					onTouch: true
				},
				scroll : {
					duration : 900
				},
				items: {
					visible: 5,
					minimum: 0,
					width: "variable",
					height: "variable"
				},

			});
		});
	</script>
		<ul style="" id="slider_img">		
		<?php
		$query = sql_query_data('articles',' active = 1 ','',array('level' => 'ASC'),array('id','category','title','image','brief'),0,100);
		if ( $query !='' ){
			$i = 0;
			foreach( $query as $next ){
				$title = stripslashes($next['title']);
				$id_avd = $next['id'];
				$category = $next['category'];
				$image = $next['image'];
				$brief = stripslashes($next['brief']);
				$url='/'.removeSpecialChars(removesign($title)).'-c'.$category.'a'.$id_avd.'.html';
				$i++;
				if(@getimagesize($image)){ ?>
				
				<li  class='itemp'>
					<a href="<?php echo $url; ?>" title="<?php echo $title; ?>"><img src='/<?php echo $image; ?>' alt="<?php echo $title; ?>" /></a>
					<h3><a href="<?php echo $url; ?>" title="<?php echo $title; ?>"><?php echo $title; ?></a></h3>
				</li>
				
			<?php	
				}
			}
		}
		?>	
		</ul>		