<div id="bg_cat_product">
<ul class="list">
	<?php   
	$query_cat = sql_query_data('products_cat',' showmenu like "%3%"  ','',array('level' => 'ASC'),array('id','name','urlfr','image'),0,4);
	$i=0;
	foreach($query_cat as $result)
	{
		$i++;
		$name = stripslashes($result['name']);
		$image = $result['image'];
		$url='/'.$result['urlfr'];
		?>
			<li id="cat_product" style="opacity: 1;">
				<div>
					<div id='bg_img'><a href="<?php echo $url; ?>" > </a></div>
					<a href="<?php echo $url; ?>" title="<?php echo $name; ?>" ><img src='/<?php echo $image; ?>'/></a>
					<h2><a href="<?php echo $url; ?>" title="<?php echo $name; ?>" ><?php echo $name; ?></a></h2>
				</div>
			</li>
		<?php
	}
	?>
</ul>	
 <script type="text/javascript"> 
 jQuery(document).ready(function() {
	jQuery("#bg_cat_product").delegate("#cat_product", "mouseover mouseout", function(e) {
		if (e.type == 'mouseover') {
		jQuery("#bg_cat_product li").not(this).dequeue().animate({opacity: "0.65"}, 300);
    	} 
		else {
		jQuery("#bg_cat_product li").not(this).dequeue().animate({opacity: "1"}, 300);
   		}
	});
});
 </script>
</div>

