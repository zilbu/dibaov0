		<div class="col-md-4 col-sm-4 col-xs-4">
				<div class="padding-add">
                    <div class="market-box row">
                        <div class="col-md-12 col-sm-12 col-xs-12 market">
                            <h2 class="title-block">Tin tức nổi bật</h2>
							<?php
								$data= sql_query_data("news",' active = "1"','',array('level' => 'DESC'),array('id','title','brief','category','image'),0,4);
								$i=0;
								foreach($data as $val){
								$id= $val['id'];
								$category= $val['category'];
								$title= $val['title'];
								$img= $val['image'];
								$url='/'.removeSpecialChars(removesign($title)).'-c'.$category.'n'.$id.'.html';
								$i++;
									if($i==1){
										echo '<div class="product-image">
												<a href="'.$url.'">
													<img src="/'.$img.'" alt="'.$title.'">
												</a>
											</div>
											<div class="product-info">
												<h5>
													<a title='.$title.' href="'.$url.'">'.$title.'</a>
												</h5>
												<div class="pull-right">
													<strong>
														<a href="'.$url.'" titlt="'.$title.'" class="blue">Đọc tiếp →</a>
													</strong>
												</div>
											</div>
											<ul class="othernews padding-add">';
									}else{
										echo '<li>
												<a href="'.$url.'">
													<img src="/'.$val['image'].'" alt ='.$title.'"></img>
													<p>'.$val['title'].'</p>
												</a>
											</li>';
									}
								}
							?>
							</ul>
                        </div>
						<div class="col-md-12 col-sm-12 col-xs-12 market">
                            <h2 class="title-block">Thư viện ảnh</h2>
                            <div class="main-gl">

							<script type="text/javascript" src="/style/js/jssor.core.js"></script>
							<script type="text/javascript" src="/style/js/jssor.utils.js"></script>
							<script type="text/javascript" src="/style/js/jssor.slider.js"></script>
							  <script>
							  
								jQuery(document).ready(function ($) {
									var options = {
										$AutoPlay: true,                                   //[Optional] Whether to auto play, to enable slideshow, this option must be set to true, default value is false
										$DragOrientation: 3                                //[Optional] Orientation to drag slide, 0 no drag, 1 horizental, 2 vertical, 3 either, default value is 1 (Note that the $DragOrientation should be the same as $PlayOrientation when $DisplayPieces is greater than 1, or parking position is not 0)
									};
									 var optionsup = {
										$AutoPlay: true,                                    //[Optional] Whether to auto play, to enable slideshow, this option must be set to true, default value is false
										$PlayOrientation: 2,                                //[Optional] Orientation to play slide (for auto play, navigation), 1 horizental, 2 vertical, 5 horizental reverse, 6 vertical reverse, default value is 1
										$DragOrientation: 2,                                //[Optional] Orientation to drag slide, 0 no drag, 1 horizental, 2 vertical, 3 either, default value is 1 (Note that the $DragOrientation should be the same as $PlayOrientation when $DisplayPieces is greater than 1, or parking position is not 0)

										$ArrowNavigatorOptions: {
											$Class: $JssorArrowNavigator$,              //[Requried] Class to create arrow navigator instance
											$ChanceToShow: 2,                               //[Required] 0 Never, 1 Mouse Over, 2 Always
											$AutoCenter: 1,                                 //[Optional] Auto center arrows in parent container, 0 No, 1 Horizontal, 2 Vertical, 3 Both, default value is 0
											$Steps: 1                                       //[Optional] Steps to go for each navigation request, default value is 1
										}
									};
									var jssor_slider1 = new $JssorSlider$("slider1_container", options);
									var jssor_sliderup = new $JssorSlider$("slider1_container_up", optionsup);
									
								});
							</script>
							<?php
								$data= sql_query_data("gallerys_cat",'1=1','',array('level' => 'DESC'),array('id','image','urlfr'),0,3);
								$i=0;
								foreach($data as $val){
									$i++;
									if($i == 1){
										echo '<div class="left-gl">
												<div class="flexslider">
												  <div id="slider1_container" style="position: relative; top: 0px; left: 0px; width: 183px;height: 254px;">
													<!-- Slides Container -->
													<div u="slides" style="cursor: move; position: absolute; left: 0px; top: 0px; width: 183px; height: 254px;
														overflow: hidden;">
														<div><a href="#"><img u="image" src="/style/img/landscape/01.jpg" /></a></div>
														<div><a href="#"><img u="image" src="/style/img/landscape/01.jpg" /></a></div>
														<div><a href="#"><img u="image" src="/style/img/landscape/01.jpg" /></a></div>
														<div><a href="#"><img u="image" src="/style/img/landscape/01.jpg" /></a></div>
														<div><a href="#"><img u="image" src="/style/img/landscape/01.jpg" /></a></div>
														<div><a href="#"><img u="image" src="/style/img/landscape/01.jpg" /></a></div>
														<div><a href="#"><img u="image" src="/style/img/landscape/01.jpg" /></a></div>
														<div><img u="image" src="/style/img/landscape/02.jpg" /></div>
														<div><img u="image" src="/style/img/landscape/03.jpg" /></div>
														<div><img u="image" src="/style/img/landscape/04.jpg" /></div>
														<div><img u="image" src="/style/img/landscape/05.jpg" /></div>
														<div><img u="image" src="/style/img/landscape/06.jpg" /></div>
														<div><img u="image" src="/style/img/landscape/07.jpg" /></div>
														<div><img u="image" src="/style/img/landscape/08.jpg" /></div>
													</div>
													<a style="display: none" href="http://www.jssor.com">slideshow</a>
												</div>
												</div>
											</div><div class="right-gl">';
										}else if($i == 2){
											echo '<img src="/'.$val['image'].'" title ='.$val['name'].'></img>';
										}else{
											echo ' <div id="slider1_container_up" style="position: relative; top: 0px; left: 0px; width: width: 183px;height: 124px;">
													<div u="slides" style="cursor: move; position: absolute; left: 0px; top: 0px; width: 183px; height: 124px;overflow: hidden;">
														<div><img u="image" src="/style/img/landscape/01.jpg" /></div>
														<div><img u="image" src="/style/img/landscape/01.jpg" /></div>
														<div><img u="image" src="/style/img/landscape/01.jpg" /></div>
														<div><img u="image" src="/style/img/landscape/01.jpg" /></div>
														<div><img u="image" src="/style/img/landscape/01.jpg" /></div>
														<div><img u="image" src="/style/img/landscape/01.jpg" /></div>
													</div>
												</div>';
										}
									}
								?>
								</div>
							</div>
                        </div>
						<div class="col-md-12 col-sm-12 col-xs-12 market">
                            <h2 class="title-block">Tìm chúng tôi trên facebook</h2>
                            <div class="main-gl">
								<iframe src="//www.facebook.com/plugins/likebox.php?href=https%3A%2F%2Fwww.facebook.com%2Ffan.xedapdien&amp;width=300px&amp;height=290&amp;colorscheme=light&amp;show_faces=true&amp;header=false&amp;stream=false&amp;show_border=false" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:375px; height:300px;background:#fff;" allowTransparency="true"></iframe>
							</div>
                        </div>
						<div class="col-md-12 col-sm-12 col-xs-12 bannerquangcao">
							<?php
								weblink(21,100);
							?>
						</div>
                    </div>
                
                

                </div>
		</div>