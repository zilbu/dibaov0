﻿<?php if (substr_count($_SERVER['PHP_SELF'], '/da.php') > 0) die ("You can't access this file directly..."); ?>

<style>

    .main-da .container {
        background: #fff;
        padding-top: 20px;
    }

    <
    style >
    #container .threesixty {
        position: relative;
        overflow: hidden;
        margin: 0 auto;
    }

    #container .threesixty .threesixty_images {
        display: none;
        list-style: none;
        margin: 0;
        padding: 0;
    }

    #container .threesixty .threesixty_images img {
        position: absolute;
        top: 0;
        width: 100%;
        height: auto;
    }

    #container .threesixty .threesixty_images img.previous-image {
        visibility: hidden;
        width: 0;
    }

    #container .threesixty .threesixty_images img.current-image {
        visibility: visible;
        width: 100%;
    }

    #container .threesixty .spinner {
        width: 60px;
        display: block;
        margin: 0 auto;
        height: 30px;
        background: #333;
        background: rgba(0, 0, 0, 0.7);
        -webkit-border-radius: 5px;
        -moz-border-radius: 5px;
        border-radius: 5px;
    }

    #container .threesixty .spinner span {
        font-family: Arial, "MS Trebuchet", sans-serif;
        font-size: 12px;
        font-weight: bolder;
        color: #FFF;
        text-align: center;
        line-height: 30px;
        display: block;
    }

    .videoda {
        float: left;
        width: 100%;
        min-height: 460px;
        background: #272626;
        margin-top: 20px;
    }

    .videoitem {
        float: left;
        padding: 0px;
    }

    .listvideosli img {
        float: left;
        padding: 10px;
        width: 103px;
        padding-left: 0px;
    }

    .listvideos li:hover {
        background: #252525;
        cursor: pointer;
    }

    .listvideos li h4 {
    }

    .listvideos {
        padding-left: 0px;
        height: 460px;
        overflow: auto;
    }

    .listvideos li {
        float: left;
        width: 100%;
        color: #fff;
        color: #cacaca;
        font-size: 13px;
        font-weight: normal;
        padding-left: 10px;
    }

    .list-video li h4 {
        color: #cacaca;
        font-size: 13px;
        font-weight: normal;
        padding: 5px;
        line-height: 21px;
    }

    .listvideos li h4 {

        color: #cacaca;
        font-size: 13px;
        font-weight: normal;
        padding: 5px;
        line-height: 21px;
    }

    #container .threesixty .nav_bar {
        position: absolute;
        top: 10px;
        right: 10px;
        z-index: 11;
    }

    #container .threesixty .nav_bar a {
        display: block;
        width: 32px;
        height: 32px;
        float: left;
        background: url(/demo/img/sprites.png) no-repeat;
        text-indent: -99999px;
    }

    #container .threesixty .nav_bar a.nav_bar_play {
        background-position: 0 0;
    }

    #container .threesixty .nav_bar a.nav_bar_previous {
        background-position: 0 -73px;
    }

    #container .threesixty .nav_bar a.nav_bar_stop {
        background-position: 0 -37px;
    }

    #container .threesixty .nav_bar a.nav_bar_next {
        background-position: 0 -104px;
    }

    .threesixty .threesixty_images {
        display: none;
        list-style: none;
        margin: 0;
        padding: 0;
    }
    .box360{
        max-width:800px;
        margin: 15px auto;
    }
    .a-html{
        position: relative !important;
    }
    .a-html .a-body{
        overflow: scroll !important;
    }
    .a-html .a-body a-scene{
        width: 100% !important;
    }
    .a-html .a-body .a-canvas{
        width: 100% !important;
        position: relative;
        height: auto !important;
    }
</style>
<?php
$tblname = 'articles';
if (isset($_REQUEST['pcatID']) or isset($_REQUEST['urlfr']) or isset($_REQUEST['urlitem'])) {
    if ($_REQUEST['urlitem']) {
        $pcatID = sql_tb($tblname, ' urlitem = "' . $_REQUEST['urlitem'] . '" or urlitem1 = "' . $_REQUEST['urlitem'] . '" ', 'category');
        $name_cat = sql_tb($tblname . '_cat', ' id = ' . $pcatID . ' ', 'name');
        $url_cat = sql_tb($tblname . '_cat', ' id = ' . $pcatID . ' ', 'urlfr');
        $artID = sql_tb($tblname, ' urlitem = "' . $_REQUEST['urlitem'] . '" or urlitem1 = "' . $_REQUEST['urlitem'] . '" ', 'id');
    }
    $arr360 = array(
        267 => array(
            'number' => '32',
            'path' => 'upimages/360/xmandibao/',
            'prefix' => 'xe-dien-xman-dibao_000',
        ),
        283 => array(
            'number' => '32',
            'path' => 'upimages/360/buf/',
            'prefix' => 'xe-dien-dibao-buffalo_000',
        ),
        280 => array(
            'number' => '32',
            'path' => 'upimages/360/zoomerdibao2016/',
            'prefix' => 'xe-dien-zoomer-dibao-20160',
        ),
        191 => array(
            'number' => '30',
            'path' => '/upimages/360/ninjadibaoeco/',
            'prefix' => 'xe-dien-ninja-dibao-eco',
        ),
        202 => array(
            'number' => '32',
            'path' => '/upimages/360/zoomerdibao/',
            'prefix' => 'xe-dien-zoomer-dibao-',
        ),
        257 => array(
            'number' => '32',
            'path' => '/upimages/360/ninjadibao/',
            'prefix' => 'xe-dap-dien-ninja-dibao-',
        ), 261 => array(
            'number' => '32',
            'path' => '/upimages/360/splus/',
            'prefix' => 'Xe-dien-giant-m133s-plus-trang-',
        ), 279 => array(
            'number' => '32',
            'path' => '/upimages/360/haodadibao/',
            'prefix' => 'xe-dien-haoda-dibao',
        ), 252 => array(
            'number' => '32',
            'path' => '/upimages/360/vespadibao/',
            'prefix' => 'xe-dien-vespas-dibao_000',
        ),
    );
    ?>
    <section class="top-detail">
        <div class="container">
            <div class="row">
                <div class="col-md-8">
                    <div class="carousel slide" id="carousel_thumbnail" data-ride="carousel">
                        <div class="carousel-inner" role="listbox">
                            <?php
                            $images = $row['images'];
                            if (count($images) > 0)
                                $images = explode(",", $images);
                            if ($images[0])
                                for ($i = 0; $i < count($images); $i++) {
                                    if ($images[$i] != 'Array' and $images[$i] != "") {
                                        $avItem = str_replace('upimages', 'upimages/', $images[$i]);
                                        ?>
                                        <div class="item <?php echo $i==0?'active':''; ?>" data-slide-number="<?php echo $i;?>">
                                            <a class="fancybox" href="/<?php echo $avItem; ?>" data-fancybox-group="gallery" title="<?php echo $title; ?>">
                                                <img src="/<?php echo $avItem; ?>" alt="<?php echo $title; ?>">
                                            </a>
                                        </div>
                                        <?php
                                    }
                                }
                            ?>
                        </div>
                    </div>
                    <div id="nav_thumb">
                        <div class="owl-carousel carousel">
                            <?php
                            if ($images[0])
                                for ($i = 0; $i < count($images); $i++) {
                                    if ($images[$i] != 'Array' and $images[$i] != "") {
                                        $avItem = str_replace('upimages', 'upimages/.thumbs', $images[$i]);
                                        ?>
                                        <div class="o-item">
                                            <a href="javascript:;" data-id="<?php echo $i;?>" id="selector_<?php echo $i;?>" class="">
                                                <img src="/<?php echo $avItem; ?>" alt="">
                                            </a>
                                        </div>
                                        <?php
                                    }
                                }
                            ?>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="info-box center">
                        <h1><?php echo $row['title']; ?></h1>
                        <p>Trải nghiệm công nghệ và sắc màu</p>
                        <span class="price"><?php echo $row['price']; ?> đ</span>
                        <div class="margin-30b flex-box">
                            <span>Màu sắc</span> : &nbsp;<ul class="list-color">
                                <li><i></i></li>
                                <li><i></i></li>
                                <li><i></i></li>
                                <li><i></i></li>
                            </ul>
                        </div>
                        <a href="/daily.html" title="Tìm đại lý gần bạn" class="link-detail" target="_blank">Tìm đại lý gần bạn <span class="fa fa-chevron-right"></span></a>
                    </div>
                </div>
            </div>
        </div>
        <script type="text/javascript" src="/style/js/bootstrap.min.js"></script>
        <link rel="stylesheet" href="/style/fancybox/jquery.fancybox.css" />
        <link rel="stylesheet" href="/style/fancybox/lib/jquery.fancybox-buttons.css" />
        <link rel="stylesheet" href="/style/fancybox/lib/jquery.fancybox-thumbs.css" />
        <link rel="stylesheet" href="/style/owl-carousel/assets/owlcarousel/assets/owl.carousel.min.css">
        <link rel="stylesheet" href="/style/owl-carousel/assets/owlcarousel/assets/owl.theme.default.min.css">
        <script src="/style/fancybox/lib/jquery.mousewheel.pack.js"></script>
        <script src="/style/fancybox/jquery.fancybox.js"></script>
        <script src="/style/fancybox/jquery.fancybox.pack.js"></script>
        <script src="/style/fancybox/lib/jquery.fancybox-buttons.js"></script>
        <script src="/style/fancybox/lib/jquery.fancybox-thumbs.js"></script>
        <script src="/style/fancybox/lib/jquery.fancybox-media.js"></script>
        <script src="/style/owl-carousel/assets/owlcarousel/owl.carousel.js"></script>
        <script type="text/javascript">
            var totalItems = <?php echo count($images); ?>;
            var id2 = 1;
            if(parseInt(totalItems)){
                $('#carousel_thumbnail').append('<div class="total"></div>');
            }
            $('.total').html(''+id2+'/'+totalItems+'');
            $('#carousel_thumbnail').carousel({interval:false});
            $('#nav_thumb a').each(function(){
                $(this).on('click',function(ev){
                    ev.preventDefault();
                    var id = parseInt($(this).data('id'));
                    $('#carousel_thumbnail').carousel(id);
                    $('#nav_thumb a').removeClass('selected');
                    $(this).addClass('selected');
                })
            });
            
            $('#carousel_thumbnail').on('slid.bs.carousel', function(e) {
                id2 = parseInt($('.item.active').index());
                $('#nav_thumb a').removeClass('selected');
                $('#selector_'+ id2).addClass('selected');

                $('.total').html(''+(id2+1)+'/'+totalItems+'');
            });
            $(function(){
                $('.fancybox').fancybox();
                $('.owl-carousel').owlCarousel({
                    margin: 10,
                    nav:true,
                    dots: false,
                    responsive:{
                        600:{
                            items:3
                        },
                        1000:{
                            items:5
                        }
                    }
                })
            })
        </script>
    </section>
    <section class="module">
        <div class="container">
            <div class="heading">
                <h3>MÔ TẢ CHI TIẾT</h3>
            </div>
            <article class="detail-content">
                <?php
                echo $row['content'];
                ?>
            </article>

            <?php
            if(!empty( $row['videos'])) {
                ?>
                <div class="heading">
                    <h3>VIDEO</h3>
                </div>
                <div class="videoWrapper">
                    <!-- Copy & Pasted from YouTube -->
                    <iframe src="<?php echo $row['videos'] ?>"></iframe>
                </div>
                <?php
            }
            ?>
            </iframe>
<!--            <div class="embed-responsive embed-responsive-1by1">-->
<!--                <iframe width="420" height="345" src="--><?php //echo $row['videos'] ?><!--">-->
<!--                </iframe>-->
<!--            </div>-->
            <!-- BEGIN 360 -->
            <div class="center">
                <div class="box360">
                    <script src="https://aframe.io/releases/0.6.1/aframe.min.js"></script>
                <a-scene>
                  <a-assets>
                    <img id="panorama" src="https://c1.staticflickr.com/5/4302/35137573294_1287bfd0ae_k.jpg" crossorigin />
                  </a-assets>
                  <a-sky src="#panorama" rotation="0 -90 0"></a-sky>
                </a-scene>
                </div>
                
            </div>
            <!-- END 360 -->
            <div class="heading">
                <h3>Thông số kỹ thuật</h3>
            </div>
            <article class="detail-content">
                <ul id="" class="parametdesc">
                    <li>
                        <span>Chiều dài x Chiều rộng x Chiều cao:</span><strong><?php echo $row['dai_rong_cao']; ?></strong>
                    </li>
                    <li>
                        <span>Đường kính bánh xe:</span><strong><?php echo $row['duong_kinh_banh_xe']; ?></strong>
                    </li>
                    <li>
                        <span>Chiều cao yên xe:</span><strong><?php echo $row['chieu_cao_yen_xe']; ?></strong>
                    </li>
                    <li><span>Hãng sản xuất:</span><?php
                        if ($row['hang_san_xuat'] == "0") {
                            $row['hang_san_xuat'] = $name_cat;
                        }
                        echo '<a href="' . $base_url . $url_cat . '" title="' . $name_cat . '">' . $row['hang_san_xuat'] . '</a>';
                        // echo $row['hang_san_xuat'];
                        ?></li>
                    <li><span>Vận tốc tối đa:</span><strong><?php echo $row['van_toc_toi_da']; ?></strong>
                    </li>
                    <li><span>Loại acquy:</span><strong><?php echo $row['loai_acquy']; ?></strong></li>
                    <li>
                        <span>Khả năng chở vật nặng:</span><strong><?php echo $row['cho_vat_lang']; ?></strong>
                    </li>
                    <li><span>Quãng đường:</span><strong><?php echo $row['quang_duong']; ?></strong></li>
                    <li><span>Bảo vệ tụt áp:</span><strong><?php echo $row['bao_ve_tut_ap']; ?></strong>
                    </li>
                    <li><span>Thời gian sạc:</span><strong><?php echo $row['thoi_gian_sac']; ?></strong>
                    </li>
                    <li><span>Vận hành:</span><strong><?php echo $row['van_hanh']; ?></strong></li>
                    <li><span>Phanh:</span><strong><?php echo $row['phanh']; ?></strong></li>
                    <li><span>Giảm xóc:</span><strong><?php echo $row['giam_soc']; ?></strong></li>
                    <li><span>Bánh xe:</span><strong><?php echo $row['banh_xe']; ?></strong></li>
                    <li><span>Trọng lượng xe:</span><strong><?php echo $row['trong_luong_xe']; ?></strong></li>
                    <li><span>Công suất:</span><strong><?php echo $row['cong_xuat']; ?></strong></li>
                    <li><span>Động cơ xe:</span><strong><?php echo $row['dong_co_xe']; ?></strong></li>
                    <li><span>Đèn chiếu sáng:</span><strong><?php echo $row['den_chieu_sang']; ?></strong></li>
                    <li><span>Thời gian bảo hành:</span><strong><?php echo $row['thoi_gian_bao_hanh']; ?></strong></li>
                    <!--li>
                        <button type="button" class="viewparameterfull" onclick="getFullSpec()">Xem chi tiết thông số xe</button>
                    </li-->
                </ul>
            </article>
        </div>
    </section>
    <section class="module">
        <div class="container">
            <div class="heading">
                <h3>Sản phẩm cùng loại</h3>
            </div>
            <div class="row">
                <ul class="list list-product">
                    <?php
                    $data = sql_query_data("articles", 'active = 1', '', array('menus' => 'DESC'), array('id', 'title', 'image', 'urlitem', 'urlitem1', 'price', 'view'), 0, 5);
                    $i = 0;
                    foreach ($data as $val) {
                        $i++;
                        $url = urlItem($val['urlitem1'], $val['urlitem']);
                        // $val['image'] = '/upimages/.thumbs/'.$val['image'];
                        $val['image'] = str_replace("news", ".thumbs/news", $val['image']);
                        echo '<li class="col-md-4 col-sm-6">
                                <div class="inner">
                                    <div class="thumb">
                                        <a href="'.$url.'" title=""><img src="'.$val['image'].'" alt=""></a>
                                        <div class="shadow-info">
                                            <strong>Đặc điểm nổi bật</strong>
                                            <p>phanh đĩa trước</p>
                                            <p>Xe điện Dibao Nami mang một thiết kế hoàn toàn mới nhỏ gọn, thanh thoát điều này thể hiện rõ nét qua mặt trước của xe</p>
                                            <p>Đồng hồ điện tử</p>
                                        </div>
                                    </div>
                                    <div class="entry">
                                        <a href="'.$url.'" title="Xe điện Dibao" class="title">'.$val['title'].'</a>
                                        <div class="pr">
                                            Giá bán từ : <span class="price">'.$val['price'].' đ</span>
                                        </div>
                                        <a href="'.$url.'" title="Chi tiết" class="link-detail">Xem chi tiết <span class="fa fa-chevron-right"></span></a>
                                    </div>
                                </div>
                            </li>';
                    }
                    ?>


                </ul>
            </div>
        </div>
    </section>

    <?php
}
?>
		
		
		
		
		
		
		
		
		
