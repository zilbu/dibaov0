<?php
if (substr_count($_SERVER['PHP_SELF'], 'bn.php') > 0) die ("You can't access this file directly..."); ?>
<?php
$tblname = 'news';
if ($_REQUEST['urlfr'] or isset($_REQUEST['ncatID']))
{
$requestid = $_REQUEST['ncatID'];

$info_cat = sql_query_data($tblname . '_cat', ' id = ' . $requestid . ' ', '', '', '');
if (is_array($info_cat)) {
    $id_cat = $info_cat[0]['id'];
    $path = $info_cat[0]['level'];
    //$content   = $info_cat[0]['content'];
    //	$content = @str_replace('\"','',$content);
    $content = stripslashes($info_cat[0]['content']);
    $level1 = substr($path, 0, 2);
    $name_cat = $info_cat[0]['name'];
    $level = strlen(str_replace('.', '', $path)) / 2;
}

if (isset($_REQUEST['artID'])) {
    $tblname = 'news';
    $artID = value_filter($_REQUEST['artID']);
}

if ($type_cat == 2) {

    if (strlen($path) == 2) {
        $path = $level1 . '.01';
        $info_cat = sql_query_data($tblname . '_cat', ' level = "' . $path . '" ', '', '', '');
        if (is_array($info_cat)) {
            $id_cat = $info_cat[0]['id'];
            $path = $info_cat[0]['level'];
            $content = $info_cat[0]['content'];
            $content = @str_replace('\"', '"', $content);
            $level1 = substr($path, 0, 2);
            $name_cat = $info_cat[0]['name'];
            $level = strlen(str_replace('.', '', $path)) / 2;

        }
    }
}
$obj = $level;
switch ($obj)
{
case '1':
case '2':
case '3':
$itemonrow = 1;
$rows = 8;
$curpage = 1;
// Get title
$catid = cat_id($tblname, $path); ?>
<?php //include($include_dir."top-main.php");
?>

<div class="container">
    <div class="row">
        <div class="col-md-12 mt50">

            <div class="muntinews">
                <ul class="newslist">
                    <?php

                    $query2 = 'select * from ' . $tblname . ' where';
//                    if (!isset($_REQUEST['artID']))
//                        $query2 .= ' id in (' . $catid . ') and';
//                    else
//
//                        $query2 .= ' id = ' . $artID . ' and';
                    $query2 .= ' lang="' . get_langID() . '"';
                    $query2 .= ' and ' . $tblname . '.active=1';
                    if($requestid != 24){
                        $query2.=' AND category='.$requestid;
                    }else{
                        if($requestid==57){
                            $query2.=' AND category=57';

                        }else{
                            $query2.=' AND category=24';

                        }
                    }
                    $query2 .= ' order by level ASC, id DESC';
                    // chỉ lấy 1 bản ghi nếu không phải là page tin tức n=24

                    if((int)$requestid != 24 && (int)$requestid != 57){
                        $query2.=' LIMIT 1';
                    }

                    $query2 = str_replace(',)', ')', $query2);

                    $doquery = mysqli_query($link, $query2);

                    if ($doquery and mysqli_num_rows($doquery) > 0)
                        $found = mysqli_num_rows($doquery);

                    if (isset($_REQUEST['curpage'])) {
                        $curpage = $_REQUEST['curpage'];
                        $from = ($curpage - 1) * $itemonrow * $rows;
                        $to = $from + $itemonrow * $rows;
                        $limit = ' limit ' . $from . ',' . $to;
                    }
                    if (isset($limit, $query2))

                        $query2 .= $limit;
                    $doquery2 = mysqli_query($link, $query2);

                    if ($doquery2 and mysqli_num_rows($doquery2) > 1)
                    {
                    if (!isset($totalpage)) {
                        if ($found < ($itemonrow * $rows))
                            $totalpage = 1;
                        else {
                            if ($found % ($itemonrow * $rows) == 0)
                                $totalpage = $found / ($itemonrow * $rows);
                            else
                                $totalpage = (int)($found / ($itemonrow * $rows)) + 1;
                        }
                    }

                    $i = 0;
                    //nếu là ve dibao
                    if ($requestid == 37) {

                        ?>
                        <div class="row">
                            <section class="module" style="padding-top:40px">
                                <div class="container">
                                    <div class="col-md-8 col-md-offset-2 about">
                                        <h2>Giới thiệu về Dibao</h2>

                                        <p>
                                            Công ty TNHH TM Tấn Thành tiền thân là Trung tâm Lắp ráp và sản xuất điện cơ BENLIN, đi vào hoạt động từ năm 2001, với hơn 14 năm kinh nghiệm trong lĩnh vực nhập khẩu và phân phối xe điện đến nay chúng tôi đã trở thành một đơn vị nhập khẩu và phân phối xe đạp điện uy tín hàng đầu Việt Nam.
                                        </p>
                                        <p class="center"><img src="pictures/about.png"></p>
                                        <h3>Dibao đã ra đời như thế nào</h3>
                                        <p>
                                            Vấn đề ô nhiễm môi trường đã trở thành một vấn đề đáng báo động trên toàn cầu và đặc biệt là ở những quốc gia
                                            đang phát triển như Việt Nam thì tình trạng ô nghiễm môi trường từ những hoạt động của con người đã trở thành
                                            nỗi lo sợ của mỗi người dân Việt.
                                            Hàng ngày, chúng ta luôn phải đối mặt với những nguy cơ ô nhiễm làm ảnh hưởng nghiệm trọng tới chất lượng
                                            sống như ô nhiễm tiếng ồn, ô nhiễm khói bụi…Với mong muốn mang tới một giải pháp giao thông thân thiện với
                                            môi trường và an toàn cho người sử dụng, Công ty TNHH MTV xe điện DK Việt Nhật đã nghiên cứu và đưa ra thị
                                            trường sản phẩm Xe đạp điện DKBike -  loại phương tiện di chuyển an toàn và thông minh, có khả năng cải thiện
                                            tình trạng môi trường, đem tới cho con người một bầu không khí trong lành.
                                        </p>
                                        <h3>Định hướng phát triển</h3>
                                        <p>
                                            Vấn đề ô nhiễm môi trường đã trở thành một vấn đề đáng báo động trên toàn cầu và đặc biệt là ở những quốc gia
                                            đang phát triển như Việt Nam thì tình trạng ô nghiễm môi trường từ những hoạt động của con người đã trở thành
                                            nỗi lo sợ của mỗi người dân Việt.
                                            Hàng ngày, chúng ta luôn phải đối mặt với những nguy cơ ô nhiễm làm ảnh hưởng nghiệm trọng tới chất lượng
                                            sống như ô nhiễm tiếng ồn, ô nhiễm khói bụi…Với mong muốn mang tới một giải pháp giao thông thân thiện với
                                            môi trường và an toàn cho người sử dụng, Công ty TNHH MTV xe điện DK Việt Nhật đã nghiên cứu và đưa ra thị
                                            trường sản phẩm Xe đạp điện DKBike -  loại phương tiện di chuyển an toàn và thông minh, có khả năng cải thiện
                                            tình trạng môi trường, đem tới cho con người một bầu không khí trong lành.
                                        </p>
                                    </div>
                                </div>
                            </section>
                        </div>
                        <?php
                        // nếu alf bảo hanh
                    } elseif ($requestid == 32) {
                        ?>
                        <div class="row">
                            <section class="module padding-40t">
                                <div class="container">
                                    <div class="col-md-8 col-md-offset-2 about">
                                        <h2>Giới thiệu về Dibao</h2>

                                        <p>
                                            Công ty TNHH TM Tấn Thành tiền thân là Trung tâm Lắp ráp và sản xuất điện cơ
                                            BENLIN, đi vào hoạt động từ năm 2001, với hơn 14 năm kinh nghiệm trong lĩnh
                                            vực nhập khẩu và phân phối xe điện đến nay chúng tôi đã trở thành một đơn vị
                                            nhập khẩu và phân phối xe đạp điện uy tín hàng đầu Việt Nam.
                                        </p>
                                        <p class="center"><img src="pictures/about.png"></p>

                                    </div>
                                </div>
                            </section>
                        </div>
                        <?php
                        // nếu là vận chuyển
                    } elseif ($requestid == 19) {

                        ?>
                        <div class="row">
                            <section class="module padding-40t">
                                <div class="container">
                                    <div class="col-md-8 col-md-offset-2 about">
                                        <h2>About us / Term and Conditions / Privacy Policies</h2>

                                        <div class="box margin-10b">
                                            <strong>Benefits for the Buyers</strong>

                                            <p>là nền tảng công nghệ thông tin (CNTT) kết nối nhu cầu thuê dịch vụ lưu
                                                trú du lịch giữa: Bên cung cấp dịch vụ lưu trú du lịch & Bên có nhu cầu
                                                sử dụng dịch vụ lưu trú du lịch. Theo pháp luật địa phương nơi hoạt
                                                động, Luxstay cam kết không cung cấp dịch vụ cho thuê nhà/dịch vụ lưu
                                                trú du lịch, mà chỉ hoạt động như một trung gian kết nối và cung cấp
                                                tiện ích CNTT cho các bên tham gia trên hệ thống Luxstay.</p>
                                        </div>
                                        <div class="box margin-10b">
                                            <strong>Benefits for the Buyers</strong>
                                            <p>là nền tảng công nghệ thông tin (CNTT) kết nối nhu cầu thuê dịch vụ lưu
                                                trú du lịch giữa: Bên cung cấp dịch vụ lưu trú du lịch & Bên có nhu cầu
                                                sử dụng dịch vụ lưu trú du lịch. Theo pháp luật địa phương nơi hoạt
                                                động, Luxstay cam kết không cung cấp dịch vụ cho thuê nhà/dịch vụ lưu
                                                trú du lịch, mà chỉ hoạt động như một trung gian kết nối và cung cấp
                                                tiện ích CNTT cho các bên tham gia trên hệ thống Luxstay.</p>
                                        </div>
                                    </div>
                                </div>
                            </section>
                        </div>
                        <?php
                    } else {

                        ?>
                        <section class=" margin-20b" style="margin-top:20px">
                            <div class="container">
                                <div >
                                    <a href="javascript:;"><img src="/pictures/1-01@2x.png"/></a>
                                </div>
                            </div>
                        </section>
                        <section class="module">
                            <div class="container">
                                <div class="row">
                                    <h1 style="margin-left: 5px">Tin tức</h1>
                                </div>
                                <div class="row">
                                    <article class="col-md-9 content-left margin-20b">
                                        <ul class="list news-feed">
                                            <?php
//                                            $d_sql  =   'select * from news where news.active=1 order by level ASC, id DESC ';
//                                            $d_doquery2 = mysqli_query($link, $d_sql);
//                                            while ($result2 = mysqli_fetch_array($d_doquery2) and $i <= ($itemonrow * $rows)) {
                                            while ($result2 = mysqli_fetch_array($doquery2) and $i <= ($itemonrow * $rows)) {

                                                $id = $result2['id'];
                                                $category = $result2['category'];
                                                $title = stripslashes($result2['title']);
                                                $image = '/' . $result2['image'];
                                                $urlfile = $result2['urlfile'];
                                                $year = $result2['year'];
                                                $coquan = $result2['coquan'];
                                                // $content=$result2['content'];
                                                $urldownload = $result2['urldownload'];
                                                // $content = stripslashes($result2['content']);
                                                $content = strip_tags($result2['content']);
                                                $brief = stripslashes($result2['brief']);
                                                $log = stripslashes($result2['log']);
                                                if ($brief == '')
                                                    $brief = $title;
                                                $url = '/' . removeSpecialChars(removesign($title)) . '-c' . $category . 'n' . $id . '.html';
                                                if ($requestid == 33) {
                                                    ?>
                                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                                        <div class="no-padding"
                                                             style="height:auto;margin:0px;border:0px;">
                                                            <div class="">
                                                                <a href="<?php echo $url; ?>"
                                                                   title="<?php echo $title; ?>">
                                                                    <h3>
                                                                        <i class="fa fa-question-circle"></i> <?php echo $title; ?>
                                                                    </h3>
                                                                </a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <?php
                                                } else {

                                                    ?>
                                                    <li class="row">
                                                        <a class="col-md-4 thumb" href="<?php echo $url; ?>" title="">
                                                            <img src="<?php echo $image; ?>"
                                                                 alt="<?php echo $title; ?>">
                                                        </a>
                                                        <div class="col-md-8 entry">
                                                            <a href="<?php echo $url; ?>" title=""
                                                               class="title"><?php echo $title; ?></a>
                                                            <div class="margin-10b">
                                                                <span class="uptime post-time"><?php echo date('Y-m-d',strtotime($log)); ?></span>
                                                                <?php
                                                                $sql_qcat    =   'SELECT name,urlfr FROM news_cat WHERE id='.(int)$category;
                                                                $que_qcat   =   mysqli_query($link,$sql_qcat);
                                                                $cat_data   =   mysqli_fetch_assoc($que_qcat);
                                                                ?>
                                                            </div>
                                                            <p class="mobie-row3d row4d"><?php echo $brief ?></p>
                                                        </div>
                                                    </li>
                                                    <?php
                                                }
                                                $i++;
                                                if ($i == $rows * $itemonrow)
                                                    break;
                                            }
                                            ?>
                                        </ul>
                                        <div class="box center">
                                            <ul class="paging">
                                                <?php paging($requestid, $totalpage, $found, $curpage, $i, removeSpecialChars(removesign($name_cat)), 'n') ?>
                                            </ul>
                                        </div>
                                    </article>
                                    <aside class="col-md-3 sidebar-right margin-20b">
<!--                                        <div class="box margin-30b">-->
<!--                                            <form action="" method="post">-->
<!--                                                <div class="find-box">-->
<!--                                                    <button><i class="fa fa-search"></i></button>-->
<!--                                                    <input type="text" name="" value="" placeholder="tìm kiếm bài viết">-->
<!--                                                </div>-->
<!--                                            </form>-->
<!--                                        </div>-->
                                        <div class="box">
                                            <h3 class="sidebar-title">TIN XEM NHIỀU</h3>
                                            <ul class="list sidebar-post">
                                                <?php
                                                $data = sql_query_data("news", ' active = "1" and (category = "24" or category="34" or category="32") ', '', array('id' => 'DESC'), array('id', 'view', 'log', 'title', 'brief', 'category', 'image'), 0, 6);
                                                $i = 0;
                                                foreach ($data as $val) {
                                                    $i++;
                                                    $id = $val['id'];
                                                    $view = $val['view'];
                                                    $log = $val['log'];
                                                    $category = $val['category'];
                                                    $title = $val['title'];
                                                    $img = $val['image'];
                                                    $url2 = '/' . removeSpecialChars(removesign($title)) . '-c' . $category . 'n' . $id . '.html';
                                                    $val['image'] = str_replace("news", ".thumbs/news", $val['image']);
                                                    ?>
                                                    <li class="row">
                                                        <a href="<?php echo $url2 ?>" class="thumb col-md-4"><img <?php if($i !=1) {?> class="mobie-fix-w70" <?php } ?>
                                                                    src="/<?php echo $img ?>" alt="<?php echo $title ?>"></a>
                                                        <a  style="    display: -webkit-box;font-weight: bold;color:#222" href="<?php echo $url2 ?>"
                                                           class="row2d title col-md-8"><?php echo $title ?></a>
                                                    </li>
                                                    <?php
                                                }
                                                ?>
                                            </ul>
                                        </div>
                                    </aside>
                                </div>
                            </div>
                        </section>
                        <?php
                    }
                    ?>
                </ul>
            </div>
            <?php
            }
            else {
                if ($doquery2 and mysqli_num_rows($doquery2) == 1) {
                    $result2 = mysqli_fetch_array($doquery2);
                    $id = $result2['id'];
                    $brief = $result2['brief'];
                    $category = $result2['category'];
                    $title = stripslashes($result2['title']);
                    $image = $result2['image'];
                    $content = $result2['content'];
                    $content = stripslashes($result2['content']);
                    $url = '/n' . $category . '_' . $name . '/' . $id . '_' . removeSpecialChars(removesign($title)) . '.html';
                    //Show item
                    ?>
                    <div class='detail_news'>
                        <?php
                        if ($id == 36) {
                            echo '	<div class="ketqua" style="width:100%;text-align:center;float:left;">
														
														<h2 style="font-weight:bold;padding-top:10px;padding-bottom:10px;float:left;width:100%;font-size:13px;text-transform:uppercase;">Kiểm tra bảo hành</h2>
														<form id="post-form">
															<input type="text" name="txtCode" class="txtCode" id="txtCode" placeholder = "Nhập số máy (Motor bánh sau) để kiểm tra..." style="width:295px;">
															<input style="font-weight: bold;border-radius: 3px;background: #fff;padding: 0px 15px 1px 15px !important;" type="button" name="sub" class="sub" id="sub" value ="Kiểm tra" />
														</form>
														<div class="target" id="target" style="width:100%;text-align:center;float:left;">
														</div>
														<div class="result" id="result" style="width: 37%;text-align: center;font-weight: bold;line-height: 20px;text-align: left;margin: 0 auto;margin-top: 10px">
															<div id="boxCode"></div>
															<div id="boxTitle"></div>
															<div id="boxDate"></div>
															<div id="boxBaohanh"></div>
														</div>
													</div>';
                        }
                        ?>
                        <?php echo $content; ?>

                    </div>
                    <?php
                    $query_news = sql_query("news", " category = '" . $category . "' and id != '" . $id . "' ", " RAND() ", 0, 9);
                    if (isset($query_news)) {
                        ?>
                        <?php
                        $i = 0;
                        foreach ($query_news as $next) {
                            $i++;
                            $id = $next['id'];
                            $category = $next['category'];
                            $title = stripslashes($next['title']);
                            $url = '/n' . $category . '_' . $name . '/' . $id . '_' . removeSpecialChars(removesign($title)) . '.html';
                            ?>
                            <?php
                        }
                    }
                }
            }
            ?>
            <?php
            break;

            default:
                break;
            }
            }
            ?>
        </div>
    </div>
    <?php
    if($requestid==38) {
        ?>
        <section class="module">
            <div class="container">
                <div class="row">
                    <div class="col-md-8 col-md-offset-2">
                        <h3 class="center">CÁC DÒNG XE NỔI BẬT</h3>
                        <ul class="list flex-box top-manuf">
                            <li>
                                <a href="javascript:;" title="" class="thumb"><img src="/pictures/Image 3@2x.png" /><span>Bridgestone</span></a>
                            </li>
                            <li>
                                <a href="javascript:;" title="" class="thumb"><img src="/pictures/Rectangle 466@2x.png" /><span>Honda</span></a>
                            </li>
                            <li>
                                <a href="javascript:;" title="" class="thumb"><img src="/pictures/Rectangle 467@2x.png" /><span>Yamaha</span></a>
                            </li>
                            <li>
                                <a href="javascript:;" title="" class="thumb"><img src="/pictures/Rectangle 468@2x.png" /><span>NIJIA (MAXBIKE)</span></a>
                            </li>
                            <li>
                                <a href="javascript:;" title="" class="thumb"><img src="/pictures/Rectangle 469@2x.png" /><span>Sunra</span></a>
                            </li>
                            <li>
                                <a href="javascript:;" title="" class="thumb"><img src="/pictures/Rectangle 470@2x.png" /><span>Dibao</span></a>
                            </li>
                            <li>
                                <a href="javascript:;" title="" class="thumb"><img src="/pictures/Rectangle 471@2x.png" /><span>Giant</span></a>
                            </li>
                            <li>
                                <a href="javascript:;" title="" class="thumb"><img src="/pictures/Rectangle 472@2x.png" /><span>Yadea</span></a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </section>
        <?php
    }
    ?>
    <section class="margin-20b padding-30t">
        <div class="container">
            <div class="heading">
                <h3>Có thể bạn quan tâm</h3>
            </div>
            <div class="row">
                <ul class="list box-news3">
                    <?php
                    $data = sql_query_data("news", ' active = "1" ', '', array('id' => 'DESC'), array('id', 'view', 'log', 'title', 'brief', 'category', 'image'), 0, 3);
                    if ($data) {
                        $i = 0;
                        foreach ($data as $result) {
                            // $url	=	$result['urlfr'];
                            $title = $result['title'];
                            $category = $result['category'];
                            $id = $result['id'];
                            $date = date('Y-m-d', strtotime($result['log']));
                            $url = '/' . removeSpecialChars(removesign($title)) . '-c' . $category . 'n' . $id . '.html';
                            echo ' <li class="col-md-4 col-sm-6">
                                    <div class="item">
                                        <a href="' . $url . '" title="' . $title . '" class="thumb"><img
                                                    src="/' . $result['image'] . '" alt="' . $title . '"/></a>
                                        <span class="uptime">' . $date . '</span>
                                        <a href="' . $url . '" title="" class="title">' . $title . '</a>
                                    </div>
                                </li>';
                        }
                    }
                    ?>
                </ul>
            </div>
        </div>
    </section>
</div>

