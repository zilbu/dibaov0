			<div class="main-slider container">
				<div class="row slidermain">
					<div id="owl-demo" class="owl-carousel">
						<?php
							$silder = sql_query('slides', ' active = 1', ' level ASC, log ASC ', 0,5 );
							if($silder) {
								$i = 0;
								foreach($silder as $result){
									$url	=	$result['org_web'];
									$title 	=   $result['org_name'];
									$brief 	=   $result['brief'];
									$img	=	$result['image'];
									echo '<div class="item">
											<a href="'.$url.'" title="'.$result['title'].'"><img src="/'.$img.'" alt="'.$result['title'].'"></a>
										</div>';
								}
							}
						?>
					</div>
				</div>
			</div>
			<div class="main-more">
				<div class="more container">
					<?php
						$moredata = sql_query('weblinks', ' active = 1 and advertising = 100', ' level ASC, log ASC ', 0,5 );
						if($silder) {
							$i = 0;
							foreach($moredata as $result){
								$url	=	$result['org_web'];
								$title 	=   $result['org_name'];
								$brief 	=   $result['brief'];
								$img	=	$result['image'];
								echo '<div class="col-md-6 col-sm-6 col-xs-6 col-lg-6">
										<a href="'.$url.'" title="'.$result['title'].'"><img src="/'.$img.'" alt="'.$result['title'].'"></a>
									</div>';
							}
						}
					?>
				</div>
			</div>
			<div class="bg-main">
				<div class="container main">
					<div class="row">
					<?php
						$Itemhome=sql_query_data('articles','active = 1 ','',array('level' => 'ASC'),array('id,title,urlitem,urlitem1,price,promotion,image'),0,9);
						if($Itemhome) {
							$i = 0;
							foreach($Itemhome as $result){
								$url	=	$result['urlitem'];
								$title	=	$result['title'];
								$urlitem	=	$result['urlitem'];
								$price	=	$result['price'];
								$image	=	$result['image'];
								$i++;
								$foAr = array(0,1,2,3,13,14,15,19,20,21);
								$arrRow=array(3,6,9,12,15);
								if(in_array($i,$foAr)){
									$class="itemmain";
								}else{
									$class="itemmain1";
								}
								echo '<div class="col-lg-4 col-md-4 item col-sm-4 col-xs-4 '.$class.'">
										<div class="row">
											<div class="col-lg-8 col-md-8 col-sm-8 col-xs-8 nopadding" data-map="map-capa" class="">
												<a href="/'.$urlitem.'.html" class="main-img"><img src="/'.$image.'" alt="'.$title.'"/></a>
											</div>
											<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
												<div class="stamp">
													<a href="/'.$urlitem.'.html"><img src="/style/img/vip.jpg" alt=""></a>
												</div>
												<div class="img-logo">
												   <h2><a href="/'.$urlitem.'.html">'.$title.'</a></h2>
												</div>
												<div>
													<ul class="list-color" data-map-value="map-capa">
														<li><a href="javascript://" class="first active isactive" data-title="Đen đỏ - Cá tính" data-img="capa-red.png?v=4">
															<img src="http://hkbike.com.vn/assets/images/home/product/color/do_den.png?v=1" alt=""></a></li>
														<li><a href="javascript://" class="" data-title="Xanh Tím Than - Mạnh mẽ" data-img="capa-blue.png?v=4">
															<img src="http://hkbike.com.vn/assets/images/home/product/color/xanh_den.png?v=1" alt=""></a></li>
														<li><a href="javascript://" class="" data-title="Trắng đen bừng sáng - Thanh lịch" data-img="capa-white.png?v=4">
															<img src="http://hkbike.com.vn/assets/images/home/product/color/den_trang.png?v=1" alt=""></a></li>
													</ul>
												</div>
												<div class="price">'.$price.'<span>đ</span></div>
											</div>
										</div>
									</div>';
								if(in_array($i,$arrRow)){
									echo '</div>';
									echo '<div class="row">';
								}	
								}
							}
						?>
					</div>
				</div>
			</div>
			<div class="bg-news">
				<div class="container">
					<div class="row">
						<div class="col-md-4 col-sm-4 col-xs-4 col-lg-4">
							<div class="headernew">
                                <h3>Tin mới nhất</h3>
                            </div>
							<ul class="ul-list-news">
								<?php
									$data= sql_query_data("news",' active = "1" and category=37 or category = 47 ','',array('id' => 'DESC'),array('id','view','log','title','brief','category','image'),0,3);
									if($data) {
										$i = 0;
										foreach($data as $result){
											$title	=	$result['title'];
											$category	=	$result['category'];
											$id	=	$result['id'];
											$url='/'.removeSpecialChars(removesign($title)).'-c'.$category.'n'.$id.'.html';
											$i++;
											if($i == 1){
												echo '<li class="first">
														<div class="col-thumb">
															<div class="left">
																<a href="'.$url.'">
																	<img title="'.$title.'" alt="'.$title.'" width="150" src="/'.$result['image'].'">
																</a>
															</div>
															<div class="col-des">
																<div class="right">
																	<a href="'.$url.'">
																		<h4 class="title">'.$title.'</h4>
																	</a>
																</div>
															</div>
														</div>
													</li>';
												}else{
													echo '<li class="notfirst">
															<a href="'.$url.'">
																<i class="fa fa-circle"></i> '.$title.'
															</a>
														</li>';
												}
											}
										}
								?>		
							</ul>
						</div>
						<div class="col-md-4 col-sm-4 col-xs-4 col-lg-4">
							<div class="headernew">
                                <h3>Tin công ty</h3>
                            </div>
							<ul class="ul-list-news">
								<?php
									$data= sql_query_data("news",' active = "1" and category=37 ','',array('id' => 'DESC'),array('id','view','log','title','brief','category','image'),0,3);
									if($data) {
										$i = 0;
										foreach($data as $result){
											$title	=	$result['title'];
											$i++;
											if($i == 1){
												echo '<li class="first">
														<div class="col-thumb">
															<div class="left">
																<a href="'.$url.'">
																	<img title="'.$title.'" alt="'.$title.'" width="150" src="/'.$result['image'].'">
																</a>
															</div>
															<div class="col-des">
																<div class="right">
																	<a href="'.$url.'">
																		<h4 class="title"> '.$title.'</h4>
																	</a>
																</div>
															</div>
														</div>
													</li>';
												}else{
													echo '<li class="notfirst">
															<a href="'.$url.'">
																<i class="fa fa-circle"></i> '.$title.'
															</a>
														</li>';
												}
											}
										}
								?>		
							</ul>
						</div>
						<div class="col-md-4 col-sm-4 col-xs-4 col-lg-4 video-home">
							<div class="headernew">
                                <h3>VIDEO</h3>
                            </div>
							<div class="videos">
								<div id="list-videos2">
									<div class="main-gl" id="danhmucvideos">
										<iframe id="video" width="390" height="250" frameborder="0" allowfullscreen="" src="http://www.youtube.com/embed/krwbnIe8zdw"></iframe>
									</div>
									<ul id="list-videos" style="float:left; margin-top:5px;">
										<li class="videohome" onclick="return showVideo('video','http://www.youtube.com/embed/AvjsFksFGZk?autoplay=1');">
											<p><img src="http://img.youtube.com/vi/AvjsFksFGZk/default.jpg" alt=""></p>
										</li><li class="videohome" onclick="return showVideo('video','http://www.youtube.com/embed/Z91irBL8rlk?autoplay=1');">
											<p><img src="http://img.youtube.com/vi/Z91irBL8rlk/default.jpg" alt=""></p>
										</li>
										<li class="videohome" onclick="return showVideo('video','http://www.youtube.com/embed/Lu2Z6j_sqcw?autoplay=1');">
											<p><img src="http://img.youtube.com/vi/Lu2Z6j_sqcw/default.jpg" alt=""></p>
										</li>
										<li class="videohome" onclick="return showVideo('video','http://www.youtube.com/embed/ESv18zwdIvY?autoplay=1');">
											<p><img src="http://img.youtube.com/vi/ESv18zwdIvY/default.jpg" alt=""></p>
										</li>

										<li class="videohome" onclick="return showVideo('video','http://www.youtube.com/embed/AeUdIRPOkLo?autoplay=1');">
											<p><img src="http://img.youtube.com/vi/AeUdIRPOkLo/default.jpg" alt=""></p>
										</li>
									</ul>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			