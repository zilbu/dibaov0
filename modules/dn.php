<?php if (substr_count($_SERVER['PHP_SELF'], '/da.php') > 0) die ("You can't access this file directly..."); ?>
<?php
if (isset($_REQUEST['ncatID'])) {
    $tblname = 'news';
    $ncatID = value_filter($_REQUEST['ncatID']);
    $name_cat = sql_tb($tblname . '_cat', ' id = ' . $ncatID . ' ', 'name');
}
?>
<?php //include($include_dir."top-main.php");?>
<section class="module padding-40t">
<div class="container">
    <div class="row">
        <article class="col-md-9 content-left margin-20b">

            <?php
            if (isset($_REQUEST['module']))
            {
            $show_another = false;
            if (isset($_REQUEST['artID'], $_REQUEST['ncatID']))
            {
            $tblname = 'news';
            $artNum = 9;
            // Get title
            $query1 = 'select ' . $tblname . '.*,' . $tblname . '_cat.level as category_level'; //,'.$tblname.'_cat.id as category_id';
            $query1 .= ' from ' . $tblname . ' inner join ' . $tblname . '_cat';
            $query1 .= ' on ' . $tblname . '.category=' . $tblname . '_cat.id';
            $query1 .= ' where category = ' . $_REQUEST['ncatID'] . ' and lang="' . get_langID() . '"';
            //echo $query1;
            $doquery1 = mysqli_query($link, $query1);
            if ($doquery1 and mysqli_num_rows($doquery1) > 0) {
                $result1 = mysqli_fetch_array($doquery1);
                $title = chain($tblname, $result1['category_level'], 'level', get_langID());
            }
            // Get items
            $query = 'select * from ' . $tblname . ' where';
            $query .= ' id="' . $_REQUEST['artID'] . '" and';
            $query .= ' lang="' . get_langID() . '"';
            $query .= ' order by';
            $query .= ' id ASC,';
            $query .= ' log DESC';
            //---------------- Show item -----------------
            function show_item($result)
            {
            global $tblname;
            global $display;
            global $datetime_format;
            global $siteUrl;
            $id = $result['id'];
            $category = $result['category'];
            $title = stripslashes($result['title']);
            $image = $result['image'];
            $view = $result['view'];
            $content = $result['content'];

            $content = stripslashes($result['content']);
            $content = str_replace("=\"www.", "=\"http://www.", $content);
            $content = str_replace('\"', '', $content);
            $datetime = $result['log'];
            $url = '/' . removeSpecialChars(removesign($result['title'])) . '-c' . $result['category'] . 'a' . $result['id'] . '.html';
            ?>
            <div class='detail-content post-detail'>
                <div class='short'>
                    <h1 class='title'><?php echo $title; ?></h1>
                    <p class="time"><i class="fa fa-clock"></i> 26/8/2018 06:48</p>
                </div>
<!--                <ul class="link-news margin-10b">-->
<!--                    <li>-->
<!--                        <a href="javascript:;" title="">Đừng Để Phải Tiếc Nuối Vì Không Lưu Ngay 4 Homestay “Đẹp - Độc --->
<!--                            Chất” Ngay Giữa Sài Thành</a>-->
<!--                    </li>-->
<!--                    <li>-->
<!--                        <a href="javascript:;" title="">Đừng Để Phải Tiếc Nuối Vì Không Lưu Ngay 4 Homestay “Đẹp - Độc --->
<!--                            Chất” Ngay Giữa Sài Thành</a>-->
<!--                    </li>-->
<!--                    <li>-->
<!--                        <a href="javascript:;" title="">Đừng Để Phải Tiếc Nuối Vì Không Lưu Ngay 4 Homestay “Đẹp - Độc --->
<!--                            Chất” Ngay Giữa Sài Thành</a>-->
<!--                    </li>-->
<!--                </ul>-->
                <?php
                if ($id == 36) {
                    echo '<div class="news-run1">
									<div class="ketqua" style="width:100%;text-align:center;float:left;position:relative;padding-top:10px;z-index:9999">
										<form id="post-form" style="width:65%;">
											<input type="text" name="txtCode" class="txtCode" id="txtCode" placeholder="Nhập số máy (Motor bánh sau) để kiểm tra hạn bảo hành ..." style="width:460px;">
											<input style="font-weight: bold;border-radius: 3px;background: #CB0000;padding: 0px 15px 1px 15px !important;" type="button" name="sub" class="sub" id="sub-bh" value="Kiểm tra" onclick="filter_home();">
										</form>
									</div>
								</div>';
                }
                ?>
                <div class='content'>
                    <?php echo $content; ?>

                </div>
                <?php if(false){ ?>
                <div class="box tag-box">
                    <strong><i class="fa fa-tags"></i> Tags</strong>
                    <ul class="tags-list">
                        <li><a href="javascript:;" title="">#Huyndai verna</a></li>
                        <li><a href="javascript:;" title="">#Huyndai verna</a></li>
                        <li><a href="javascript:;" title="">#Huyndai verna</a></li>
                        <li><a href="javascript:;" title="">#Huyndai verna</a></li>
                        <li><a href="javascript:;" title="">#Huyndai verna</a></li>
                        <li><a href="javascript:;" title="">#Huyndai verna</a></li>
                    </ul>
                </div>
                <?php } ?>
                <?php
                }
                //Another news
                $show_another = true;
                $another_query = 'select * from ' . $tblname . ' where';
                $another_query .= ' id>' . $_REQUEST['artID'] . ' and';
                $another_query .= ' category="' . $_REQUEST['ncatID'] . '"';
                $another_query .= ' order by id ASC';
                $another_query .= ' limit 0,' . ($artNum + 1) . '';

                $previous_query = 'select * from ' . $tblname . ' where';
                $previous_query .= ' id<' . $_REQUEST['artID'] . ' and';
                $previous_query .= ' category="' . $_REQUEST['ncatID'] . '"';
                $previous_query .= ' order by id ASC';
                $previous_query .= ' limit 0,' . ($artNum + 1) . '';
                }
                //show_content();
                //echo $query;
                $doquery = mysqli_query($link, $query);
                if ($doquery and mysqli_num_rows($doquery) > 0) {
                    $result = mysqli_fetch_array($doquery);
                    $update = 'update ' . $tblname . ' set view="' . ($result['view'] + 1) . '" where id="' . $_REQUEST['artID'] . '"';
                    //echo $update;
                    $doupdate = mysqli_query($link, $update);
                    ?>
                    <?php @show_item($result); ?>
                    <?php
                }
                //End show detail
                ?>
                <div class="othernews">
                    <?php echo $display['old_news']; ?>
                </div>
                <?php
                //Another article
                if ($show_another == true) {
                    $cont = false;
                    ?>
                    <div class="old_news"
                         style="float: left; text-align: left; padding-left: 15px; margin: 15px 0; clear:both;">
                        <?php
                        //echo $another_query;
                        if ($doanother_query = mysqli_query($link, $another_query) and mysqli_num_rows($doanother_query) > 0) {
                            $counter = 0;
                            while ($result = mysqli_fetch_array($doanother_query)) {
                                $counter++;
                                //echo $counter;
                                if ($counter == ($artNum + 1)) {
                                    $nextid = $result['id'];
                                    $cont = true;
                                    break;
                                } else {
                                    $id = $result['id'];
                                    $log = $result['log'];
                                    $category = $result['category'];
                                    $title = stripslashes($result['title']);
                                    $name = sql_tb('news_cat', ' id = "' . $category . '"', 'name');
                                    $url = '/' . removeSpecialChars(removesign($result['title'])) . '-c' . $result['category'] . 'n' . $result['id'] . '.html';
                                    echo "<i class='fa fa-arrow-circle-right'></i> <a href=" . $url . " style='color: #303030;font-weight: bold;text-decoration: none;line-height: 25px;' >" . $title . "</a>  <i>(" . $log . " )</i><br/>\n";
                                }
                            }
                        }
                        if ($doprevious_query = mysqli_query($link, $previous_query) and mysqli_num_rows($doprevious_query) > 0) {
                            $counter = 0;
                            while ($result = mysqli_fetch_array($doprevious_query)) {
                                $counter++;
                                //echo $counter;
                                if ($counter == ($artNum + 1)) {
                                    $nextid = $result['id'];
                                    $cont = true;
                                    break;
                                } else {
                                    $id = $result['id'];
                                    $log = $result['log'];
                                    $category = $result['category'];
                                    $title = $result['title'];
                                    $name = sql_tb('news_cat', ' id = ' . $category . '', 'name');
                                    $url = '/' . removeSpecialChars(removesign($result['title'])) . '-c' . $result['category'] . 'n' . $result['id'] . '.html';
                                    echo "<i class='fa fa-arrow-circle-right'></i> <a href=" . $url . " style='color: #303030;font-weight: bold;text-decoration: none;line-height: 25px;' >" . $title . "</a> <i>(" . $log . " )</i><br/>\n";
                                }
                            }
                        }
                        ?>
                    </div>
                <?php
                }
                else
                {
                ?>
                    <script>
                        alert("<?php echo $display['noarticle']; ?>")
                        window.history.go(-1)
                    </script>
                    <?php
                }
                }
                ?>
            </div>
        </article>
        <aside class="col-md-3 sidebar-right margin-20b">
            <div class="box margin-30b hidden-xs">
                <form action="" method="post">
                    <div class="find-box">
                        <button><i class="fa fa-search"></i></button>
                        <input type="text" name="" value="Tìm kiếm bài viết" onfocus="{$(this).val('')}" onblur="{$(this).val('Tìm kiếm bài viết')}">
                    </div>
                </form>
            </div>
            <div class="box">
                <h3 class="sidebar-title">TIN XEM NHIỀU</h3>
                <ul class="list sidebar-post">

                    <?php
                    $data= sql_query_data("news",' active = "1" and (category = "24" or category="34" or category="32")','',array('id' => 'DESC'),array('id','view','log','title','brief','category','image'),0,6);
                    if($data) {
                        $i = 0;
                        foreach($data as $result){
                            $i++;
                            // $url	=	$result['urlfr'];
                            $title	=	$result['title'];
                            $category	=	$result['category'];
                            $id	=	$result['id'];
                            $date   =   date('Y-m-d',strtotime($result['log']));
                            $url='/'.removeSpecialChars(removesign($title)).'-c'.$category.'n'.$id.'.html';
                            if($i != 1) $check_img_thb =  'class="mobie-fix-w70"';
                            echo '<li class="row">
                                    <a href="'.$url.'" title="'.$title.'" class="thumb col-md-4"><img '.$check_img_thb.' src="/'.$result['image'].'" alt="'.$title.'"></a>
                                    <a href="'.$url.'" class="title col-md-8" style="font-weight:bold">'.$title.'</a>
                                </li>';
                        }
                    }
                    ?>
                </ul>
            </div>
        </aside>
    </div>
</div>
</section>
<section class="margin-20b">
    <div class="container">
        <div class="heading">
            <h3>Có thể bạn quan tâm</h3>
        </div>
        <div class="row">
            <ul class="list box-news3">
                <?php
                $data= sql_query_data("news",' active = "1" ','',array('id' => 'DESC'),array('id','view','log','title','brief','category','image'),0,3);
                if($data) {
                    $i = 0;
                    foreach($data as $result){
                        // $url	=	$result['urlfr'];
                        $title	=	$result['title'];
                        $category	=	$result['category'];
                        $id	=	$result['id'];
                        $date   =   date('Y-m-d',strtotime($result['log']));
                        $url='/'.removeSpecialChars(removesign($title)).'-c'.$category.'n'.$id.'.html';
                        echo ' <li class="col-md-4 col-sm-6">
                                    <div class="item">
                                        <a href="'.$url.'" title="'.$title.'" class="thumb"><img
                                                    src="/'.$result['image'].'" alt="'.$title.'"/></a>
                                        <span class="uptime">'.$date.'</span>
                                        <a href="'.$url.'" title="" class="title">'.$title.'</a>
                                    </div>
                                </li>';
                    }
                }
                ?>
            </ul>
        </div>
    </div>
</section>