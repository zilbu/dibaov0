<style>
    .main-main {
        background: #fff;
    }

</style>
<section class="padding-30t">
<div class='container'>
    <?php
    if (isset($_GET['id'])) {
        ?>
        <div class="box-head margin-30b">
            <div class="row">
                <div class="col-md-9">
                    <ul class="brkum1">
                        <li><a href="javascript:;">Miền Bắc</a></li>
                        <li><a href="javascript:;">Hà Nội</a></li>
                    </ul>
                </div>
<!--                <div class="col-md-3">-->
<!--                    <div class="box">-->
<!--                        <form action="" method="post">-->
<!--                            <div class="find-box">-->
<!--                                <button><i class="fa fa-search"></i></button>-->
<!--                                <input type="text" name="" value="" placeholder="Tìm kiếm">-->
<!--                            </div>-->
<!--                        </form>-->
<!--                    </div>-->
<!--                </div>-->
            </div>
        </div>
        <div class="border-t padding-30t">
            <div class="row">
                <ul class="list box-news3">
                    <?php
                    $data = sql_query_data("locationitem", ' active = "1" and provinces= "' . $_GET['id'] . '" ', '', array('id' => 'DESC'), array('id', 'title', 'image', 'provinces', 'location', 'address', 'phone'), 0, 600);
                    $i = 0;
                    $arrMien = array(
                        '0' => 'Miền Bắc',
                        '1' => 'Miền Trung',
                        '2' => 'Miền Nam',
                    );
                    foreach ($data as $val) {
                        echo '<li class="col-md-4 col-sm-6">
                                                <div class="item center">
                                                    <a href="javascript:;" title="" class="thumb"><img src="/' . $val['image'] . '" alt="" /></a>
                                                    <a href="javascript:;" title="" class="title">' . $arrMien[$val['provinces']] . '</a>
                                                    <p class="addr">' . $val['address'] . ' Hotline: ' . $val['phone'] . '</p>
                                                </div>
                                            </li>';
                    }

                    ?>
                </ul>
            </div>
        </div>
        <section class="margin-20b">
            <div class="container">
                <div class="heading">
                    <h3>Có thể bạn quan tâm</h3>
                </div>
                <div class="row">
                    <ul class="list box-news3">
                        <?php
                        $data= sql_query_data("news",' active = "1" ','',array('id' => 'DESC'),array('id','view','log','title','brief','category','image'),0,3);
                        if($data) {
                            $i = 0;
                            foreach($data as $result){
                                // $url	=	$result['urlfr'];
                                $title	=	$result['title'];
                                $category	=	$result['category'];
                                $id	=	$result['id'];
                                $date   =   date('Y-m-d',strtotime($result['log']));
                                $url='/'.removeSpecialChars(removesign($title)).'-c'.$category.'n'.$id.'.html';
                                echo ' <li class="col-md-4 col-sm-6">
                                    <div class="item">
                                        <a href="'.$url.'" title="'.$title.'" class="thumb"><img
                                                    src="/'.$result['image'].'" alt="'.$title.'"/></a>
                                        <span class="uptime">'.$date.'</span>
                                        <a href="'.$url.'" title="" class="title">'.$title.'</a>
                                    </div>
                                </li>';
                            }
                        }
                        ?>
                    </ul>
                </div>
            </div>
        </section>
        <?php
    } else {
    ?>
    <div class="row" style="margin-bottom: 60px;">
        <div class="col-md-6 col-md-offset-1">

            <h3>DANH SÁCH CỬA HÀNG TOÀN QUỐC</h3>
            <p>Không ngừng cải thiện nâng cao chất lượng sản phẩm cũng như dịch vụ, chỉ sau 4 năm ra mắt, DIBAO hiện
                nay là một tên tuổi thuộc hàng “top” trong thị trường xe điện, với gần 200 cửa hàng phủ sóng trên
                toàn quốc</p>

            <h2>Mua xe điện DIBAO ở đâu</h2>
            <div class="box stores">
                 <div id="ContentPlaceHolder1_desssss" class="div-showrom">
                    <p class="p-showrom-hanoi">I. Showroom</p>
                </div>
                <ul class="list tree-store" id="list_store">

                    <!-- Miền Bắc -->
                    <?php
                    $dataAddress = sql_query_data("provinces", ' active = "1" ', '', array('id' => 'DESC'), array('id', 'name', 'mien'), 0, 600);
                    $arrMien = array(
                        '0' => 'Miền Bắc',
                        '1' => 'Miền Trung',
                        '2' => 'Miền Nam',
                    );
                    $array = array(6, 12, 18, 24, 30, 36, 42, 48);
                    for ($i = 0; $i < 3; $i++) {
                        echo '<li><a href="javascript:;">'.$arrMien[$i].'</a>';
                         echo '<ul>';
                        foreach ($dataAddress as $val) {
                            if ($val['mien'] == $i) {
                                $url = '/dai-ly/' . $val['id'];
                                echo '<li>
                                        <a href="' . $url . '" title="' . $val['name'] . '"><i class="fa fa-chevron-right"></i> ' . $val['name'] . '</a>
                                    </li>';
                            }
                        }
                        echo '</ul>
                            </li>';
                    }

                    ?>

                </ul>
            </div>
        </div>
        <div class="col-md-4 col-md-offset-1 hidden-xs">
            <div class="center">
                <img src="/pictures/VietNamMap.png" alt="">
            </div>
        </div>
    </div>
        <?php
    }
    ?>

</div>
</section>
<script type="text/javascript">
    $(function () {
        $('#list_store').find('li a').bind('click', function (e) {
            if ($(this).attr('href') == 'javascript:;') {
                e.preventDefault();
                $(this).parent().toggleClass('on')
            }

        })
    })

</script>