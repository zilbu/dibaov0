<div class="container">
<div class="row content">
        <div class="col-lg-12 col-md-12 col-sm-12">
            <div class="tt_module">
				<h2 class="title-block">
					<a href="/" class="home">Trang chủ</a> <img src="/images/arrow-bc.png"> <a href="#">Hệ thống của hàng</a> 	
				</h2>
			</div>
        </div>
		<?php
			if(isset($_GET['keymap'])){
				$data= sql_query_data("locationitem",' active = "1" and id = "'.$_GET['keymap'].'" ','',array('id' => 'DESC'),array('id','title','address','image','info'),0,1000);
				foreach($data as $val){
					$address=$val['address'];
					$dataimage=$val['image'];
				}
			}
		?>
        <div class="col-lg-12 col-md-12 col-sm-12">
            <div class="page-content">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12">
                        <h2 style="font-size:15px;float:left;padding-bottom:20px;" class="single-title">Địa chỉ <span class="red"><?php echo $data[0]['address'];?></span></h2>
                    </div>
					<div class="store-info col-lg-12 col-md-12 col-sm-12">
						<div class="l">
							<img  src="/<?php echo $data[0]['image'];?>" alt="">
							</div>
							<div class="r">
							<p class="info-title">Thông tin liên hệ</p>
							<p>- <strong>Địa chỉ:</strong> <?php echo $data[0]['address'];?></p>
							<p>- <strong>Điện thoại:</strong> 04.22108888</p><p class="info-title">Thời gian hoạt động</p>
							<p>8 giờ - 22 giờ (kể cả CN và ngày lễ)</p>
							<p class="info-title">Giới thiệu và chỉ dẫn</p>
							<p><?php echo $data[0]['info']?></p>
						</div>
					</div>
                    <section class="main-content col-lg-9 col-md-9 col-sm-9 ">
                        <script type="text/javascript">
							var gmarker = {"Store_ID":1,"Store_Name":"CÔNG TY TNHH TM XNK THẾ GIỚI XE ĐẠP ĐIỆN",
							"Store_Addr":"<?php echo $data[0]['address'];?>",	
							"Store_Mobile":"092.6708888",					
							"Store_Phone":"04.22108888",			
							"Store_Fax":"099496 8888","Store_Email":"info.thgioixedien@gmail.com",		
							"Store_Latitude":"<?php echo getArrayJsLat($address);?>",	
							"Store_Longitude":"<?php echo getArrayJsLong($address);?>"	
							}		
						</script>
                        <div id="mapStore" style="width: 100%; height: 500px; position: relative; overflow: hidden; transform: translateZ(0px); background-color: rgb(229, 227, 223);">
						
						</div>
                    </section>
                    <aside class="sidebar col-lg-3 col-md-3 col-sm-3">
                        <ul>
						<?php
								$data= sql_query_data("locationitem",' active=1 ','',array('id' => 'DESC'),array('id,title,address'),0,1000);
								if(isset($data)){
									foreach($data as $val){
										echo '<li><i class="green">→</i> '.$val['address'].' <a href="/xem-ban-do/'.$val['id'].'.html" target="_blank">[ Bản đồ ]</a></li>';
									}
								}
							?>
						</ul>

                    </aside>
                </div>
            </div>
        </div>
    </div>
	<script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?sensor=false"></script>

    <script type="text/javascript">
        var iconBase = 'http://xedien.com.vn/style/img/iconmap.png';
    </script>
	<style>
		.store-img {
			    background: url('http://thegioixedien.com.vn/upimages/weblinks/logo-xe-dap-dien_copy.png') no-repeat center top;
    margin-right: 10px;
    width: 249px;
    height: 88px;
    display: inline-block;
    background-size: 42% 100%;
		}
	</style>
    
    
    <script type="text/javascript">
        function initialize() {
            var myLatlng = new google.maps.LatLng(gmarker.Store_Latitude, gmarker.Store_Longitude);
            var mapOptions = {
                zoom: 16,
                center: myLatlng,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            };

            var map = new google.maps.Map(document.getElementById('mapStore'), mapOptions);

            var contentString = '<div class="store-info">';
            contentString += '<div class="store-img"></div>';
            contentString += '<div class="store-content">';
            contentString += '<p><strong>' + gmarker.Store_Name + '</strong></p>';
            contentString += gmarker.Store_Addr;
            contentString += '<br>Hotline: ' + gmarker.Store_Mobile + ' - ĐT: ' + gmarker.Store_Phone;
            contentString += '<br>Hỗ trợ kĩ thuật: ' + gmarker.Store_Fax;
            contentString += '<br>Email: ' + gmarker.Store_Email;
            contentString += '</div>';
            contentString += '</div>';

            var infowindow = new google.maps.InfoWindow({
                maxWidth: 500,
                content: contentString
            });

            var marker = new google.maps.Marker({
                position: myLatlng,
                map: map,
                title: gmarker.Store_Name,
                icon: iconBase
            });

            infowindow.open(map, marker);

            //google.maps.event.addListener(marker, 'click', function () {
            //    infowindow.open(map, marker);
            //});
        }
        
        google.maps.event.addDomListener(window, 'load', initialize);
    </script>
</div>