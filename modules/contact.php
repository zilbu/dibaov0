<?php if (substr_count($_SERVER['PHP_SELF'], '/contact.php') > 0) die ("You can't access this file directly..."); ?>
<script>
    function checkcontact() {
        str = document.check.phone.value;
        if (document.check.fullname.value == '') {
            alert('<?php echo $display['namecheck']; ?>');
            document.check.fullname.focus();
            document.check.fullname.style.border = 'solid 1px red';
            return false;
        }
        else {
            document.check.fullname.style.border = 'solid 1px #b5b6c4';
        }
        if (document.check.address.value == '') {
            alert('<?php echo $display['addcheck']; ?>');
            document.check.address.focus();
            document.check.address.style.border = 'solid 1px red';
            return false;
        }
        else {
            document.check.address.style.border = 'solid 1px #b5b6c4';
        }
        if (str == '') {
            alert('<?php echo $display['phonecheck']; ?>');
            document.check.phone.focus();
            document.check.phone.style.border = 'solid 1px red';
            return false;
        }
        else {
            document.check.phone.style.border = 'solid 1px #b5b6c4';
        }
        if (isNaN(str)) {
            alert('<?php echo $display['fnumbercheck']; ?>');
            document.check.phone.focus();
            document.check.phone.style.border = 'solid 1px red';
            return false;
        }
        else {
            document.check.phone.style.border = 'solid 1px #b5b6c4';
        }
        if (document.check.email.value == '') {
            alert('<?php echo $display['emailcheck']; ?>');
            document.check.email.focus();
            document.check.email.style.border = 'solid 1px red';
            return false;
        }
        else {
            document.check.email.style.border = 'solid 1px #b5b6c4';
        }
        if (document.check.content.value == '') {
            alert('<?php echo $display['contentcheck']; ?>');
            document.check.content.focus();
            document.check.content.style.border = 'solid 1px red';
            return false;
        }
        else {
            document.check.content.style.border = 'solid 1px #b5b6c4';
        }
        if (document.check.security_code.value == '') {
            alert('<?php echo $display['introimage']; ?>');
            document.check.security_code.focus();
            document.check.security_code.style.border = 'solid 1px red';
            return false;
        }
        else {
            document.check.security_code.style.border = 'solid 1px #b5b6c4';
        }
        mail = /^[a-z][a-z0-9_\.]*\@[a-z-]*\.[a-z]*[a-z0-9_\.]*/g;
        if (mail.test(document.check.email.value) == false) {
            alert('<?php echo $display['emailcheckerror']; ?>');
            document.check.email.focus();
            document.check.email.style.border = 'solid 1px red';
            flag = false;
            return false;
        }
        else {
            document.check.email.style.border = 'solid 1px #b5b6c4';
        }
    }
</script>
<?php //include($include_dir."top-main.php");?>
<div class="container">
    <div class="row">
        <section class="padding-40t margin-30b">
            <div class="container">
                <div class="row">
                    <div class="col-md-6 col-md-offset-2 contact-form">
                        <div class="box">
                            <h3>Contact us</h3>
                            <form class="form" method="post" action="/thong-tin-lien-he.html" onSubmit="return checkcontact();">
                                <div class="form-group">
                                    <div class="box">
                                        <label class="col-md-4">Full name *</label>
                                        <div class="col-md-8">
                                            <input type="text" name="fullname" value="" class="form-control">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="box">
                                        <label class="col-md-4">Email *</label>
                                        <div class="col-md-8">
                                            <input type="text" name="email" value="" class="form-control">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="box">
                                        <label class="col-md-4">Address</label>
                                        <div class="col-md-8">
                                            <input type="text" name="address" value="" class="form-control">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="box">
                                        <label class="col-md-4">Phone number *</label>
                                        <div class="col-md-8">
                                            <input type="text" name="phone" value="" class="form-control">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="box">
                                        <label class="col-md-4">Message *</label>
                                        <div class="col-md-8">
                                            <textarea name="content" rows="8" class="form-control"></textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group center">
                                    <button class="btn btn-submit">send</button>
                                </div>
                            </form>

                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>