<section class="slider">
    <div class="carousel" id="main_slider">
        <?php
        $silder = sql_query('slides', ' active = 1', ' level ASC, log ASC ', 0,5 );
        if($silder) {
            $i = 0;
            foreach($silder as $result){
                $url	=	$result['org_web'];
                $title 	=   $result['org_name'];
                $brief 	=   $result['brief'];
                $img	=	$result['image'];
                echo '<div class="item">
                        <a href="'.$url.'" title="'.$result['title'].'"><img src="/'.$img.'" alt="'.$result['title'].'"></a>
                    </div>';
            }
        }
        ?>
    </div>
</section>
<section class="mid-box">
    <div class="container">
        <ul class="row list">
            <li class="col-md-4 col-sm-4">
                <a style="display: block" href="/gioi-thieu-ve-chung-toi-n38">
                    <img src="pictures/anh1.1.png"/>
                    <div class="entry">
                        <span>Về chúng tôi</span>
                        <span class="fa fa-chevron-circle-right"></span>
                    </div>
                </a>
            </li>
            <li  class="col-md-4 col-sm-4">
                <a style="display: block" href="/xe-may-dien">
                    <img src="pictures/anh2.1.png"/>
                    <div class="entry">
                        <span>Sản phẩm</span>
                        <span class="fa fa-chevron-circle-right"></span>
                    </div>
                </a>
            </li>
            <li class="col-md-4 col-sm-4">
                <a style="display: block" href="/tin-tuc-n24">
                    <img src="pictures/anh3.1.png"/>
                    <div class="entry">
                        <span>Tin tức</span>
                        <span class="fa fa-chevron-circle-right"></span>
                    </div>
                </a>
            </li>
        </ul>
    </div>
</section>
<section>
    <div class="container">
        <div class="more">
            <?php
            $moredata = sql_query('weblinks', ' active = 1 and advertising = 100', ' level ASC, log ASC ', 0,5 );
            if($silder) {
                $i = 0;
                foreach($moredata as $result){
                    $url    =   $result['org_web'];
                    $title  =   $result['org_name'];
                    $brief  =   $result['brief'];
                    $img    =   $result['image'];
                    echo '<div class="col-md-6 margin-10b">
                                            <a href="'.$url.'" title="'.$result['title'].'"><img src="/'.$img.'" alt="'.$result['title'].'"></a>
                                        </div>';
                }
            }
            ?>
        </div>
    </div>
</section>
<section class="module">
    <div class="container">
        <div class="heading">
            <a href="/noi-bat"><h3>SẢN PHẨM NỔI BẬT</h3></a>
        </div>
        <div class="row">
            <ul class="list list-product">
                <?php
                $Itemhome = sql_query_data('articles', 'active = 1 ', '', array('level' => 'ASC'), array('id,title,urlitem,urlitem1,brief,price,promotion,image'), 0, 9);
                if ($Itemhome) {
                    $i = 0;
                    foreach ($Itemhome as $result) {
                        $url = $result['urlitem'];
                        $title = $result['title'];
                        $urlitem = $result['urlitem'].'.html';
                        $price = $result['price'];
                        $image = $result['image'];
                        $i++;
                        $foAr = array(0, 1, 2, 3, 13, 14, 15, 19, 20, 21);
                        $arrRow = array(3, 6, 9, 12, 15);
                        if (in_array($i, $foAr)) {
                            $class = "itemmain";
                        } else {
                            $class = "itemmain1";
                        }
                        echo '<li class="col-md-4 col-sm-6">
                                <div class="inner">
                                    <div class="thumb">
                                        <a href="' . $urlitem . '" title="' . $title . '"><img src="/' . $image . '" alt="' . $title . '" /></a>
                                        <div class="shadow-info">
                                            '.$result['brief'].'
                                        </div>
                                    </div>
                                    <div class="entry">
                                        <a href="' . $urlitem . '" title="'. $title .'" class="title">'. $title .'</a>
                                        <div class="pr">
                                            Giá bán từ : <span class="price">' . $price . 'đ</span>
                                        </div>
                                        <a href="' . $urlitem . '" title="Chi tiết" class="link-detail">Xem chi tiết <span class="fa fa-chevron-right"></span></a>
                                    </div>
                                </div>
                            </li>';

                    }
                }
                ?>

            </ul>
        </div>
    </div>
</section>
<section class="module sp">
    <div class="container">
        <div class="row">
            <div class="col-md-4" style="margin-bottom: 10px">
                <div class="inner">
                    <a href="/daily.html" title="Tìm cửa hàng ủy nhiệm gần bạn">
                        <span class="fa fa-globe"></span>
                        <h3>Cửa hàng Dibao ủy nhiệm</h3>
                        Tìm cửa hàng ủy nhiệm gần bạn
                        <i class="fa fa-chevron-right"></i>
                    </a>
                </div>
            </div>
            <div class="col-md-4 bottom5" style="margin-bottom: 10px">
                <div class="inner">
                    <a href="/bao-hanh-n32" title="Thông tin về dịch vụ vụ bảo hành và bảo trì">
                        <span class="fa fa-expand-arrows-alt"></span>
                        <h3>Dich vụ sau bán hàng</h3>
                        Thông tin về dịch vụ vụ
                            bảo hành và bảo trì
                        <i class="fa fa-chevron-right"></i>
                    </a>
                </div>
            </div>
            <div class="col-md-4 bottom5" style="margin-bottom: 10px">
                <div class="inner">
                    <a href="/van-chuyen-n19" title="Vận chuyển">
                        <span class="fa fa-file-alt"></span>
                        <h3>Bảng giá & catalog</h3>
                        Vận chuyển
                        <i class="fa fa-chevron-right"></i>
                    </a>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="module news">
    <div class="container">
        <div class="heading">
            <h3>TIN TỨC NỔI BẬT</h3>
        </div>
        <div class="row">
            <ul class="list box-news3">

                <?php
                $data= sql_query_data("news",' active = "1" AND level="1" ','',array('id' => 'DESC'),array('id','view','log','title','brief','category','image'),0,3);
                if($data) {
                    $i = 0;
                    foreach($data as $result){
                        // $url	=	$result['urlfr'];
                        $title	=	$result['title'];
                        $category	=	$result['category'];
                        $id	=	$result['id'];
                        $date   =   date('Y-m-d',strtotime($result['log']));
                        $url='/'.removeSpecialChars(removesign($title)).'-c'.$category.'n'.$id.'.html';
                        echo ' <li class="col-md-4 col-sm-6">
                                    <div class="item">
                                        <a href="'.$url.'" title="'.$title.'" class="thumb"><img
                                                    src="/'.$result['image'].'" alt="'.$title.'"/></a>
                                        <div class="post-time"><span class="uptime">'.$date.'</span></div>
                                        
                                        <a href="'.$url.'" title="" class="title">'.$title.'</a>
                                    </div>
                                </li>';
                    }
                }
                ?>
            </ul>
        </div>
        <div class="row hidden-xs">
            <?php
            $data= sql_query_data("news",' active = "1" ','',array('id' => 'DESC'),array('id','log','view','log','title','brief','category','image'),0,3);
            if($data) {
                $i = 0;
                foreach($data as $result){
                    $i++;
                    // $url	=	$result['urlfr'];
                    $title	=	$result['title'];
                    $category	=	$result['category'];
                    $id	=	$result['id'];
                    $date   =   date('Y-m-d',strtotime($result['log']));
                    $url='/'.removeSpecialChars(removesign($title)).'-c'.$category.'n'.$id.'.html';
                    if($i==1){
                        echo ' <div class="col-md-7 col-sm-7">
                            <div class="inner hot-item">
                                <a href="' . $url . '" title="' . $title . '" class="thumb"><img src="/' . $result['image'] . '" alt="' . $title . '"/></a>
                                <div class="entry">
                                    <a href="' . $url . '" title="" class="title">' . $title . '</a>
                                    <div class="post-time">
                                        ' . date('Y-m-d', strtotime($result['log'])) . '
                                    </div>
                                </div>
                            </div>
                        </div>';
                    }else {
                        if($i==2){
                            echo '          
                            <div class="col-md-5 col-sm-5">
                                <ul class="list news-right">';
                        }
                        echo '<li class="item row">
                                <div class="col-xs-6 col-sm-6">
                                    <a href="' . $url . '" title="' . $title . '" class="thumb"><img src="/' . $result['image'] . '" alt="' . $title . '"/></a>
                                </div>
                                <div class="col-xs-6 col-sm-6">
                                    <div class="info-right">
                                        <a class="row2d mb10 title" href="' . $url . '" title="' . $title . '">' . $title . '</a>
                                       
                                        <p class="row3d fz12">
                                            ' . $result['brief'] . '
                                        </p>
                                        <div class="post-time">
                                            ' . date('Y-m-d', strtotime($result['log'])) . '
                                        </div>
                                    </div>
                                </div>
                            </li>';
                        if($i==count($data)){
                            echo '          
                                </ul>
                            </div>';
                        }
                    }
                }
            }
            ?>

        </div>
        <div class="center" style="margin-top:15px;">
            <a href="/tin-tuc-n24" title="Xem Thêm" class="link-detail">Xem Thêm <span
                        class="fa fa-chevron-right"></span></a>
        </div>
    </div>
</section>