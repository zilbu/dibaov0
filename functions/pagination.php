
<?php

include "pagination.class.php";

$p = new pagination($number);

// Số trang còn lại tính từ đầu
$p->paginationLeft = 3; 

// Số trang còn lại tính từ cuối
$p->paginationRight = 3; 

// link href
//if(isset($_GET['order']))
{
$p->path = ''.$url_link_p.'curpage=[pageno]'; // or $p->path = 'curpage/[pageno]/';
}
//else
//$p->path = '?curpage=[pageno]'; // or $p->path = 'curpage/[pageno]/';

// Số sản phẩm	
$p->setCount($count); 

// Kiểm tra trang nào
if(isset($_GET['curpage'])){
	$p->setStart($_GET['curpage']);
}
// In đoạn phân trang
?>
<style>
	#pagination{ 
		clear:both;
		float: right;
		margin: 0 auto;
		padding-right: 50px;
		}
	#pagination a { 
		display:block;
		float:left;
		padding: 0 6px;
		text-align:center;
		line-height: 18px;
		background:#fff;
		text-decoration:none;
		font-weight:bold;
		border:solid 1px #ccc;
		margin-left: 2px;
	}
	
	#pagination a:hover {
	/*	background:#CCC; */
	}
	
	#pagination a.active {
	/*	background:#CCC; */
	color: red;
	}
	
	#pagination a.prev, a.next {
	}
	
	#pagination a.active, a.more {
		cursor:default;
	}
	
	#pagination p {
		clear:left; 
		padding:50px; 
		background:#CCC
	}
	
	
</style>
