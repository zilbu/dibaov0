<?php
	function getArrayJsLat($address=''){
		$url = "http://maps.googleapis.com/maps/api/geocode/json?address=".urlencode($address)."&sensor=false";
		$result_string = file_get_contents($url);
		$result = json_decode($result_string, true);
		$lat = $result['results'][0]['geometry']['location']['lat'];
		$dataMaps= $lat;
		return $dataMaps;
	}
	function getArrayJsLong($address=''){
		$url = "http://maps.googleapis.com/maps/api/geocode/json?address=".urlencode($address)."&sensor=false";
		$result_string = file_get_contents($url);
		$result = json_decode($result_string, true);
		$long = $result['results'][0]['geometry']['location']['lng'];
		$dataMaps= $long;
		return $dataMaps;
	}

	function urlItem($str,$str1){
		if($str != ""){
			$url='/'.$str.".html";
		}else{
			$url='/'.$str1.".html";
		}
		return $url;
	}
	function urlNews($title,$category,$id){
		$url='/'.removeSpecialChars(removesign($title)).'-c'.$category.'n'.$id.'.html';
		return $url;
	}
	function CutStr($str,$number = 150,$dot=true)

{

    $leng = strlen($str);

    if($leng > $number)

    {

        $temp  = substr($str, 0, $number);

        $str   = substr($temp, 0, strrpos($temp, " ")).($dot ? "..." : "");

    }

    

    return $str;

}
    function products($id_avd,$number,$where = '',$order = ' level ASC, log desc ')
    {
	global $siteUrl;
	global $display;
		$products = sql_query('articles',' active = 1 '.$where.' ' ,$order,0, $number); // avdproducts like "%'.$id_avd.'%" and
		if ( $products !='' )
		{
			$i=0;
			foreach( $products as $next )
			{
				$i++;
				$title = stripslashes($next['title']);
				$id = $next['id'];
                $manufacturer = $next['manufacturer'];
				if(ctype_digit($manufacturer)){
					$tblname = "properties";
					$manufacturer = sql_tb($tblname,' id = "'.$manufacturer.'" ','name');
				}
                $image = $next['image'];
                $category = $next['category'];
                $capacity = $next['capacity'];
                $code = $next['code'];
                $content = $next['content'];
                $urlitem = $next['urlitem'];
                $promotion = $next['promotion'];
                $price = $next['price'];
				if($promotion or $promotion != 0 )
					$promotion     =  bsVndDot($promotion).' '.$display['part_meny'];
                $urlitem1 = $next['urlitem1'];
					// $price     =  bsVndDot($next['price']).' '.$display['part_meny'];
				// if(!$next['price'] or !$next['price'] == ""){ $price = 'Liên hệ';};
				if($urlitem1)
					$url= '/'.$urlitem1.'.html';
				else if($urlitem)	
					$url= '/'.$urlitem.'.html';
				// $image = substr($image, 8, strlen($image));
				// $image = 'upimages/.thumbs'.$image;
				?>
				<li style="position:relative;" class="itemPro">
					<span class="space-img"><a href="<?php echo $url; ?>" title="<?php echo $title; ?>" class="img"><img src="/<?php echo $image;?>" /></a></span>
					<h3><a href="<?php echo $url; ?>" title="<?php echo $title; ?>"><?php echo $title; ?></a></h3>
					<div class="all_price">
						<span class="pricee">Giá: <?php echo $price; ?></span>
					</div>
					<i style="position: absolute;z-index: 99;top: 0px;left: 0px;"><img src="/style/img/ico4.png" style=" width: 27px; height: 77px; margin-left: 0px;" /></i>				
				</li>
			<?php
			}
		}   	
    }	
	
	
/*
* Founction Query data of table
*@ $_tbName name table data
*@ $_where	proviso data
*@ $orderby	 order
*@ $select	name data select
*/	
	
	//class getSqlQuery{
		
		function sql_query_data($_tbName, $_where, $cat_id = '', $_orderby, $select = '', $_from = 0, $_amount = 100,$test ="")
		{
			global $link;
			//$_tbName = preg_replace("/_cat$/","",$_tbName); 
			$_table = $_tbName."_cat";
			$return = array();
			$pinfo = get_by_id($_table,$cat_id);
			$_plevel = $pinfo['level'];
		
		
			$where = array( "1" );
			
			if(is_array($_where)) //Kiểm tra có phải mảng
			{
				foreach($_where as $_wherekey => $_whereitem)
				{
					if(is_array($_whereitem))
					{
						$tmpwhere = " ".$_tbName.".".$_whereitem[0]." ";
						
						if($_whereitem[2] == '>=' || $_whereitem[2] == '>' || $_whereitem[2] == '<=' || $_whereitem[2] == '<' || $_whereitem[2] == 'like' || $_whereitem[2] == 'in' || $_whereitem[2] == '!=' || $_whereitem[2] == '=' )
						{
						
						
							if(is_numeric($_whereitem[1]))
							{
								 $tmpwhere .= $_whereitem[2]." ".$_whereitem[1]." ";
							}
							else if($_whereitem[2] == 'in')	$tmpwhere .= $_whereitem[2]." (".$_whereitem[1].") ";
							
							else $tmpwhere .= $_whereitem[2]." '".$_whereitem[1]."' ";
								
							$where[] = $tmpwhere;
						}
						else {
							if(is_numeric($_whereitem[1]))
								$tmpwhere .= " = ".$_whereitem[1]." ";
								else $tmpwhere .= " = '".$_whereitem[1]."' ";
								
							$where[] = $tmpwhere;
						}
					}
					else if($_whereitem) $where[] = $_whereitem;
				}
				
				$where = implode(" and ",$where);
			}
			else $where = $_where;	
			
			//Search item
			/*if(isset($_REQUEST['keyword']))
			{
				$_keyword = $_REQUEST['keyword'];
				$where = ' '.$where.' and ( '.$_tbName.'.title like "%'.$_keyword.'%" or '.$_tbName.'.content like "%'.$_keyword.'%"  or '.$_tbName.'.code like "%'.$_keyword.'%" )';
			}*/
			//Order 
			$order = [];
			if(is_array($_orderby))
			{
				foreach($_orderby as $_orderbykey => $_orderbyitem )
				$order[] = " ".$_tbName.".".$_orderbykey." ".$_orderbyitem." ";
			}
			else $order = $_orderby;
			if($order != "" and is_array($order)) $order = implode(" , ",$order);
			
			//build select clause
			if($select)
			{
				if(is_array($select))
				{
					$_select = implode(",",$select);
				}
				else $_select = $select;		
			}
			else			
			{	
				$_select = [];
				$_get_field = mysqli_query($link,"SHOW COLUMNS FROM ".$_tbName);
				if($_get_field && mysqli_num_rows($_get_field))
				{
					while($_row = mysqli_fetch_assoc($_get_field))
					{
						$_select[] = " ".$_tbName.".".$_row['Field']." as ".$_row['Field']." ";
					}
				}
				
				if($_select) $_select = implode(",",$_select);
			}
			
			//build sql query
			if(!$_plevel)
				$_table = '';
			if($_select == "")$sql = "select * ";
			else $sql = "select $_select ";
			if($_plevel)
				$sql .= "from $_tbName , $_table where ";
			else
				$sql .= "from $_tbName where ";
			if($_plevel)
			{
				$sql .= "$_tbName.category = $_table.id and ";
				$sql .= "$_table.level like '$_plevel%' and ";
				$sql .= " $_table.lang = ".get_langID()." and ";
			}
			$sql .= " $_tbName.lang = ".get_langID()." and ".$where." ";
			if($order != "") $sql .= " order by ".$order."  ";
			if($_amount != "") $sql .= " limit ".intval($_from).", ".intval($_amount); 

			$do_sql = mysqli_query($link,$sql);
			
			if($do_sql && mysqli_num_rows($do_sql) > 0)
			{
				while($row_sql = mysqli_fetch_assoc($do_sql))
				{
					$return[] = $row_sql;
				}
			}			if($test != "")
			{ echo $sql.'<br/>';}
			return $return;
		}  
		
		
/////////////////////////////////////////////////

function cat_id($tblname, $path)
{
    global $link;
	$query ='select '.$tblname.'.*,'.$tblname.'_cat.level as category_level';
	$query.=' from '.$tblname.' inner join '.$tblname.'_cat';
	$query.=' on '.$tblname.'.category='.$tblname.'_cat.id';
	$query.=' where '.$tblname.'_cat.level like "'.$path.'%"';
	$doquery=mysqli_query($link,$query);
	if ($doquery and mysqli_num_rows($doquery) > 0)
	{
		$j=0;
		while($result = mysqli_fetch_array($doquery))
		{
			$j++;
			$id=$result['id'];
			$catid[] = $id;
		}
			
	}
		
	$catid = @implode(',',$catid);
	return 	$catid;	
}
	
	
	
	?>