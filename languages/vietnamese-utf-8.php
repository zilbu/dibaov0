<?php

$charset='utf-8';

$datetime_format='%h : %i : %s, %d / %m / %y';

$display=array(


		//--------- top page ---------------------------------
	'info_footer1' => ' CÔNG TY TNHH TM XNK THẾ GIỚI XE ĐẠP ĐIỆN. MST : 0106775832 . SỞ KẾ HOẠCH ĐẦU TƯ TP.HÀ NỘI ',
		'hotline_com1' => '0966.888887',
		'hotline_com2' => '04.22108888 - 0967.408888 - 092.6708888',
		'khieunai' => '0966.888887',
		'info_footer'=>'<li>Tổng đài bán hàng (8:30 - 21:30)</li>
							<li><i class="icons icon-phone"></i><strong>04.2210.8888</strong>, hoặc: <b>0967.408888</b></li>
							<li><i class="icons icon-mail-alt"></i><a href=""></a></li>
							<li>Gọi khiếu nại (8:15 - 21:30) : <i class="icons icon-phone"></i><strong style="color:red;">092.6708888</strong></li>
							<li><i class="icons icon-mail-alt"></i><a href=""></a></li>',
		
		
		'newss'=>'Tin tức nổi bật',
		'group'=>'Nhóm nhiên cứu',
		'linkweb'=>'Liên kết website',
		'linkeven'=>'http://fsh.org.vn/su-kien-a85',
		'address_support'=>'
			Địa chỉ: 8/50 Đào Duy Từ, Hoàn Kiếm, Hà Nội, Việt Nam.
			<br/>+Hotline:84 098 44 19 885
			<br/>+Email: info@halongtoursbooking.com',
		'yourinformation'=>'Thông tin người gửi',
		'infotour'=>'Cho chúng tôi biết thông tin tour bạn đã tham gia.',
		'commentservices'=>'Bình luận của bạn về dịch vụ của chúng tôi.',
		'cruiseoverview'=>'Thông tin tour',
		'booking'=>'Booking',
		'assessment'=>'Nhận xét khách hàng',
		'comments1'=>'Nhận xét',
		'rating'=>'Đánh giá',
		'datetime'=>'Thời gian',
		'ticketprice'=>'Giá gốc',
		'promotion'=>'Giá khuyến mại',
		'discount'=>'Giảm giá',
		'schedule'=>'Lịch trình',
		'total'=>'Điểm',
		'view_more'=>'Xem thêm',
		'photos'=>'Hình ảnh',
		'cruisequality'=>'Dịch vụ',
		'fooddrink'=>'Ăn uống',
		'cabinquality'=>'Khách sạn',
		'staffquality'=>'HD viện',
		'entertainment'=>'Lịch trình',
		'tourinfo'=>'Thông tin tour',
		'differencebettwen'=>'Sự khác biệt giữa',
		
		'whithh'=>'Tại sao chọn chúng tôi?', 
		'whithc'=>'
			+ Giá tốt nhất
			<br/>+ Chất lượng tốt nhất
			<br/>+ The best Popular
			<br/>+ The best Popular
		',
		'contact_banner'=>'
			<b>Email:</b> sales@halongsapatoursbooking.com - <b>Website:</b> halongsapatoursbooking.com
			<br/> Hỗ trợ dịch vụ: 24 / 24
		',
		'hotline'=>'Book online or call <b>(084) - 4 - 66528592</b>',
		
		'choselanguage'=>'Chọn ngôn ngữ',
		'coquan'=>'Cơ quan ban hành',
		'year'=>'Năm phát hành',
		'filename'=>'Tên file',
		'downloadfile'=>'Tải file',
	    'seemore'=>'Xem thêm',
	    'readnews'=>'Tin đọc nhiều',
	    'newreceived'=>'Tin mới nhận',
		'repeatpassword'=>'Nhập lại mật khẩu',
		
		'homepage'=>'Trang chủ',
		'contact'=> 'Liên hệ',
		'contact_phone'=>'',
		'aboutus'=>'Giới thiệu',
		'detailsupport'=>'Chăm sóc khách hàng',
		'detail'=>'Chi tiết',
		'support'=>'Hỗ trợ',
		'aboutus'=>'Giới thiệu',
		'saleonline'=>'Bán hàng Online',
        'country'=>'Quốc gia',
        'manufacturer'=>'Hãng sản xuất',
        'origin'=>'Xuất xứ',
        'faqs'=>'Câu hỏi',
        'comment'=>'Bình luận',
        'commentyou'=>'Ý kiến bạn đọc',
        'vote'=>'Bình chọn',
        'yourcomment'=>'Ý kiến của bạn',
		'mapstreet'=>'BẢN ĐỒ CHỈ DẤN ĐƯỜNG',
		//----------------------------------------------------
		//--------- Book tour --------------------------------
		'photogallery'=>'Thư viện ảnh',
		'booking'=>'Booking',
		'socialnetworkinglinks'=>'Liên kết mạng xã hội',
		'booking_demand'=>'Đặt tour theo yêu cầu',
		'booking_info'=>'BTA chúng tôi có một đội ngũ đầy kinh nghiệm và luôn sẵn sàng để hỗ trợ quý khách đến bất cứ nơi nào bạn muốn',
		'name'=>'Tên',		
		'mr/mrs'=>'Ông/bà',
		'sex_info'=>'Ông - Bà',

		'enterkeywords'=>'Nhập từ khóa...',

		'firstname'=>'Tên',

		'tourtosee'=>'Tour xem nhiều',	

		'surname'=>'Họ',

		'nationality'=>'Quốc tịch',

		'comments'=>'Yêu cầu',

		'kindstourism'=>'Loại hình du lịch',

		'kindstourism_info'=>'Tour nội địa - Tour xuyên Việt ',

		'typehotel'=>'Loại khách sạn',

		'typehotel_info'=>' 1 Sao - 2 Sao - 3 Sao - 4 Sao - 5 Sao',

		'start'=>'Ngày đi',

		'end'=>'Ngày về',

		'typeroom'=>'Loại phòng',

		'typeroom_info'=>'Phòng đơn - Phòng bình dân',

		'adults'=>'Người lớn (12+)',

		'children'=>'Trẻ em (2->12)',

		'infant'=>'Trẻ sơ sinh (<2)',

		'tourfloating'=>'Tour nổi bật',				

		'areasactivity'=>'Lĩnh vực họat động',

		'destination'=>'Chọn điểm đến',

		'videos'=>'Video clips',
		
		//--------- main page --------------------------------
		'newscat'=>'Tin tức bổ ích',
		'salenewscat'=>'Tin khuyến mại',
		'hotnews'=>'Tin tiêu điểm',
		'listproductscat'=>'Sản phẩm nên xem',
		'productcatalog'=>'Danh mục sản phẩm',
		'topproductcat'=>'Sản phẩm nổi bật',
		'relatedresources'=>'Nội dung liên quan',
		'code'=>'Mã SP',
        'famousbrand'=>'Thương hiệu nổi tiếng',
        'sellers'=>'Sản phẩm bán chạy',
		'edit'=>'Thay đổi',
		'editpcompleted'=>'Thay đổi thành công !',
		'lostpass'=>'Lấy lại mật khẩu',
		'getpasscompleted'=>'Yêu cầu đã được chấp thuận !',
		'activecompleted'=>'Kích hoạt thành công !',
		'buy'=>'Mua hàng',
        'saleproducts'=>'Sản phẩm giảm giá',
        'pools_cat' =>'Bình chọn',
		'contactcontent'=>'',
		//----------------------------------------------------
		//--------- left page --------------------------------
		'weeknew'=>'Tiêu điểm trong tuần',
		'video'=>'Video clips',
		
		'old_news'=>'Tin khác',
		//----------------------------------------------------
		'b1'=>'Chọn hàng cần mua và bỏ vào giỏ hàng',
		'b2'=>'Xem và cập nhật giỏ hàng',
		'b3'=>'Điền đầy đủ thông tin vào đơn đặt hàng',
		'b4'=>'Hoàn tất đơn đặt hàng ',
		'count'=>'Số lượng ',
		'del'=>'Xóa',
		'total1'=>'Thành tiền',
		'next'=>'Tiếp tục mua hàng',
		'update_cart'=>'Cập nhật giỏ hàng',
        'paid' => 'Thanh toán',
        'paid1' => 'Hướng dẫn thanh toán',
        'viewcart'=>'Xem giỏ hàng',
		'cart'=>' Từng bước mua sản phẩm',
		'bank_payment'=>'Chuyển khoản qua ngân hàng',
		'house_payment'=>'Thanh toán khi giao hàng',
		'information'=>'Nội dung',
		'shop_payment'=>'Thanh toán tại cửa hàng',
		'online_payment'=>'Thanh toán qua NgânLượng.vn',
		'forum'=>'Diễn đàn',
		'null'=>'Không có sản phẩm',
		'progress_payment'=>'Hình thức thanh toán',
		'cart_null'=>'Hiện tại giỏ hàng không có sản phẩm',		
		'lastupdate'=>'Cập nhật ngày',
		'contactinfo'=>'Thông tin liên hệ',
		'productcat'=>'Sản phẩm',
		'supportonline'=>'Hỗ trợ trực tuyến',
		'advertise'=>'Liên kết quảng cáo',
		'searchadvance'=>'Tìm kiếm nâng cao',
		'exchange'=>'Tỷ giá ngoại tệ',
		'keyword'=>'Từ khóa',
		'catsearch'=>'Danh mục',
		'keyword'=>'Từ khóa',
		'searchall'=>'Tất cả',
		'pools'=>'Thăm dò ý kiến',
		'weather'=>'Thời tiết',
		'allinfo'=>'Thông tin hữu ích',
		'exchangesourse'=>'Theo Vietcombank',
		'gold'=>'Giá vàng',
		'stock'=>'Chứng khoán',
		'vtv'=>'Lịch phát sóng',
		'loto'=>'Kết quả xổ số',
		'weather'=>'Thời tiết',
		'regcompleted'=>'Quý khách đã hoàn thành việc đăng ký. Xin mời xác thực thông tin kích hoạt qua email !',
		//----------------------------------------------------
		//--------- right page -------------------------------
        'productCats' => "Danh mục sản phẩm",
		'errors'=>'Có lỗi trong quá trình thực hiện ! Mời quý khách thử lại !',
		'memberexists'=>'Email này đã tồn tại. Mời quý khách thử lại !',
		'invalidlogin'=>'Đăng nhập không thành công ! Mời quý khách thử lại',
		'logincompleted'=>'Đăng nhập thành công !',
		'memberlogout'=>'Đăng xuất thành công !',
		'accountinformation'=>'THÔNG TIN TÀI KHOẢN',
		'parentinformation'=>'THÔNG TIN VỀ MẸ/BỐ',
		'babyinformation'=>'THÔNG TIN VỀ BÉ',
		'parent_name'=>'Họ tên mẹ/bố',
		'nickname'=>'Nickname',
		'district'=>'Quận/Huyện',
		'city'=>'Tỉnh/Thành phố',
		'register'=>'Đăng ký',
		'login'=>'Đăng nhập',
		'baby_name'=>'Họ tên bé',
		'birthday'=>'Ngày sinh',
		'logout'=>'Thoát',
		'profile'=>'Cá nhân',
		'notereg'=>'Đăng ký thành viên<br />Lưu ý : Email đăng nhập phải là email có thực, để sử dụng nhận thông tin mật khẩu và liên hệ !<br />Mật khẩu tối thiểu phải có 6 ký tự !',
		'notereg1'=>'Bạn cần kích hoạt trước khi đăng nhập vào hệ thống.',
		'notereg2'=>'Bạn hãy nhập <b>Email đăng nhập</b> và <b>Mật khẩu</b> để đăng nhập vào hệ thống.',
		'memberinfo'=>'Thông tin thành viên',
		'emaillogin'=>'Email đăng nhập',
		'password'=>'Mật khẩu',
		'reset'=>'Nhập lại',
		'shoppingcart'=>'Giỏ hàng',
		'more_detail'=>'Xem chi tiết',
		'your_shoppingcart'=>'Giỏ hàng của bạn',
		'productname'=>'Tên sản phẩm',
		'productquantity'=>'Số lượng',
		'price'=>'Giá',
		'quicknews'=>'Tin mới',
		'slideproducts'=>'Giới thiệu sản phẩm',
		'products'=>'Sản phẩm',
		'pay'=>'Thanh tóan',
		'bestseler'=>'Sản phẩm Hot nhất',
		'downloadprices'=>'Download:',
		'downloadhere'=>'Mời quí khách tải báo giá tại đây',
		'selloffproducts'=>'Sản phẩm giảm giá',
		'addtoshoppingcart'=>'Thêm sản phẩm này vào giỏ hàng',
		'view_detail'=>'chi tiết',
		'weblinks'=>'Liên kết',
		'result'=>'Kết quả',
		'send'=>' Gửi ',
		'fullname'=>'Họ và tên',
		'email'=>'E-mail',
		'reenteremail'=>'Xác nhận email',
		'address'=>'Địa chỉ',
		'numberofcustomers'=>'Số khách',
		'adult'=>'Người lớn',
		'child'=>'Trẻ em',
		'phone'=>'Điện thoại',
		'fax'=>'Fax',
		'web'=>'Website',
		'job'=>'Nghề nghiệp',
		'jobadd'=>'Nơi công tác',
		'content'=>'Nội dung',
		'newcatalog'=>'Danh mục tin tức',
		//------------------------------------------------------
		//--------- bottom page --------------------------------
		'descriptions'=>'Giới thiệu',
		'services'=>'Dịch vụ',
        'copyright'=>'Copyright 2013 by Bluesky',
		'home'=>'Trang chủ',
		'intro'=>'Giới thiệu',
		'social'=>'Mạng xã hội',
		//-----------------------------------------------------
		
		//--------- detailnews page --------------------------------
		'new_articles'=>'Các thông tin mới đăng : ',
		'old_articles'=>'Các thông tin khác : ',
		'old_articles1'=>'Dịch vụ liên quan: ',
		'oldnews'=>'Tin tức đã đưa',
		'specifications'=>'Thông số kỹ thuật',
		//-----------------------------------------------------
		
		//--------- tocart page -------------------------------
		'delete_product'=>'Xóa sản phẩm này khỏi giỏ hàng',
		'viewnext'=>'Xem tiếp',
		'goback'=>'Trở lại',
		'updatecart'=>'Cập nhật',
		'selectall'=>'Chọn tất',
		'emtycart'=>'Giỏ rỗng ! Bạn hãy chọn sản phẩm !',
		'showcart'=>'Mời xem giỏ hàng',
		'link_partner' =>'Đối tác',
		'money'=>'Thành tiền: ',
		'the_first'=>'1',
		'the_last'=>'end',
		'part_meny'=>'VNĐ',
		'part_meny_cart'=>'VNĐ',
		'more_detail'=>'Xem chi tiết',
		'payment'=>'Thanh toán tiện lợi',
		'part_money'=>'đ',
        'detailcart'=> 'Chi tiết giỏ hàng',
        'order_cart'=>'Đặt hàng',
		//---------------------------------------------------
		
		//--------- Gallerys page -------------------------------
		'gallery'=>'Thư viện ảnh',
		
		//-------------------------------------------------------
		//--------- viewcart page -------------------------------
		'shoppingcart_info'=>'Cập nhật thông tin giỏ hàng',
		'totalamot'=>'Tổng tiền',
		'orderinfo'=>'Nhập thông tin đơn hàng',
		'amount'=>'Giá tiền',
		'quantity'=>'Số lượng',
		'promotion'=>'Khuyến mại',
		'promotionprice'=>'Giá bán',
		'productsinfo'=>'THÔNG TIN CHI TIẾT',
		'customerinfo'=>'THÔNG TIN KHÁCH HÀNG',
		'status'=>'Tình trạng',
		'manufacturer'=>'Hãng sản xuất',
		'groupproduct'=>'Nhóm bài viết',
		'sameproducts'=>'Sản phẩm cùng loại',
		'seemoreproducts'=>'Sản phẩm xem nhiều',
		'warranty'=>'Bảo hành',
		'detail'=>'Chi tiết',
		'cart'=>'Giỏ hàng',
		'yes'=>'Có hàng',
		'no'=>'Hết hàng',
		'order'=>'ĐẶT HÀNG',
		'sendfrom'=>'Người gửi',
		'sendto'=>'Người nhận',
		'update'=>'Cập nhật',
		'month'=>'tháng',
		'contentorder'=>'Nội dung',
		'noteorder'=>'Để thực hiện đặt hàng sản phẩm quý khách cần đăng ký thành viên. Bấm đăng ký <a class="brieftitle" href="?module=reg">tại đây</a>',
		'note1'=>'Cảm ơn bạn đã mua hàng tại bepxinh.vn !',
		'note2'=>'Cảm ơn bạn đã mua hàng tại bepxinh.vn ! Sau đây là chi tiết đơn đặt hàng của bạn :',
		'note3'=>'Món hàng của bạn sẽ được gửi trong vòng 1 -2 ngày làm việc sau khi chúng tôi nhận được thanh toán.',
		//---------------------------------------------------
		
		//--------- contact page------------------------------
		'contactsuccess'=>'<font color="#000">Cám ơn Quý khách đã gởi thông tin liên hệ đến chúng tôi!<br>
	      Chúng tôi sẽ phúc đáp yêu cầu của Quý Khách trong vòng 24 tiếng đồng hồ sau khi nhận được thông tin yêu cầu của Quý khách! <br></font>',
		'contactunsuccess'=>'Có lỗi sảy ra trong quá trình liên hệ!<br><font color="#0045ae">Quý khách hãy kiểm tra lại các thông tin đã nhập !<br> </font>',
		'contactsuccesstitle'=>'LIÊN HỆ THÀNH CÔNG !<br>',
		'info_com'=>'
			<br/>Add: No 8/50 Dao Duy Tu Lane, Hoan Kiem, Hanoi, Viet Nam.

			<br/>Tel: +84 4 3935 2024 | Fax: +84 4 3935 2023
			<br/>Hotline:84 098 44 19 885
			<br/>Email: info@camelcityhotel.com
			<br/>Web: www.camelcityhotel.com
		',	
		'coppyright'=>'<b>COPPYRIGHT © 2013 by ppri.org.vn',	
        'info_contact'=>'Thông tin',            
        //---------------------------------------------------
		'supportdownload'=>'Nếu bạn chưa nhận được thông báo download hãy bấm vào',
		'downloadlink'=>' đây ',
		'downloadend'=>'để tải về !',
		//--------- detailproducts page------------------------------
		'addtocart'=>'Đặt hàng',
		'monthwarranty'=>'tháng',
		'productdescription'=>'GIỚI THIỆU SẢN PHẨM',
		'new_products'=>'Sản phẩm mới cập nhật : ',
		'old_products'=>'Sản phẩm cũ hơn : ',
		'lastupdate'=>'Cập nhật mới nhất lúc',
		'infor_product'=>'Thông tin sản phẩm',
		'new_prices'=>'Bảng giá mới',
		'information_maker'=>'Thông tin hãng sản xuất',
		'vnd'=>'VNĐ (+VAT)',
		//'comment'=>'Lời bình',
		'postcomment'=>'Gửi lời bình',
		'commentempty'=>'Không có lời bình nào',
		'incode'=>'Sai mã bảo vệ',
		'secode'=>'Mã bảo vệ',
		'introimage'=>'Bạn hãy nhập đoạn mã nhìn thấy trong ảnh !',
		'posted'=>'lời bình',
        'totals'=>'Tổng tiền',
        'totalvaluecart'=>'Tổng giá trị đơn hàng',
        'title'=>'Giá trên đã trừ đi % giảm giá',
		//---------------------------------------------------
		//search font
		'helpsearchfrm'=>'Mời nhập các thông tin liên quan đến nội dung cần tra cứu vào form dưới đây để tiến hành tìm kiếm
						  Có thể gõ Tiếng Việt. Nhấn phím F12 để bật/tắt bộ gõ, phím F9 để chuyển đổi cách gõ ',
		'javaenable'=>'Cho phép thực thi Javascript',
		'groupcategory'=>'----- Danh sách nhóm tin -----',
		'helpsearchfrm1'=>'<p class="title10">- Có thể sử dụng dấu nháy kép ( " " ) để tìm kiếm chính xác chuỗi được đặt trong dấu nháy.</p>
						   <p class="title10">- Một số vấn đề về mã Tiếng Việt có thể đưa ra kết quả tìm kiếm không đúng.</p>
						   <p class="title10">- Thay vì tìm một cụm từ dài ngay từ đầu, bạn có thể tìm từ cụm từ ngắn cho đến dài để thu hẹp phạm vi tìm kiếm.<p>',
		'searchresult'=>'Kết quả tìm kiếm',
		'seriname'=>'Loại SP',
		'chooseprice'=>'Chọn giá',
		'price_from'=>'Từ',
		'price_to'=>'Đến',
		//others
        'download' => 'Tải về',
		'dname'=>'Tên báo giá',
		'dview'=>'Số lượt tải về',
		'customer'=>'Khách hàng',
		'customeronline'=>'khách',
		'partner'=>'Đối tác chiến lược',
		'noarticle'=>'Thông tin đang được cập nhật',
		'contactinfo'=>'Thông tin liên hệ',
		'contactprice'=>'Liên hệ',
		'bytitle'=>'Danh mục thông tin',
		'bycat'=>'Nhóm thông tin',
		'image'=>'Hình ảnh',
		'found'=>'Tổng số',
		'articles'=>'thông tin hiển thị trên',
		'page'=>'trang',
		'today'=>'Hôm nay, ',
		'day'=>'ngày',
		'visitor'=>'Số lượt truy cập',
		'diaryenum'=>'Đánh giá truy cập',
		'therevisitor'=>'Online nhiều nhất',
		'online'=>'Đang online',
		'useronline'=>'khách',
		'favorite'=>'Lưu địa chỉ website này vào danh mục Favorite của máy tính ?',
		'feedback'=>'Ý kiến phản hồi',
		'uncount'=>' 1 ',
		'continueview'=>'Xem tiếp',
		'viewbydate'=>'Xem theo ngày tháng',
		'select'=>'Chọn',
		'date'=>'Ngày tháng',
		'time'=>'thời gian',
		'month'=>'tháng',
		'previouspage'=>'Trang trước',
		'addtofavourite'=>'Lưu trang này vào danh sách Favourite',
		'setashomepage'=>'Đặt thường trực khi mở trình duyệt',
		'printversion'=>'Bản in',
		'category'=>'Danh mục',
		'speed'=>'Tốc độ',
		'back'=>'Trở lại',
		'views'=>'Lượt xem',
		'view'=>'Lượt xem',
        'view_all'=>'Xem tất cả',
        'view_more' => 'Đọc thêm...',
		'viewpro'=>'Số lượt xem sản phẩm',
		'jumpto'=>'Xem bản ghi số ',
		'mustbe'=>'Bạn phải nhập số khác 0',
		'record'=>'Bản ghi',
		'send2friend'=>'Gửi thông tin',
		'youremail'=>'E-mail của bạn',
		'receiveemail'=>'E-mail người nhận',
		'notvalid'=>'Không hợp lệ',
		'message'=>'Lời nhắn',
		'max'=>'Tối đa',
		'char'=>'ký tự',
		'index'=>'quay lại trang chủ',
		'passcheck'=>'Mật khẩu tối thiểu phải có 6 ký tự !',
		/*=================== check javascript shopping cart form===============================*/
		'checkfullname'=>'Mời bạn nhập họ tên !',
		//date month
		'january'=>'Tháng 1',
		'february'=>'Tháng 2',
		'march'=>'Tháng 3',
		'april'=>'Tháng 4',
		'may'=>'Tháng 5',
		'june'=>'Tháng 6',
		'july'=>'Tháng 7',
		'august'=>'Tháng 8',
		'september'=>'Tháng 9',
		'october'=>'Tháng 10',
		'november'=>'Tháng 11',
		'december'=>'Tháng 12',
		//end date month
		//title
		'mr'=>'Ông',
		'hotline'=>'(04) 36 85 8536',
		'mrs'=>'Bà',
		'dr'=>'Anh',
		'ms'=>'Chị',
		'namecheck'=>'Mời bạn nhập họ tên !',
		'addcheck'=>'Mời bạn nhập địa chỉ !',
		'phonecheck'=>'Mời bạn nhập số điện thoại !',
		'faxcheck'=>'Mời bạn nhập số fax !',
		'faxnumbercheck'=>'Số fax phải là số !',
		'fnumbercheck'=>'Số điện thoại phải là số !',
		'emailcheck'=>'Mời bạn nhập điền email !',
		'contactwithcheck'=>'Bạn phải chọn phương thức thanh toán !',
		'contentcheck'=>'Nội dung không thể để trống !',
		'emailcheckerror'=>'Địa chỉ email không hợp lệ !',
		'searchcheck'=>'Bạn phải nhập ký tự cần tìm !',
		//print webpage
		'nolink'=>'Không có liên kết để tạo bản in',
		'invalidlink'=>'Liên kết không hợp lệ',
		'print'=>'In thông tin',
		'closewindow'=>'Đóng cửa sổ',
		'printnote'=>'Bản in của bài viết ( mặc định khổ A4 )',
		);

?>