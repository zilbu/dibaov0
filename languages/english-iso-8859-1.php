<?php

$charset='utf-8';

$datetime_format='%h : %i : %s, %d / %m / %y';

$display=array(
	
		'home'=>'Home', 
		'homepage'=>'Home', 
		'even'=>'UPCOMING EVENTS ',
		'newss'=>'Latest news',
		'group'=>' Research group',
		'linkweb'=>'WEBSITE LINKS ',
		'linkeven'=>'http://thegioixedien.com.vn/',
		

		'news'=>'News',

		'address_support'=>'

			Address: No 8 Dao Duy Tu Lane, Hoan Kiem, Hanoi, Viet Nam

			<br/>Tel: (84-4)39352024/25',

		'booking'=>'Booking',

		'yourinformation'=>'Customer Information',

		'infotour'=>'Let me know about your tour participation.',

		'commentservices'=>'Your comments about our services.',

		'photos'=>'Photos',

		'promotion'=>'Promotion',

		'view_more'=>'View more',

		'old_articles1'=>'Other News : ',

		'assessment'=>'Customer Reviews',

		'rating'=>'Rating',

		'cruiseoverview'=>'Itinerary',

		'lastnews'=>'Lastest news',

		'ticketprice'=>'Normal Price',

		'description'=>'Description',

		'discount'=>'Discount',

		'schedule'=>'Destinations',

		'total'=>'Total',

		'tourinfo'=>'Tour info',

		'cruisequality'=>'Cruise quality',

		'fooddrink'=>'Food/Drink',

		'cabinquality'=>'Cabin quality',

		'staffquality'=>'Staff quality',
	
		'old_news'=>'old news',
	
		'entertainment'=>'Entertainment',

		'differencebettwen'=>'Difference between',

		

		'address1'=>'Schedule',
		

		'dayNews' => 'News today',

		'newsCat' => 'News categories',

		'comment'=>'Comment',

        'commentyou'=>'Comment you',

        'vote'=>'Vote',

        'yourcomment'=>'Your comment',

		'mapstreet'=>'MAP STREET',

		'areasactivity'=>'Scope of activities',

		'businesslinks'=>'Business Links',	

		'whatnew'=>"what's news ?",

		'newreceived'=>'New information received',

		'seemore'=>'See more',

		'readnews'=>'Read news',

		'weblinks'=>'Weblinks',

		'repeatpassword'=>'Repeat password',

		'choselanguage'=>'Chose language',

		'contact_banner'=>'

			<b>Email:</b> info@halongsapatoursbooking.com - <b>Website:</b> halongsapatoursbooking.com

			<br/> Customer service: 24 / 7

		',

		'hotline'=>'Book online or call <b>(84) 984375738</b>',

		'whithh'=>'Why book with us?',

		'whithc'=>'

			+ Great Prices

			<br/>+ Great Quality

			<br/>+ Great Policy

			<br/>+ 24/7 Customer Service

			<br/>+ The best deals for your tour

			<br/>+ Understand what do you want

		',

		

		//--------- Book tour --------------------------------

		'mr/mrs'=>'Mr./Mrs.',

		'sex_info'=>'Mr. - Mrs.',

		'firstname'=>'First name',

		'surname'=>'Surname',

		'nationality'=>'Nationality',

		'comments'=>'comments',

		'kindstourism'=>'Kinds of tourism',

		'adults'=>'Adults (12+)',

		'children'=>'Children (2->12)',

		'infant'=>'Infant (<2)',

//--------- Gallerys page -------------------------------

		'gallery'=>'Gallery',	

		'newscat'=>'Hot News',

		'supportonline'=>'Support online',

		'weather&rates'=>'Weather & rates',			

		'weather'=>'Weather',				

		'SPECIALSAVINGPACKAGES'=>'2013 - 2014 SPECIAL SAVING PACKAGES',				

		'booking_demand'=>'Tour demand',			

		'booking_info'=>'',			

		'booking_demand'=>'Tour demand',			

		'store'=>'Store',

		'mr/mrs'=>'Mr./Mrs.',

		'sex_info'=>'Mr. - Mrs.',

		'firstname'=>'First name',

		'surname'=>'Surname',

		'nationality'=>'Nationality',

		'comments'=>'comments',

		'kindstourism'=>'Kinds of tourism',

		'typehotel'=>'Type of hotel',

		'typeroom'=>'Type of room',

		'start'=>'Day start ',

		'end'=>'End  day ',

		'adults'=>'Adults (12+)',

		'children'=>'Children (2->12)',

		'infant'=>'Infant (<2)',

		'destination'=>'Destination',

		'designedby'=>'Designed by <a >BLUESKY.VN</a>',

		'copyright'=>'Copyright 2013 by Bluesky',		

		'info_com'=>'

		',			

		'amount'=>'Amount',

		'enterkeywords'=>'Enter keywords...',

		'tourfloating'=>'Tour floating on',	

		'typegift'=>'Type of gift',

		'corporate'=>'Corporate',

		'photogallery'=>'Photo gallery',

		'comments'=>'Comments',

		'part_meny'=>'$',	

		'the_last'=>'The last',

		'the_first'=>'The first',

		'phone'=>'Phone',

		'postcode'=>'Postcode',

		'country'=>'Country',

		'videos'=>'Videos clip',

		'links'=>'Links',

		'download'=>'Download',

		'librarybooks'=>'Library books',

		'vietnamese'=>'Việt Nam',

		'english'=>'English',

		'latestpost'=>'Latest post',

		'ads'=>'Ads',

		'customers'=>'customers',

		'best_cus'=>'Partners and loyalty',

		'contact'=>'Contact us',

        'viewall' => 'View all',

		'price'=>'Price',

		'faqs'=>'FAQs',

		'questions'=>'Faqs',

		'code'=>'roduct code',

		'courses'=>'Courses',

		'seeall'=>'See all',

		'newshighlights'=>'News Highlights',

		'newsproducts'=>'New Product',

		'other_products'=>'Other Products : ',

		'same_news'=>'Other news on the subject',

		'partners'=>'Partners and affiliates',

		'detailpro'=>'Detail product',

		'secode'=>'Security Code',

		'onlinesupport'=>'Online support',

		'service'=>'Services',

		'search'=>'Search',

		'contactinfo'=>'Contact information',

		'keywordsearch'=>'Keyword :',

		'timesearch'=>'Time :',

		'time'=>'Date time',

		'transport'=>'Transport',

		'booking'=>'Booking now',

		'datesearch'=>'Date',

		'monthsearch'=>'Month',

		'yearsearch'=>'Year',

		'support'=>'Support',

		'image'=>'Images',

		'page'=>'Page',

		'news'=>'News',

		'rate'=>'Rate',

		'logo_pro'=>'Product Picture',

		'today'=>'Today, ',

		'day'=>'date',

		'visitor'=>'Total',

		'online'=>'Online',

		'new_articles'=>'Later news : ',

		'old_articles'=>'Older news : ',

		'new_products'=>'Later products : ',

		'old_products'=>'Older products : ',

		'noarticle'=>'Not found any article',

		'select'=>'Select',

		'previouspage'=>'Previour page',

		'nextpage'=>'Next page',

		'firstpage' => 'First page',

		'lastpage' => 'Last page',

		'youremail'=>'Your email',

		'receiveemail'=>'Recipient E-mail',

		'notvalid'=>'Not valid',

		'message'=>'Message',

		'max'=>'Max',

		'char'=>'chars',

		'success'=>'Success',

		'unsuccess'=>'Unsuccess',

		'emaillogin'=>'Email Login',

		'password'=>'Password',

		'login'=>'Login',

		'logincompleted'=>'Completed Login !',

		'invalidlogin'=>'Email login or password is incorrect or does not exist !',

		'invalidlosepass'=>'Email login is incorrect or does not exist !',

		'forgetpassword'=>'Forget password',

		'register'=>'Register',

		'pregister'=>'Congratulations to you. You have successfully registered member!',

		'notetip'=>'Click here',

		'editprofile'=>'Change of personal information',

		'editpcompleted'=>'Successful change !',

		'edit'=>'Edit',

		'logout'=>'Log out',

		'memberlogout'=>'Log out successful !',

		'name'=>'Name',

		'address'=>'Address',

		'phone'=>'Phone',

		'email'=>'E-mail',

		'job'=>'Job',

		'jobadd'=>'Office',

		'content'=>'Content',

		'send'=>'Send',

		'end'=>'Result',

		'pools'=>'Pool',

		'reset'=>'Reset',

		'nolink'=>'No link',

		'invalidlink'=>'Invalid link',

		'namegmax'=>'Max 30 chars',

		'passgmax'=>'Min 15 chars',

		'saleman'=>'Sales offices',

		'accountman'=>'Department of Human Resources',

		'supportman'=>'Technical Department',

		'manager'=>'Board of Directors',

		'selectmode'=>'Select',

		'contactwith'=>'Contact with',

		'fullname'=>'Fullname',

		'namecheck'=>'Fullname can not be empty !',

		'addcheck'=>'Address can not be empty !',

		'phonecheck'=>'Phone can not be empty !',

		'fnumbercheck'=>'Phone must be numeric !',

		'emailcheck'=>'Email can not be empty !',

		'contactwithcheck'=>'Please select one department !',

		'contentcheck'=>'Content can not be empty !',

		'emailcheckerror'=>'Invalid email !',

		'introimage'=>'Please type security code in image !',

		'incorrec'=>'Security code not match !',

		'profile'=>'Profile',

		'month'=>'month',

		'searchcheck'=>'Empty keyword !',

		'contactsuccess'=>'Thanks !<br> We will contact to you soon. <br>',

		'withkey' => ' with keyword ',

                'withtag' => ' with tag ',

		);

?>