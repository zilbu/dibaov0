var isMobile = {
    Android: function () {
        return navigator.userAgent.match(/Android/i);
    },
    BlackBerry: function () {
        return navigator.userAgent.match(/BlackBerry/i);
    },
    iOS: function () {
        return navigator.userAgent.match(/iPhone|iPad|iPod/i);
    },
    Opera: function () {
        return navigator.userAgent.match(/Opera Mini/i);
    },
    Windows: function () {
        return navigator.userAgent.match(/IEMobile/i);
    },
    any: function () {
        return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows());
    }
};
function togglesearch(obj){
    if(obj.parentNode.classList.contains('open')){
        obj.parentNode.classList.remove('open')
    }else{
        obj.parentNode.classList.add('open')
    }
}
function showModal(type=2){
    var modal = $('#md_hotline');
    switch (type) {
        case 2:
            modal = $('#md_hotline');
            break;
        default:
            modal = $('#md_hotline');
    }
    modal.modal();
}
$(function(){
    $('#totop').on('click',function(){
        $('html,body').animate({scrollTop: 0},500)
    });
    $(window).scroll(function(){
        if ($(window).scrollTop() >= 200) {
            $('#totop').addClass('show');
        }else{
            $('#totop').removeClass('show');
        }
        if($(window).scrollTop() >= 100){
            $('#header .menu-bar').addClass('neo');
        }else{
            $('#header .menu-bar').removeClass('neo');
        }
    });
    $('#btn_tog_m').on('click',function(){
        $('#header').toggleClass('os');
    });
    $('#btn_toggle').on('click',function(){
        $('#header').toggleClass('open');
    });

})
