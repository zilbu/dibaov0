﻿	function init(number,imagePath,filePrefix,play){
					
					var tgxd360;
					tgxd360 = $('.thegioixedien').ThreeSixty({
						totalFrames: number, // Total no. of image you have for 360 slider
						endFrame: number, // end frame for the auto spin animation
						currentFrame: 1, // This the start frame for auto spin
						imgList: '.threesixty_images', // selector for image list
						progress: '.spinner', // selector to show the loading progress
						imagePath:imagePath, // path of the image assets 'upimages/360/mocha/alaskan/'
						filePrefix: filePrefix, // file prefix if any 'xe-dien-mocha-alaskan-'
						ext: '.jpg', // extention for the assets 
						height: 650,
						width: 960,
						autoplayDirection:0,
						autoplay:1,
						// navigation:!0,
						// disableSpin:!0,
						navigation: true
					});
					if(play == 1){
						tgxd360.play();
					}
					$('.custom_previous').bind('click', function(e) {
					  tgxd360.previous();
					});
					$('.custom_next').bind('click', function(e) {
					  tgxd360.next();
					});
					$('.custom_play').bind('click', function(e) {
					  tgxd360.play();
					});
					$('.custom_stop').bind('click', function(e) {
					  tgxd360.stop();
					});
					// $('.controls-360 button').click(function(e) {
						// var n=$('.controls-360 button');
						// alert(n.data('action'));
						// if(n.data('action') == "play"){
							// n.data("action","pause");
							// n.html('<i class="wico-pause fa fa-pause"><\/i>');
							// tgxd360.play();
						// }
						// if(n.data('action') == "pause"){
							// n.attr("data-action","play");
							// n.html('<i class="wico-play fa fa-play"><\/i>');
							// tgxd360.stop();
					// }
									// var n=$(this);
									// n.data("action")==="play"?(car.play(),n.data("action","stop"),
									// n.html('<i class="wico-pause"><\/i>')):(car.stop(),n.data("action","play"),
									// n.html('<i class="wico-play"><\/i>'))
				}

		
		(function(n){"use strict";
		n.ThreeSixty=function(t,i){
			var u=this,r,f=[];u.$el=n(t);
			u.el=t;u.$el.data("ThreeSixty",u);
			u.init=function(){r=n.extend({},n.ThreeSixty.defaultOptions,i);
			r.disableSpin&&(r.currentFrame=1,r.endFrame=1);
			u.initProgress();
			u.loadImages();
			u.$el.on("mousedown touchstart",function(){u.stop()});
			u.$el.on("mouseup touchend",function(){u.play()})};
			u.resize=function(){};
			u.initProgress=function(){
				u.$el.css({width:r.width+"px",height:r.height+"px","background-image":"none !important"});
				r.styles&&u.$el.css(r.styles);u.responsive();u.$el.find(r.progress).css({marginTop:r.height/2-15+"px"});
				u.$el.find(r.progress).fadeIn("slow");u.$el.find(r.imgList).show("slow")};
				u.loadImages=function(){
					var t,e,i,o;
				t=document.createElement("li");o=r.zeroBased?0:1;e=r.imgArray?r.imgArray[r.loadedImages]:r.domain+r.imagePath+r.filePrefix+u.zeroPad(r.loadedImages+o)+r.ext+(u.browser.isIE()?"?"+(new Date).getTime():"");
				i=n("<img>").attr("src",e).addClass("previous-image").appendTo(t);
				f.push(i);
				u.$el.find(r.imgList).append(t);
				n(i).load(function(){u.imageLoaded()})};
				u.imageLoaded=function(){
					r.loadedImages+=1;
					n(r.progress+" span").text(Math.floor(r.loadedImages/r.totalFrames*100)+"%");
					r.loadedImages>=r.totalFrames?(r.disableSpin&&f[0].removeClass("previous-image").addClass("current-image"),n(r.progress).fadeOut("slow",function(){n(this).hide();
					u.showImages();
					u.showNavigation()})):u.loadImages()
				};
				u.showImages=function(){u.$el.find(".txtC").fadeIn();
				u.$el.find(r.imgList).fadeIn();
				u.ready=!0;r.ready=!0;
				r.drag&&u.initEvents();
				u.refresh();
				u.initPlugins();
				r.onReady();
				setTimeout(function(){u.responsive()},50)
				};
				u.initPlugins=function(){n.each(r.plugins,function(t,i){if(typeof n[i]=="function")n[i].call(u,u.$el,r);else throw new Error(i+" not available.");})};u.showNavigation=function(){
					if(r.navigation&&!r.navigation_init){var t,i,f,e;t=n("<div/>").attr("class","nav_bar");
					i=n("<a/>").attr({href:"#","class":"nav_bar_next"}).html("next");
					f=n("<a/>").attr({href:"#","class":"nav_bar_previous"}).html("previous");e=n("<a/>").attr({href:"#","class":"nav_bar_play"}).html("play");
					t.append(f);
					t.append(e);
					t.append(i);
					u.$el.prepend(t);
					f.bind("mousedown touchstart",u.next);
					i.bind("mousedown touchstart",u.previous);
					e.bind("mousedown touchstart",u.play_stop);
					r.navigation_init=!0}};
					u.play_stop=function(t){t.preventDefault();
					r.autoplay?(r.autoplay=!1,n(t.currentTarget).removeClass("nav_bar_stop").addClass("nav_bar_play"),clearInterval(r.play),r.play=null):(r.autoplay=!0,r.play=setInterval(u.moveToNextFrame,r.playSpeed),n(t.currentTarget).removeClass("nav_bar_play").addClass("nav_bar_stop"))};
					u.next=function(n){
						n&&n.preventDefault();
						r.endFrame-=5;
						u.refresh()};
						u.previous=function(n){n&&n.preventDefault();
							r.endFrame+=5;
							u.refresh()
						};
						u.play=function(n){var t=n||r.playSpeed;
						r.autoplay||(r.autoplay=!0,r.play=setInterval(u.moveToNextFrame,t))};
						u.stop=function(){r.autoplay&&(r.autoplay=!1,clearInterval(r.play),r.play=null)};
						u.moveToNextFrame=function(){
							r.autoplayDirection===1?r.endFrame-=1:r.endFrame+=1;u.refresh()};
						u.gotoAndPlay=function(n){var i;if(r.disableWrap)r.endFrame=n,u.refresh();
					else{
						i=Math.ceil(r.endFrame/r.totalFrames);
						i===0&&(i=1);
					var t=i>1?r.endFrame-(i-1)*r.totalFrames:r.endFrame,f=r.totalFrames-t,e=0;e=n-t>0?n-t<t+(r.totalFrames-n)?r.endFrame+(n-t):r.endFrame-(t+(r.totalFrames-n)):t-n<f+n?r.endFrame-(t-n):r.endFrame+(f+n);t!==n&&(r.endFrame=e,u.refresh())}};
					u.initEvents=function(){
						u.$el.bind("mousedown touchstart touchmove touchend mousemove click",function(n){
						n.preventDefault();
						n.type==="mousedown"&&n.which===1||n.type==="touchstart"?(r.pointerStartPosX=u.getPointerEvent(n).pageX,r.dragging=!0):n.type==="touchmove"?u.trackPointer(n):n.type==="touchend"&&(r.dragging=!1)
					});
					n(document).bind("mouseup",function(){r.dragging=!1;
					n(this).css("cursor","none")});
					n(window).bind("resize",function(){u.responsive()});
					n(document).bind("mousemove",function(n){
						r.dragging?(n.preventDefault(),!u.browser.isIE&&r.showCursor&&u.$el.css("cursor","url(assets/images/hand_closed.png), auto")):!u.browser.isIE&&r.showCursor&&u.$el.css("cursor","url(assets/images/hand_open.png), auto");
					u.trackPointer(n)});
					n(window).bind("resize",function(){u.resize()})};
					u.getPointerEvent=function(n){return n.originalEvent.targetTouches?n.originalEvent.targetTouches[0]:n};
					u.trackPointer=function(n){
						r.ready&&r.dragging&&(r.pointerEndPosX=u.getPointerEvent(n).pageX,r.monitorStartTime<(new Date).getTime()-r.monitorInt&&(r.pointerDistance=r.pointerEndPosX-r.pointerStartPosX,r.endFrame=r.pointerDistance>0?r.currentFrame-Math.ceil((r.totalFrames-1)*r.speedMultiplier*(r.pointerDistance/u.$el.width())):r.currentFrame-Math.floor((r.totalFrames-1)*r.speedMultiplier*(r.pointerDistance/u.$el.width())),r.disableWrap&&(r.endFrame=Math.min(r.totalFrames-(r.zeroBased?1:0),r.endFrame),r.endFrame=Math.max(r.zeroBased?0:1,r.endFrame)),u.refresh(),r.monitorStartTime=(new Date).getTime(),r.pointerStartPosX=u.getPointerEvent(n).pageX))};
					u.refresh=function(){
						r.ticker===0&&(r.ticker=setInterval(u.render,Math.round(1e3/r.framerate)))};
						u.render=function(){
							var n;
							r.currentFrame!==r.endFrame?(n=r.endFrame<r.currentFrame?Math.floor((r.endFrame-r.currentFrame)*.1):Math.ceil((r.endFrame-r.currentFrame)*.1),u.hidePreviousFrame(),r.currentFrame+=n,u.showCurrentFrame(),u.$el.trigger("frameIndexChanged",[u.getNormalizedCurrentFrame(),r.totalFrames])):(window.clearInterval(r.ticker),r.ticker=0)};
								u.hidePreviousFrame=function(){
									f[u.getNormalizedCurrentFrame()].removeClass("current-image").addClass("previous-image")
								};
							u.showCurrentFrame=function(){f[u.getNormalizedCurrentFrame()].removeClass("previous-image").addClass("current-image")};
							u.getNormalizedCurrentFrame=function(){
								var n,t;
								return r.disableWrap?(n=Math.min(r.currentFrame,r.totalFrames-(r.zeroBased?1:0)),
								t=Math.min(r.endFrame,r.totalFrames-(r.zeroBased?1:0)),n=Math.max(n,r.zeroBased?0:1),
								t=Math.max(t,r.zeroBased?0:1),r.currentFrame=n,r.endFrame=t):(n=Math.ceil(r.currentFrame%r.totalFrames),
								n<0&&(n+=r.totalFrames-(r.zeroBased?1:0))),n};
							u.getCurrentFrame=function(){return r.currentFrame};
							u.responsive=function(){r.responsive&&u.$el.css({height:u.$el.find(".current-image").first().css("height"),width:"100%"})};u.zeroPad=function(n){function i(n,t){var i=n.toString();if(r.zeroPadding)while(i.length<t)i="0"+i;return i}var u=Math.log(r.totalFrames)/Math.LN10,t=1e3,f=Math.round(u*t)/t,e=Math.floor(f)+1;return i(n,e)};u.browser={};u.browser.isIE=function(){var n=-1,t,i;return navigator.appName==="Microsoft Internet Explorer"&&(t=navigator.userAgent,i=new RegExp("MSIE ([0-9]{1,}[\\.0-9]{0,})"),i.exec(t)!==null&&(n=parseFloat(RegExp.$1))),n!==-1};u.getConfig=function(){return r};
							n.ThreeSixty.defaultOptions={
								dragging:!1,ready:!1,pointerStartPosX:0,pointerEndPosX:0,
								pointerDistance:0,monitorStartTime:0,monitorInt:10,ticker:0,
								speedMultiplier:10,totalFrames:180,currentFrame:0,endFrame:0,
								loadedImages:0,framerate:60,domains:null,domain:"",parallel:!1,
								queueAmount:8,idle:0,filePrefix:"",ext:"jpg",height:960,width:700,
								styles:{},navigation:!1,autoplay:!1,autoplayDirection:1,disableSpin:!1,disableWrap:!1,
								responsive:!1,zeroPadding:!1,zeroBased:!1,plugins:[],showCursor:!1,
								drag:!0,
								onReady:function(){},
			imgList:".threesixty_images",imgArray:null,playSpeed:280};u.init()};
			n.fn.ThreeSixty=function(t){
				return Object.create(new n.ThreeSixty(this,t))}})
			(jQuery);typeof Object.create&&(Object.create=function(a){"use strict";function b(){}return b.prototype=a,new b});	
							
var xhr;
var nameCookieJsScript = 'cookieReffer';
//$(function () {
//    $('.ppu_tab_pro_sales').hover(
//  function () {
//      $(".ppu_tab_color_img").show();
//  },
//  function () {
//      $(".ppu_tab_color_img").hide();
//  }
//);
	
			
			
	// !function(a){"use strict";
	// a.ThreeSixty=function(b,c){
		// var d,e=this,f=[];e.$el=a(b),e.el=b,e.$el.data("ThreeSixty",e),
		// e.init=function(){d=a.extend({},
		// a.ThreeSixty.defaultOptions,c),
		// d.disableSpin&&(d.currentFrame=1,d.endFrame=1),
		// e.initProgress(),
		// e.loadImages(),
		// e.on("mousedown touchstart",function(){u.stop()}),
		// e.on("mouseup touchend",function(){u.play()})},
		// e.resize=function(){},
		// e.initProgress=function(){e.$el.css({width:d.width+"px",height:d.height+"px","background-image":"none !important"}),
		// d.styles&&e.$el.css(d.styles),
		// e.responsive(),
		// e.$el.find(d.progress).css({marginTop:d.height/2-15+"px"}),
		// e.$el.find(d.progress).fadeIn("slow"),
		// e.$el.find(d.imgList).hide()},
			// e.loadImages=function(){
			// var b,c,g,h;
			// b=document.createElement("li"),
			// h=d.zeroBased?0:1,
			// c=d.imgArray?d.imgArray[d.loadedImages]:d.domain+d.imagePath+d.filePrefix+e.zeroPad(d.loadedImages+h)+d.ext+(e.browser.isIE()?"?"+(new Date).getTime():""),
			// g=a("<img>").attr("src",c).addClass("previous-image").appendTo(b),
			// f.push(g),e.$el.find(d.imgList).append(b),a(g).load(function(){e.imageLoaded()})
		// },
			// e.imageLoaded=function(){d.loadedImages+=1,a(d.progress+" span").text(Math.floor(100*(d.loadedImages/d.totalFrames))+"%"),d.loadedImages>=d.totalFrames?(d.disableSpin&&f[0].removeClass("previous-image").addClass("current-image"),a(d.progress).fadeOut("slow",function(){a(this).hide(),e.showImages(),e.showNavigation()})):e.loadImages()}
			// ,e.showImages=function(){
			// e.$el.find(".txtC").fadeIn(),
			// e.$el.find(d.imgList).fadeIn(),
			// e.ready=!0,d.ready=!0,d.drag&&e.initEvents(),
			// e.refresh(),e.initPlugins(),d.onReady(),
			// setTimeout(function(){e.responsive()},50)},
			// e.initPlugins=function(){
				// a.each(d.plugins,function(b,c){
					// if("function"!=typeof a[c])throw new Error(c+" not available.");a[c].call(e,e.$el,d)})},
			// e.showNavigation=function(){
				// if(d.navigation&&!d.navigation_init){
					// var b,c,f,g;b=a("<div/>").attr("class","nav_bar"),
					// c=a("<a/>").attr({href:"#","class":"nav_bar_next"}).html("next"),
					// f=a("<a/>").attr({href:"#","class":"nav_bar_previous"}).html("previous"),
					// g=a("<a/>").attr({href:"#","class":"nav_bar_play"}).html("play"),
					// b.append(f),
					// b.append(g),
					// b.append(c),
					// e.$el.prepend(b),
					// c.bind("mousedown touchstart",e.stop),
					// f.bind("mousedown touchstart",e.previous),
					// g.bind("mousedown touchstart",e.play_stop),
					// d.navigation_init=!0}},
					// e.play_stop=function(b){
						// b.preventDefault(),
						// d.autoplay?(d.autoplay=!1,a(b.currentTarget).removeClass("nav_bar_stop").addClass("nav_bar_play"),
						// clearInterval(d.play),d.play=null):(d.autoplay=!0,d.play=setInterval(e.moveToNextFrame,d.playSpeed),a(b.currentTarget).removeClass("nav_bar_play").addClass("nav_bar_stop"))},
					// e.next=function(a){
						// a&&a.preventDefault(),
						// d.endFrame-=5,e.refresh()},
						// e.previous=function(a){a&&a.preventDefault(),
						// d.endFrame+=5,e.refresh()},e.play=function(a){
							// var b=a||d.playSpeed;
							// d.autoplay||(d.autoplay=!0,d.play=setInterval(e.moveToNextFrame,b))},
						// e.stop=function(){
							// d.autoplay&&(d.autoplay=!1,clearInterval(d.play),d.play=null)},
							// e.moveToNextFrame=function(){
								// 1===d.autoplayDirection?d.endFrame-=1:d.endFrame+=1,e.refresh()},e.gotoAndPlay=function(a){if(d.disableWrap)d.endFrame=a,e.refresh();else{var b=Math.ceil(d.endFrame/d.totalFrames);0===b&&(b=1);
								// var c=b>1?d.endFrame-(b-1)*d.totalFrames:d.endFrame,f=d.totalFrames-c,g=0;g=a-c>0?a-c<c+(d.totalFrames-a)?d.endFrame+(a-c):d.endFrame-(c+(d.totalFrames-a)):f+a>c-a?d.endFrame-(c-a):d.endFrame+(f+a),c!==a&&(d.endFrame=g,e.refresh())}},
						// e.initEvents=function(){
							// e.$el.bind("mousedown touchstart touchmove touchend mousemove click",function(a){
								// a.preventDefault(),"mousedown"===a.type&&1===a.which||"touchstart"===a.type?(d.pointerStartPosX=e.getPointerEvent(a).pageX,d.dragging=!0):"touchmove"===a.type?e.trackPointer(a):"touchend"===a.type&&(d.dragging=!1)}),
						// a(document).bind("mouseup",function(){
							// d.dragging=!1,a(this).css("cursor","none")}),
						// a(window).bind("resize",function(){
							// e.responsive()}),
						// a(document).bind("mousemove",function(a){
							// d.dragging?(a.preventDefault(),!e.browser.isIE&&d.showCursor&&e.$el.css("cursor","url(undefined1.jpg), auto")):!e.browser.isIE&&d.showCursor&&e.$el.css("cursor","url(undefined1.jpg), auto"),e.trackPointer(a)}),
						// a(window).resize(function(){e.resize()})},
						// e.getPointerEvent=function(a){return a.originalEvent.targetTouches?a.originalEvent.targetTouches[0]:a},
						// e.trackPointer=function(a){d.ready&&d.dragging&&(d.pointerEndPosX=e.getPointerEvent(a).pageX,d.monitorStartTime<(new Date).getTime()-d.monitorInt&&(d.pointerDistance=d.pointerEndPosX-d.pointerStartPosX,d.endFrame=d.pointerDistance>0?d.currentFrame+Math.ceil((d.totalFrames-1)*d.speedMultiplier*(d.pointerDistance/e.$el.width())):d.currentFrame+Math.floor((d.totalFrames-1)*d.speedMultiplier*(d.pointerDistance/e.$el.width())),d.disableWrap&&(d.endFrame=Math.min(d.totalFrames-(d.zeroBased?1:0),d.endFrame),d.endFrame=Math.max(d.zeroBased?0:1,d.endFrame)),e.refresh(),d.monitorStartTime=(new Date).getTime(),d.pointerStartPosX=e.getPointerEvent(a).pageX))},
						// e.refresh=function(){0===d.ticker&&(d.ticker=setInterval(e.render,Math.round(1e3/d.framerate)))},
						// e.render=function(){var a;d.currentFrame!==d.endFrame?(a=d.endFrame<d.currentFrame?Math.floor(.1*(d.endFrame-d.currentFrame)):Math.ceil(.1*(d.endFrame-d.currentFrame)),e.hidePreviousFrame(),d.currentFrame+=a,e.showCurrentFrame(),e.$el.trigger("frameIndexChanged",[e.getNormalizedCurrentFrame(),d.totalFrames])):(window.clearInterval(d.ticker),d.ticker=0)},
						// e.hidePreviousFrame=function(){f[e.getNormalizedCurrentFrame()].removeClass("current-image").addClass("previous-image")},
						// e.showCurrentFrame=function(){f[e.getNormalizedCurrentFrame()].removeClass("previous-image").addClass("current-image")},
						// e.getNormalizedCurrentFrame=function(){
							// var a,b;return d.disableWrap?(a=Math.min(d.currentFrame,d.totalFrames-(d.zeroBased?1:0)),b=Math.min(d.endFrame,d.totalFrames-(d.zeroBased?1:0)),a=Math.max(a,d.zeroBased?0:1),b=Math.max(b,d.zeroBased?0:1),d.currentFrame=a,d.endFrame=b):(a=Math.ceil(d.currentFrame%d.totalFrames),0>a&&(a+=d.totalFrames-(d.zeroBased?1:0))),a},
						// e.getCurrentFrame=function(){
							// return d.currentFrame},
							// e.responsive=function(){d.responsive&&e.$el.css({height:e.$el.find(".current-image").first().css("height"),width:"100%"})},
							// e.zeroPad=function(a){function b(a,b){var c=a.toString();if(d.zeroPadding)for(;c.length<b;)c="0"+c;return c}var c=Math.log(d.totalFrames)/Math.LN10,e=1e3,f=Math.round(c*e)/e,g=Math.floor(f)+1;return b(a,g)},
						// e.browser={},e.browser.isIE=function(){var a=-1;if("Microsoft Internet Explorer"===navigator.appName){var b=navigator.userAgent,c=new RegExp("MSIE ([0-9]{1,}[\\.0-9]{0,})");null!==c.exec(b)&&(a=parseFloat(RegExp.$1))}
							// return-1!==a},e.getConfig=function(){return d},
							// a.ThreeSixty.defaultOptions={
								// dragging:!1,ready:!1,pointerStartPosX:0,pointerEndPosX:0,
								// pointerDistance:0,monitorStartTime:0,monitorInt:10,ticker:0,
								// speedMultiplier:70,totalFrames:180,currentFrame:0,endFrame:0,
								// loadedImages:0,framerate:60,domains:null,domain:"",parallel:!1,
								// queueAmount:8,idle:0,filePrefix:"",ext:"jpg",height:300,width:300,
								// styles:{},navigation:!1,autoplay:!1,autoplayDirection:1,disableSpin:!1,disableWrap:!1,
								// responsive:!1,zeroPadding:!1,zeroBased:!1,plugins:[],showCursor:!1,
								// drag:!0,
								// onReady:function(){},imgList:".threesixty_images",imgArray:null,playSpeed:250},e.init()},a.fn.ThreeSixty=function(b){
				// return Object.create(new a.ThreeSixty(this,b))}}
							// (jQuery),"function"!=typeof Object.create&&(Object.create=function(a){"u